var App = function () {
    return {
        init: function () {
            $('input[type=text].numeric').numeric();

            $('input[type=text].cep').unmask();
            $('input[type=text].cep').mask('99999-999').change(function () {
                var element = $(this),
                    url = sprintf('https://viacep.com.br/ws/%s/json/', element.val().replace('-', '')),
                    selected = element.data('selected'),
                    options = {
                        type: 'GET',
                        url: url,
                        dataType: 'json',
                        onbeforesend: function (xhr, settings) {

                        },
                        onerror: function (xhr, status, errorThrown) {

                        },
                        onsuccess: function (xhr, status, args) {
                            if (xhr.erro) {

                            } else {
                                $('#logradouro').val(xhr.logradouro);
                                $('#logradouro-value').val(xhr.logradouro);
                                $('#complemento').val(xhr.complemento);
                                $('#bairro').val(xhr.bairro);
                                $('#bairro-value').val(xhr.bairro);
                                var options = {
                                    type: 'GET',
                                    url: sprintf('/rest/repository/cidade/find/%s', xhr.ibge),
                                    dataType: 'json',
                                    onbeforesend: function (xhr, settings) {

                                    },
                                    onerror: function (xhr, status, errorThrown) {

                                    },
                                    onsuccess: function (xhr, status, args) {
                                        $('#estado').data('selected', xhr.estado.id);
                                        $('#cidade').data('selected', xhr.id);
                                        handleEstado();
                                        handleCidade();
                                        $('#numero').focus();
                                        return true;
                                    }
                                };

                                ajax.Request.handle(options);
                            }
                            return true;
                        }
                    };

                ajax.Request.handle(options);
            });

            $('input[type=text].cpf').unmask();
            $('input[type=text].cpf').mask('999.999.999-99');

            $('input[type=text].cnpj').unmask();
            $('input[type=text].cnpj').mask('99.999.999/9999-99');

            $('input[type=tel]').unmask();
            $('input[type=tel]').mask('(99) 9999-9999?9').focusout(function () {
                var phone, element;
                element = $(this);
                element.unmask();
                phone = element.val().replace(/\D/g, '');
                if (phone.length > 10) {
                    element.mask('(99) 99999-999?9');
                } else {
                    element.mask('(99) 9999-9999?9');
                }
            }).trigger('focusout');

            $('.decimal').each(function () {
                var $this = $(this);
                var precision = $this.attr('precision') == undefined ? 2 : $this.attr('precision');
                var maxValue = $this.attr('maxValue') == undefined ? -1 : $this.attr('maxValue');
                var minValue = $this.attr('minValue') == undefined ? -1 : $this.attr('minValue');
                var allowZero = $this.attr('allowZero') == undefined ? false : $this.attr('allowZero');

                $this.maskMoney({
                    symbol: '',
                    showSymbol: true,
                    thousands: '.',
                    decimal: ',',
                    symbolStay: true,
                    allowZero: allowZero,
                    precision: parseInt(precision),
                    maxValue: parseFloat(maxValue),
                    minValue: parseFloat(minValue)
                });
            });
        }
    }
}();

var Message = function () {
    return {
        warning: function (title, message, appendTo) {
            if (message !== null) {
                $('.alert-warning').fadeOut("slow");
                $('.alert-warning').remove();
                $('  <div class="alert alert-warning fade in alert-dismissable">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                    '    <strong>' + title + '</strong>' +
                    '    <br/>' +
                    '    ' + message +
                    '</div>').prependTo(appendTo);
                $('.alert-warning').fadeIn("slow");

                $('html, body').animate({
                    scrollTop: appendTo.offset().top
                }, 2000);
            }
        },
        danger: function (title, message, appendTo) {
            if (message !== null) {
                $('.alert-danger').fadeOut("slow");
                $('.alert-danger').remove();
                $('  <div class="alert alert-danger fade in alert-dismissable" style="display: none">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                    '    <strong>' + title + '</strong>' +
                    '    <br/>' +
                    '    ' + message +
                    '</div>').prependTo(appendTo);
                $('.alert-danger').fadeIn("slow");

                $('html, body').animate({
                    scrollTop: appendTo.offset().top
                }, 2000);
            }
        },
        success: function (title, message, appendTo) {
            if (message !== null) {
                $('.alert-success').fadeOut("slow");
                $('.alert-success').remove();
                $('  <div class="alert alert-success fade in alert-dismissable">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                    '    <strong>' + title + '</strong>' +
                    '    <br/>' +
                    '    ' + message +
                    '</div>').prependTo(appendTo);
                $('.alert-success').fadeIn("slow");

                $('html, body').animate({
                    scrollTop: appendTo.offset().top
                }, 2000);
            }
        },
        info: function (title, message, appendTo) {
            if (message !== null) {
                $('.alert-info').fadeOut("slow");
                $('.alert-info').remove();
                $('  <div class="alert alert-info fade in alert-dismissable">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                    '    <strong>' + title + '</strong>' +
                    '    <br/>' +
                    '    ' + message +
                    '</div>').prependTo(appendTo);
                $('.alert-info').fadeIn("slow");

                $('html, body').animate({
                    scrollTop: appendTo.offset().top
                }, 2000);
            }
        }
    }
}();