var ajax = function () {
    return {
        Loading: {
            init: function (id) {
                id = id || 'canvasloader-container';

                if (window.Loading && window.Loading.indexOf(id) >= 0) {
                    return;
                }

                var cl = new CanvasLoader(id);
                cl.setColor('#4384f4');
                cl.setShape('spiral');
                cl.setDiameter(40);
                cl.setDensity(77);
                cl.setRange(0.9);
                cl.setSpeed(3);
                cl.setFPS(32);
                cl.show();

                if (!window.Loading) {
                    window.Loading = [];
                }

                Loading.push(id);
                ajax.Loading.hide(id);
            },

            show: function (id) {
                id = id || 'canvasloader-container';
                $(ajax.escapeClientId(id)).show();
            },

            hide: function (id) {
                id = id || 'canvasloader-container';
                $(ajax.escapeClientId(id)).hide();
            }
        },

        Utils: {

            loadStylesheets: function (stylesheets) {
                for (var i = 0; i < stylesheets.length; i++) {
                    $('head').append('<link type="text/css" rel="stylesheet" href="' + stylesheets[i] + '" />');
                }
            },

            loadScripts: function (scripts) {
                var loadNextScript = function () {
                    var script = scripts.shift();
                    if (script) {
                        ajax.getScript(script, loadNextScript);
                    }
                };

                loadNextScript();
            },

            getContent: function (node) {
                return node.outerHTML;
            },

            updateFormStateInput: function (name, value, xhr) {
                var trimmedValue = $.trim(value);

                var forms = null;
                if (xhr && xhr.ajaxSettings && xhr.ajaxSettings.portletForms) {
                    forms = $(xhr.ajaxSettings.portletForms);
                } else {
                    forms = $('form');
                }

                var parameterNamespace = '';
                if (xhr && xhr.ajaxArgs && xhr.ajaxArgs.parameterNamespace) {
                    parameterNamespace = xhr.ajaxArgs.parameterNamespace;
                }

                for (var i = 0; i < forms.length; i++) {
                    var form = forms.eq(i);

                    if (form.attr('method') === 'post') {
                        var input = form.children("input[name='" + parameterNamespace + name + "']");

                        if (input.length > 0) {
                            input.val(trimmedValue);
                        } else {
                            form.append('<input type="hidden" name="' + parameterNamespace + name + '" value="' + trimmedValue + '" autocomplete="off" />');
                        }
                    }
                }
            },

            updateHead: function (content) {
                var cache = $.ajaxSetup()['cache'];
                $.ajaxSetup()['cache'] = true;

                var headStartTag = new RegExp("<head[^>]*>", "gi").exec(content)[0];
                var headStartIndex = content.indexOf(headStartTag) + headStartTag.length;
                $('head').html(content.substring(headStartIndex, content.lastIndexOf("</head>")));

                $.ajaxSetup()['cache'] = cache;
            },

            updateBody: function (content) {
                var bodyStartTag = new RegExp("<body[^>]*>", "gi").exec(content)[0];
                var bodyStartIndex = content.indexOf(bodyStartTag) + bodyStartTag.length;
                $('body').html(content.substring(bodyStartIndex, content.lastIndexOf("</body>")));
            },

            updateElement: function (id, content, xhr) {
                if (id === $('head')[0].id) {
                    ajax.Utils.updateHead(content);
                } else {
                    $(ajax.escapeClientId(id)).replaceWith(content);
                }
            }
        },

        Queue: {

            delays: {},

            requests: new Array(),

            xhrs: new Array(),

            offer: function (request) {
                if (request.delay && request.sourceId) {
                    var $this = this,
                        sourceId = request.sourceId,
                        createTimeout = function () {
                            return setTimeout(function () {
                                $this.requests.push(request);

                                if ($this.requests.length === 1) {
                                    ajax.Request.send(request);
                                }
                            }, request.delay);
                        };

                    if (this.delays[sourceId]) {
                        clearTimeout(this.delays[sourceId].timeout);
                        this.delays[sourceId].timeout = createTimeout();
                    } else {
                        this.delays[sourceId] = {
                            timeout: createTimeout()
                        };
                    }
                } else {
                    this.requests.push(request);

                    if (this.requests.length === 1) {
                        ajax.Request.send(request);
                    }
                }
            },

            poll: function () {
                if (this.isEmpty()) {
                    return null;
                }

                var processed = this.requests.shift(),
                    next = this.peek();

                if (next) {
                    ajax.Request.send(next);
                }

                return processed;
            },

            peek: function () {
                if (this.isEmpty()) {
                    return null;
                }

                return this.requests[0];
            },

            isEmpty: function () {
                return this.requests.length === 0;
            },

            addXHR: function (xhr) {
                this.xhrs.push(xhr);
            },

            removeXHR: function (xhr) {
                var index = $.inArray(xhr, this.xhrs);
                if (index > -1) {
                    this.xhrs.splice(index, 1);
                }
            },

            abortAll: function () {
                for (var i = 0; i < this.xhrs.length; i++) {
                    this.xhrs[i].abort();
                }

                this.xhrs = new Array();
                this.requests = new Array();
            }
        },

        Request: {

            handle: function (cfg, ext) {
                cfg.ext = ext;

                if (cfg.async === undefined) {
                    cfg.async = true;
                }

                if (cfg.async) {
                    ajax.Request.send(cfg);
                } else {
                    ajax.Queue.offer(cfg);
                }
            },

            send: function (cfg) {
                if (cfg.async === undefined) {
                    cfg.async = true;
                }

                var global = (cfg.global === true || cfg.global === undefined) ? true : false,
                    csrf = (cfg.dataType === undefined || cfg.dataType.toLowerCase() !== 'json') ? $('meta[name=csrf]').attr('content') : false,
                    csrf_header = (cfg.dataType === undefined || cfg.dataType.toLowerCase() !== 'json') ? $('meta[name=csrf_header]').attr('content') : false,
                    form = ajax.getForm(cfg),
                    sourceId = ajax.getSourceId(cfg);

                if (cfg.onstart) {
                    var retVal = cfg.onstart.call(this, cfg);
                    if (retVal === false) {
                        if (!cfg.async) {
                            ajax.Queue.poll();
                        }

                        return false;
                    }
                }

                if (cfg.ext && cfg.ext.onstart) {
                    cfg.ext.onstart.call(this, cfg);
                }

                if (global) {
                    $(document).trigger('ajaxStart');
                }

                var url = cfg.url || form.attr('action'),
                    contentType = cfg.contentType || (form !== null && form.attr('enctype')) || 'application/x-www-form-urlencoded; charset=UTF-8',
                    params = [];

                if ('multipart/form-data' === contentType && (form === null && form === undefined)) {
                    console.log('Error: para utilizar "multipart/form-data" e preciso ter um formulario.');
                    return;
                }

                if (cfg.url) {
                    if (!new RegExp("^(http|https)://", "i").test(url)) {
                        url = ajax.getContextPath() + url;
                    }
                }

                if (cfg.params) {
                    ajax.Request.addParams(params, cfg.params);
                }

                if (cfg.ext && cfg.ext.params) {
                    ajax.Request.addParams(params, cfg.ext.params);
                }

                if (csrf && (form === null || form.find('input[type=hidden][name=_csrf]').length === 0)) {
                    ajax.Request.addParam(params, '_csrf', csrf)
                }

                if (csrf_header) {
                    ajax.Request.addParam(params, '_csrf_header', csrf_header)
                }

                if (form) {
                    $.merge(params, form.serializeArray());
                }

                var data = 'multipart/form-data' === contentType ? new FormData(form.get(0)) : $.param(params),
                    xhrOptions = {
                        url: url + ('multipart/form-data' === contentType ? ('?' + $.param(params)) : ''),
                        contentType: contentType,
                        type: ('multipart/form-data' === contentType ? 'POST' : false) || cfg.type || 'POST',
                        cache: false,
                        dataType: ('multipart/form-data' === contentType ? 'html' : false) || cfg.dataType || 'html',
                        data: data,
                        sourceId: sourceId,
                        global: false,
                        beforeSend: function (xhr, settings) {
                            xhr.ajaxSettings = settings;
                            xhr.ajaxArgs = {};

                            if (cfg.onbeforesend) {
                                cfg.onbeforesend.call(this, xhr, settings);
                            }

                            if (cfg.ext && cfg.ext.onbeforesend) {
                                cfg.ext.onbeforesend.call(this, xhr, settings);
                            }

                            if (global) {
                                $(document).trigger('ajaxSend', [xhr, this]);
                            }
                        },
                        error: function (xhr, status, errorThrown) {
                            var modal = $('<div class="modal fade" id="session-expired" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">'
                                + '        <div class="modal-dialog" role="document">'
                                + '            <div class="modal-content">'
                                + '                 <div class="modal-body">'
                                + '                     <p>Sua sessão expirou!</p>'
                                + '                     <p>Você será redirecionado para tela de login.</p>'
                                + '                 </div>'
                                + '                 <div class="modal-footer">'
                                + '                     <button id="btn-ok" type="button" class="btn-u btn-success" data-dismiss="modal" onclick="ajax.ResponseProcessor.doRedirect(\'/login\');">Redirecionar</button>'
                                + '                 </div>'
                                + '            </div>'
                                + '        </div>'
                                + '    </div>');

                            $.ajax({
                                type: "GET",
                                url: ajax.getContextPath(),
                                success: function (data_error, status_error, xhr_error) {
                                    if ($(data_error).find('form').attr('action') === '/login') {
                                        modal.appendTo($('body'));
                                        $('#session-expired').modal('show');
                                    } else {
                                        var html = $(xhr.responseText),
                                            code = html.find('title').text(),
                                            title = html.find('p.error-title').text(),
                                            message = html.find('p.error-message').text();
                                        console.log(code + ' - ' + title + ': ' + message);

                                        if (cfg.onerror) {
                                            cfg.onerror.call(this, xhr, title, message);
                                        }

                                        if (cfg.ext && cfg.ext.onerror) {
                                            cfg.ext.onerror.call(this, xhr, title, message);
                                        }

                                        if (global) {
                                            $(document).trigger('ajaxError', [xhr, this, code + ' - ' + title + ': ' + message]);
                                        }

                                        if (form) {
                                            form.children('input.ui-submit-param').remove();
                                        }
                                    }
                                }
                            });
                        },
                        success: function (data, status, xhr) {
                            var parsed;

                            var html = $(data);

                            if (!html.is('.invalid') && !html.is('.severe')) {
                                if (cfg.onsuccess) {
                                    parsed = cfg.onsuccess.call(this, data, status, xhr);
                                }
                            }

                            if (cfg.ext && cfg.ext.onsuccess && !parsed) {
                                parsed = cfg.ext.onsuccess.call(this, data, status, xhr);
                            }

                            if (global) {
                                $(document).trigger('ajaxSuccess', [xhr, this]);
                            }

                            if (parsed) {
                                $('input.invalid:visible:first').focus();

                                if (html.is('.invalid') || html.is('.severe')) {
                                    if (cfg.onerror) {
                                        cfg.onerror.call(this, xhr, status, null);
                                    }
                                }
                                return;
                            } else {
                                ajax.Response.handle(data, status, xhr);
                                $('input.invalid:visible:first').focus();

                                if (html.is('.invalid') || html.is('.severe')) {
                                    if (cfg.onerror) {
                                        cfg.onerror.call(this, xhr, status, null);
                                    }
                                }
                            }
                        },
                        complete: function (xhr, status) {
                            if (cfg.oncomplete) {
                                cfg.oncomplete.call(this, xhr, status, xhr.pfArgs);
                            }

                            if (cfg.ext && cfg.ext.oncomplete) {
                                cfg.ext.oncomplete.call(this, xhr, status, xhr.pfArgs);
                            }

                            if (global) {
                                $(document).trigger('ajaxComplete', [xhr, this]);
                            }

                            form = ajax.getForm(cfg);

                            if (form) {
                                form.children('input.ui-submit-param').remove();
                            }

                            ajax.Queue.removeXHR(xhr);

                            if (!cfg.async) {
                                ajax.Queue.poll();
                            }
                        }
                    };

                if (cfg.timeout) {
                    xhrOptions['timeout'] = cfg.timeout;
                }

                ajax.Queue.addXHR($.ajax(xhrOptions));
            },

            resolveExpressionsForAjaxCall: function (cfg, type) {
                var expressions = '';

                if (cfg[type]) {
                    expressions += cfg[type];
                }

                if (cfg.ext && cfg.ext[type]) {
                    expressions += " " + cfg.ext[type];
                }

                return expressions;
            },

            resolveComponentsForAjaxCall: function (cfg, type) {
                var expressions = ajax.Request.resolveExpressionsForAjaxCall(cfg, type);
                return ajax.expressions.SearchExpressionFacade.resolveComponents(expressions);
            },

            addParam: function (params, name, value, parameterNamespace) {
                if (parameterNamespace || !name.indexOf(parameterNamespace) === 0) {
                    params.push({name: parameterNamespace + name, value: value});
                } else {
                    params.push({name: name, value: value});
                }

            },

            addParams: function (params, paramsToAdd, parameterNamespace) {
                for (var i = 0; i < paramsToAdd.length; i++) {
                    var param = paramsToAdd[i];
                    if (parameterNamespace && !param.name.indexOf(parameterNamespace) === 0) {
                        param.name = parameterNamespace + param.name;
                    }

                    params.push(param);
                }
            },

            addParamFromInput: function (params, name, form, parameterNamespace) {
                var input = null;
                if (parameterNamespace) {
                    input = form.children("input[name*='" + name + "']");
                } else {
                    input = form.children("input[name='" + name + "']");
                }

                if (input && input.length > 0) {
                    var value = input.val();
                    ajax.Request.addParam(params, name, value, parameterNamespace);
                }
            }
        },

        Response: {

            handle: function (data, status, xhr) {
                var doc = $.parseHTML(data),
                    partialResponseNode = doc[0];

                switch (partialResponseNode.nodeName.toLowerCase()) {
                    case "delete":
                        ajax.ResponseProcessor.doDelete(partialResponseNode);
                        break;

                    case "insert":
                        ajax.ResponseProcessor.doInsert(partialResponseNode);
                        break;

                    case "attributes":
                        ajax.ResponseProcessor.doAttributes(partialResponseNode);
                        break;

                    case "eval":
                        ajax.ResponseProcessor.doEval(partialResponseNode);
                        break;

                    case "extension":
                        ajax.ResponseProcessor.doExtension(partialResponseNode, xhr);
                        break;

                    case "eval":
                        ajax.ResponseProcessor.doEval(partialResponseNode);
                        break;

                    case "extension":
                        ajax.ResponseProcessor.doExtension(partialResponseNode, xhr);
                        break;

                    case "error":
                        ajax.ResponseProcessor.doError(partialResponseNode, xhr);
                        break;

                    case "redirect":
                        ajax.ResponseProcessor.doRedirect(partialResponseNode);
                        break;

                    default:
                        var activeElement = $(document.activeElement);
                        var activeElementId = activeElement.attr('id');
                        var activeElementSelection;
                        if (activeElement.length > 0 && activeElement.is('input') && $.isFunction($.fn.getSelection)) {
                            activeElementSelection = activeElement.getSelection();
                        }

                        ajax.ResponseProcessor.doUpdate(partialResponseNode, xhr);
                        ajax.Response.handleReFocus(activeElementId, activeElementSelection);
                        break;
                }

                ajax.ResponseProcessor.addCallbackParam(xhr);
            },

            handleReFocus: function (activeElementId, activeElementSelection) {

                if (window.customFocus === false
                    && activeElementId
                    && activeElementId !== $(document.activeElement).attr('id')) {

                    var elementToFocus = $(ajax.escapeClientId(activeElementId));
                    var refocus = function () {
                        elementToFocus.focus();

                        if (activeElementSelection && activeElementSelection.start) {
                            elementToFocus.setSelection(activeElementSelection.start, activeElementSelection.end);
                        }
                    };

                    if (elementToFocus.length) {
                        refocus();

                        setTimeout(function () {
                            if (!elementToFocus.is(":focus")) {
                                refocus();
                            }
                        }, 50);
                    }
                }

                window.customFocus = false;
            }
        },

        ResponseProcessor: {

            doRedirect: function (node) {
                try {
                    window.location.assign(ajax.getContextPath() + (typeof(node) === 'string' ? node : node.getAttribute('url')));
                } catch (error) {
                    console.log('Error redirecting to URL: ' + node.getAttribute('url'));
                }
            },

            doUpdate: function (node, xhr) {
                var id = node.getAttribute('id'),
                    content = ajax.Utils.getContent(node);

                ajax.Utils.updateElement(id, content, xhr);
            },

            doEval: function (node) {
                var textContent = node.textContent || node.innerText || node.text;
                $.globalEval(textContent);
            },

            addCallbackParam: function (xhr) {
                var textContent = xhr.getResponseHeader('X-CallbackParam');
                if (xhr && textContent) {
                    if (xhr.pfArgs) {
                        var json = $.parseJSON(textContent);
                        for (var name in json) {
                            xhr.pfArgs[name] = json[name];
                        }
                    } else {
                        xhr.pfArgs = $.parseJSON(textContent);
                    }
                }
            },

            doExtension: function (node, xhr) {
                if (xhr) {
                    if (node.getAttribute("ln") === "ajax" && node.getAttribute("type") === "args") {
                        var textContent = node.textContent || node.innerText || node.text;
                        if (xhr.pfArgs) {
                            var json = $.parseJSON(textContent);
                            for (var name in json) {
                                xhr.pfArgs[name] = json[name];
                            }
                        } else {
                            xhr.pfArgs = $.parseJSON(textContent);
                        }
                    }
                }
            },

            doError: function (node, xhr) {
                // currently nothing...
            },

            doDelete: function (node) {
                var id = node.getAttribute('id');
                $(ajax.escapeClientId(id)).remove();
            },

            doInsert: function (node) {
                if (!node.childNodes) {
                    return false;
                }

                for (var i = 0; i < node.childNodes.length; i++) {
                    var childNode = node.childNodes[i];
                    var id = childNode.getAttribute('id');
                    var jq = $(ajax.escapeClientId(id));
                    var content = ajax.Utils.getContent(childNode);

                    if (childNode.nodeName === "after") {
                        $(content).insertAfter(jq);
                    }
                    else if (childNode.nodeName === "before") {
                        $(content).insertBefore(jq);
                    }
                }
            },

            doAttributes: function (node) {
                if (!node.childNodes) {
                    return false;
                }

                var id = node.getAttribute('id');
                var jq = $(ajax.escapeClientId(id));

                for (var i = 0; i < node.childNodes.length; i++) {
                    var attrNode = node.childNodes[i];
                    var attrName = attrNode.getAttribute("name");
                    var attrValue = attrNode.getAttribute("value");

                    if (!attrName) {
                        return;
                    }

                    if (!attrValue || attrValue === null) {
                        attrValue = "";
                    }

                    jq.attr(attrName, attrValue);
                }
            }
        },

        getForm: function (cfg) {
            var sourceId = ajax.getSourceId(cfg),
                form = null;

            if (cfg.formId) {
                form = ajax.expressions.SearchExpressionFacade.resolveComponentsAsSelector(cfg.formId);
            } else if (sourceId) {
                form = $(ajax.escapeClientId(sourceId)).closest('form');
            }

            if (form === null || form.length === 0) {
                return null;
            }

            return form;
        },

        getSourceId: function (cfg) {
            if (cfg.source) {
                if (typeof(cfg.source) === 'string') {
                    return cfg.source;
                } else {
                    return $(cfg.source).attr('id');
                }
            }

            return null;
        },

        getContextPath: function () {
            return $('meta[name=ctx]').attr('content') || '';
        },

        escapeClientId: function (id) {
            return "#" + id.replace(/:/g, "\\:");
        },

        isIE: function (version) {
            return ajax.env.isIE(version);
        },

        inArray: function (arr, item) {
            for (var i = 0; i < arr.length; i++) {
                if (arr[i] === item) {
                    return true;
                }
            }

            return false;
        },

        isNumber: function (value) {
            return typeof value === 'number' && isFinite(value);
        },

        getScript: function (url, callback) {
            $.ajax({
                type: "GET",
                url: url,
                success: callback,
                dataType: "script",
                cache: true,
                async: false
            });
        }
    };
}();
