var Registrar = function () {
    return {
        init: function () {
            var form = $("#for-registrar");

            form.validate({
                errorPlacement: function (label, element) {
                    element.addClass('invalid');
                    label.insertAfter(element);
                }
            });
        }
    };
}();