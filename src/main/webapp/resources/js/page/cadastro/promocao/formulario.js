var Promocao = function () {
    return {
        init: function () {
            var formId = 'form-promocao',
                form = $(ajax.escapeClientId(formId)),
                message = form,
                btnConfirm = $('#btn-confirm'),
                btnSubmit = $('#save'),
                btnCancel = $('#cancel'),
                addIngrediente = $('#add-ingrediente'),
                selectIngrediente = $('#select-ingrediente'),
                options = {
                    type: 'GET',
                    url: ajax.getContextPath() + '/ingrediente/listar',
                    dataType: 'json',
                    onsuccess: function (xhr, status, args) {
                        $.each(xhr.aaData, function (index, item) {
                            if ($('#ingredientes tbody tr input.id[value="' + item.id + '"]').length === 0) {
                                $('<option value="' + item.id + '">' + item.nome + '</option>').appendTo(selectIngrediente);
                            }
                        });
                        return true;
                    }
                },
                loading = ajax.Loading;

            ajax.Request.handle(options);

            loading.init();

            $('#ingredientes tbody tr input.quantidade').maskMoney({
                symbol: '',
                showSymbol: true,
                thousands: '.',
                decimal: ',',
                symbolStay: true,
                allowZero: false,
                precision: 2
            });

            $('#ingredientes tbody tr button.delete').click(function (e) {
                var data = $(this).closest('tr');

                $('<option value="' + data.find('input.id').val() + '">' + data.find('input.nome').val() + '</option>').appendTo(selectIngrediente);
                $(this).closest('tr').remove();

                if ($('#ingredientes tbody tr').length === 0) {
                    $('<tr class="empty-table"><td colspan="3">Nenhum Ingrediente Cadastrado</td></tr>').appendTo($('#ingredientes tbody'));
                }
            });

            selectIngrediente.change(function (e) {
                addIngrediente.prop("disabled", false);
            });

            addIngrediente.click(function (e) {
                $('#ingredientes tbody tr.empty-table').remove();
                var index = $('#ingredientes tbody tr').length,
                    intem = $('#select-ingrediente option:selected');

                $('<tr><td>' + intem.val() + '<input id="ingredientes' + index + '.ingrediente.id" name="ingredientes[' + index + '].ingrediente.id" value="' + intem.val() + '" type="hidden" class="id"></td><td>' + intem.text() + '<input id="ingredientes' + index + '.ingrediente.nome" name="ingredientes[' + index + '].ingrediente.nome" value="' + intem.text() + '" type="hidden" class="nome"></td><td><input id="ingredientes' + index + '.quantidade" name="ingredientes[' + index + '].quantidade" class="quantidade" required value="0,00" type="text"></td><td><button type="button" class="btn btn-space btn-danger btn-xs delete"><span>Excluir</span></button></td></tr>')
                    .appendTo($('#ingredientes tbody'));

                $('#ingredientes tbody tr:eq(' + index + ') input.quantidade').maskMoney({
                    symbol: '',
                    showSymbol: true,
                    thousands: '.',
                    decimal: ',',
                    symbolStay: true,
                    allowZero: false,
                    precision: 2
                });

                $('#ingredientes tbody tr:eq(' + index + ') button.delete').click(function (e) {
                    var data = $(this).closest('tr');

                    $('<option value="' + data.find('input.id').val() + '">' + data.find('input.nome').val() + '</option>').appendTo(selectIngrediente);
                    $(this).closest('tr').remove();

                    if ($('#ingredientes tbody tr').length === 0) {
                        $('<tr class="empty-table"><td colspan="3">Nenhum Ingrediente Cadastrado</td></tr>').appendTo($('#ingredientes tbody'));
                    }
                });

                intem.remove();
                addIngrediente.prop("disabled", true);
            });

            btnConfirm.click(function (e) {
                ajax.ResponseProcessor.doRedirect('/promocao');
            });

            form.validate({
                submitHandler: function () {
                    var options = {
                        source: formId,
                        params: [
                            {name: 'tipoDesconto', value: $('input[name=tipo-desconto]:checked').data('name')}
                        ],
                        onbeforesend: function (xhr, settings) {
                            loading.show();
                            btnSubmit.hide();
                            btnCancel.hide();
                        },
                        onerror: function (xhr, status, errorThrown) {
                            loading.hide();
                            btnSubmit.show();
                            btnCancel.show();
                            Message.danger(status, errorThrown, message);
                        },
                        oncomplete: function (xhr, status, args) {
                            if (status === 'success') {
                                App.init();
                                Promocao.init();
                            }
                        }
                    };

                    ajax.Request.handle(options);
                },

                errorPlacement: function (label, element) {
                    element.addClass('invalid');
                    label.insertAfter(element);
                }
            });
        }
    };
}();