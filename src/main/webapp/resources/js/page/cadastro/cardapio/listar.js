var Cardapio = function () {
    return {
        init: function () {
            var formId = 'confirm-delete-form',
                form = $(ajax.escapeClientId(formId)),
                message = form.children('.modal-body'),
                modal = $('#confirm-delete'),
                insert = $('#insert'),
                btnCancel = $('#btn-cancel'),
                btnConfirm = $('#btn-confirm'),
                loading = ajax.Loading,
                table = $('#cardapio').DataTable({
                    processing: true,
                    serverSide: true,
                    filter: false,
                    displayLength: 20,
                    lengthChange: false,
                    paginate: true,
                    ordering: false,
                    paginationType: 'full_numbers',
                    ajax: {
                        url: ajax.getContextPath() + '/cardapio/listar',
                        type: 'POST',
                        data: {
                            _csrf: $('meta[name=csrf]').attr('content')
                        }
                    },
                    columns: [
                        {data: 'id'},
                        {data: 'nome'},
                        {data: 'observacao'},
                        {
                            data: 'valor',
                            render: $.fn.dataTable.render.number('.', ',', 2)
                        },
                        {
                            data: null,
                            defaultContent: '<button class="btn btn-space btn-warning btn-xs edit"><span>Editar</span></button><button class="btn btn-space btn-danger btn-xs delete" data-toggle="modal" data-target="#confirm-delete"><span>Excluir</span></button>',
                            orderable: false
                        }
                    ]
                });

            loading.init();

            $('#cardapio tbody').on('click', 'button.edit', function () {
                var data = table.row($(this).parents('tr')).data();
                ajax.ResponseProcessor.doRedirect('/cardapio/' + data.id);
            });

            insert.click(function (e) {
                ajax.ResponseProcessor.doRedirect('/cardapio/cadastrar');
            });

            modal.on('show.bs.modal', function (e) {
                var data = table.row($(e.relatedTarget).parents('tr')).data();
                form.data('record-id', data.id);
            });

            form.validate({
                submitHandler: function () {
                    var options = {
                        source: formId,
                        url: form.attr('action') + form.data('record-id'),
                        onbeforesend: function (xhr, settings) {
                            loading.show();
                            btnCancel.hide();
                            btnConfirm.hide();
                        },
                        onerror: function (xhr, status, errorThrown) {
                            loading.hide();
                            btnCancel.show();
                            btnConfirm.show();
                            Message.danger(status, errorThrown, message);
                        },
                        onsuccess: function (data, status, xhr) {
                            loading.hide();
                            btnCancel.show();
                            btnConfirm.show();
                            modal.modal('hide');
                            Message.success('Exclusão', 'Item excluído com sucesso!', $('#message'));
                            table.ajax.reload();
                            return true;
                        }
                    };

                    ajax.Request.handle(options);
                }
            });
        }
    };
}();