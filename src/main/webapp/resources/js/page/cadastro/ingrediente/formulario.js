var Ingrediente = function () {
    return {
        init: function () {
            var formId = 'form-ingrediente',
                form = $(ajax.escapeClientId(formId)),
                message = form,
                btnConfirm = $('#btn-confirm'),
                btnSubmit = $('#save'),
                btnCancel = $('#cancel'),
                loading = ajax.Loading;

            loading.init();

            btnConfirm.click(function (e) {
                ajax.ResponseProcessor.doRedirect('/ingrediente');
            });

            form.validate({
                submitHandler: function () {
                    var options = {
                        source: formId,
                        onbeforesend: function (xhr, settings) {
                            loading.show();
                            btnSubmit.hide();
                            btnCancel.hide();
                        },
                        onerror: function (xhr, status, errorThrown) {
                            loading.hide();
                            btnSubmit.show();
                            btnCancel.show();
                            Message.danger(status, errorThrown, message);
                        },
                        oncomplete: function (xhr, status, args) {
                            if (status === 'success') {
                                App.init();
                                Ingrediente.init();
                            }
                        }
                    };

                    ajax.Request.handle(options);
                },

                errorPlacement: function (label, element) {
                    element.addClass('invalid');
                    label.insertAfter(element);
                }
            });
        }
    };
}();