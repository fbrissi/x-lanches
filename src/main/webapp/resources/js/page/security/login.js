var Login = function () {
    return {
        init: function () {
            var form = $("#login-form");

            form.validate({
                errorPlacement: function (label, element) {
                    element.addClass('invalid');
                    label.insertAfter(element);
                }
            });
        }
    };
}();