package br.com.finch.xlanches.web.infrastructure.persistence.jpa.repository;

import br.com.finch.xlanches.web.domain.model.usuario.Papel;
import br.com.finch.xlanches.web.domain.model.usuario.PapelRepository;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 * @author Filipe Bojikian Rissi
 */
@Named
@RequestScoped
public class PapelRepositoryImpl extends AbstractRepository<Papel, Integer> implements PapelRepository {

    public PapelRepositoryImpl() {
    }

    @Inject
    public PapelRepositoryImpl(EntityManager entityManager) {
        super(entityManager);
    }


    @Override
    public Papel findByTipo(Papel.Tipo tipo) {
        TypedQuery<Papel> query = createNamedQuery(PapelRepository.findByTipo, new Parameter("tipo", tipo));
        try {
            return query.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

}