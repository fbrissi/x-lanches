package br.com.finch.xlanches.web.infrastructure.persistence.jpa.repository;

import br.com.finch.xlanches.web.domain.model.localizacao.Estado;
import br.com.finch.xlanches.web.domain.model.localizacao.EstadoRepository;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

/**
 * @author Filipe Bojikian Rissi
 */
@Named
@RequestScoped
public class EstadoRepositoryImpl extends AbstractRepository<Estado, Integer> implements EstadoRepository {

    protected EstadoRepositoryImpl() {
    }

    @Inject
    public EstadoRepositoryImpl(EntityManager entityManager) {
        super(entityManager);
    }

}