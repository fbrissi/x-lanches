package br.com.finch.xlanches.web.infrastructure.validation.annotation;

import br.com.finch.xlanches.web.infrastructure.validation.logic.PassWordConfirmValidatorLogic;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Filipe Bojikian Rissi
 */
@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = PassWordConfirmValidatorLogic.class)
@Documented
public @interface PassWordConfirm {

    String message() default "{xlanches.validator.constraints.Senha.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * @return The password field
     */
    String password();

    /**
     * @return The confirm password field
     */
    String confirm();

}