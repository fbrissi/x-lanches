package br.com.finch.xlanches.web.domain.model.usuario;

import br.com.finch.xlanches.web.domain.model.exception.SenhaInvalidaException;
import br.com.finch.xlanches.web.domain.model.shared.ValuesObject;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Size;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

/**
 * @author Filipe Bojikian Rissi
 */
@Embeddable
public class RedefinirSenha implements ValuesObject<RedefinirSenha> {

    private static final long serialVersionUID = 7026088378788016610L;

    public static final int TEMPO_EXPIRA_RESET_SENHA = 48;

    public static final RedefinirSenha NULL = new RedefinirSenha();

    @Column(name = "criado")
    private final LocalDateTime criado;

    @Column(name = "expira")
    private final LocalDateTime expira;

    @Size(min = 20, max = 64)
    @Column(name = "senha")
    private final String senha;

    protected RedefinirSenha() {
        super();
        this.criado = null;
        this.expira = null;
        this.senha = null;
    }

    protected RedefinirSenha(String senha) {
        super();
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        this.senha = passwordEncoder.encode(senha);
        this.criado = LocalDateTime.now();
        this.expira = criado.plusHours(RedefinirSenha.TEMPO_EXPIRA_RESET_SENHA);
    }

    public LocalDateTime getCriado() {
        return criado;
    }

    public LocalDateTime getExpira() {
        return expira;
    }

    public void autenticar(String senha) throws SenhaInvalidaException {
        assert (senha != null);

        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        if (!passwordEncoder.matches(senha, this.senha))
            throw new SenhaInvalidaException();
    }

    public boolean isValid() {
        if (criado == null)
            return false;

        if (expira == null)
            return false;

        if (senha == null)
            return false;

        if (expira.isBefore(LocalDateTime.now()))
            return false;

        return true;
    }

    public static final String novaSenha() {
        return Long.toString(ByteBuffer.wrap(UUID.randomUUID().toString().getBytes(StandardCharsets.UTF_8)).getLong(), Character.MAX_RADIX);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(this.criado)
                .append(this.expira)
                .append(this.senha).toHashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (object == null)
            return false;

        if (this == object)
            return true;

        if (!(object instanceof RedefinirSenha))
            return false;

        RedefinirSenha other = (RedefinirSenha) object;
        return sameValue(other);
    }

    @Override
    public boolean sameValue(RedefinirSenha other) {
        return new EqualsBuilder()
                .append(this.criado, other.criado)
                .append(this.expira, other.expira)
                .append(this.senha, other.senha).isEquals();
    }

    @Override
    public int compareTo(RedefinirSenha other) {
        return new CompareToBuilder()
                .append(this.criado, other.criado)
                .append(this.expira, other.expira)
                .append(this.senha, other.senha).toComparison();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(getClass().getSimpleName());
        sb.append('{');
        sb.append("criado=").append(criado.format(DateTimeFormatter.ISO_DATE_TIME));
        sb.append(", expira=").append(expira.format(DateTimeFormatter.ISO_DATE_TIME));
        sb.append(", senha='").append(senha).append('\'');
        sb.append('}');
        return sb.toString();
    }

}