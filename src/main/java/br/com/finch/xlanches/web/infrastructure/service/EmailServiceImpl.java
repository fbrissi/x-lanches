package br.com.finch.xlanches.web.infrastructure.service;

import br.com.finch.xlanches.web.domain.service.Attached;
import br.com.finch.xlanches.web.domain.service.EmailProxy;
import br.com.finch.xlanches.web.domain.service.EmailService;
import br.com.finch.xlanches.web.infrastructure.cdi.SessionProduces;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.inject.Named;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.util.ByteArrayDataSource;
import javax.naming.NamingException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * @author Filipe Bojikian Rissi
 */
@Named
public class EmailServiceImpl implements EmailService {

    private static final Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);

    private static final String MAIL_SMTP_CONNECTIONTIMEOUT = "mail.smtp.connectiontimeout";
    private static final String MAIL_SMTP_TIMEOUT = "mail.smtp.timeout";

    private static final String TEXT_HTML = "text/html";

    private static final int socketTimeout = 5000;
    private static final int socketConnectionTimeout = 5000;


    private String subtype = TEXT_HTML;

    private final StringBuilder replyTo = new StringBuilder();
    private final StringBuilder to = new StringBuilder();
    private final StringBuilder bcc = new StringBuilder();
    private final StringBuilder cc = new StringBuilder();

    private final List<Attached> listAttached = new ArrayList<>();

    public class EmailProxyImpl implements EmailProxy {

        private final Session session;
        private final String from;

        public EmailProxyImpl(Session session) {
            this.session = session;
            this.from = session.getProperty("mail.from");
        }

        @Override
        public void send(String subject, Path html) throws MessagingException, IOException {
            send(subject, html, new HashMap<>());
        }

        @Override
        public void send(String subject, Path html, Map<String, String> replace) throws MessagingException, IOException {
            StringBuilder body = new StringBuilder();

            try (BufferedReader reader = Files.newBufferedReader(html)) {
                String linha;
                while ((linha = reader.readLine()) != null) {
                    if (linha.startsWith("<?") || linha.endsWith("?>"))
                        continue;
                    linha = linha.replaceAll("[../]+assets", session.getProperty("mail.base.url"));
                    for (Map.Entry<String, String> entry : replace.entrySet()) {
                        if (linha.contains(entry.getKey()))
                            linha = linha.replace(entry.getKey(), entry.getValue());
                    }
                    body.append(linha);
                }
            }

            if (to.length() == 0 && cc.length() == 0 && bcc.length() == 0) {
                throw new MessagingException("E-mail não foi direcionado para nenhum endereço e-mail.");
            }

            Properties properties = new Properties(System.getProperties());

            if (socketTimeout > 0) {
                properties.setProperty(MAIL_SMTP_TIMEOUT, Integer.toString(socketTimeout));
            }

            if (socketConnectionTimeout > 0) {
                properties.setProperty(MAIL_SMTP_CONNECTIONTIMEOUT, Integer.toString(socketConnectionTimeout));
            }

            List<InternetAddress> replyTo = new LinkedList<>();
            List<InternetAddress> to = new LinkedList<>();
            List<InternetAddress> bcc = new LinkedList<>();
            List<InternetAddress> cc = new LinkedList<>();

            InternetAddress addressFrom;
            try {
                String[] fromAddress = from.split("\\s*:\\s*");
                if (fromAddress.length >= 2)
                    addressFrom = new InternetAddress(fromAddress[0], fromAddress[1]);
                else
                    addressFrom = new InternetAddress(fromAddress[0]);
                addressFrom.validate();

                for (String value : EmailServiceImpl.this.replyTo.toString().split("\\s*,\\s*")) {
                    if ("".equals(value))
                        continue;
                    replyTo.add(createInternetAddress(value));
                }

                for (String value : EmailServiceImpl.this.to.toString().split("\\s*,\\s*")) {
                    if ("".equals(value))
                        continue;
                    to.add(createInternetAddress(value));
                }

                for (String value : EmailServiceImpl.this.bcc.toString().split("\\s*,\\s*")) {
                    if ("".equals(value))
                        continue;
                    bcc.add(createInternetAddress(value));
                }

                for (String value : EmailServiceImpl.this.cc.toString().split("\\s*,\\s*")) {
                    if ("".equals(value))
                        continue;
                    cc.add(createInternetAddress(value));
                }
            } catch (UnsupportedEncodingException | AddressException e) {
                logger.error(e.getMessage(), e);
                throw new MessagingException(e.getMessage());
            }

            MimeMessage message = new MimeMessage(session);
            Multipart multipart = new MimeMultipart();

            message.setFrom(addressFrom);

            message.setReplyTo(replyTo.toArray(new InternetAddress[0]));
            message.setRecipients(Message.RecipientType.TO, to.toArray(new InternetAddress[0]));
            message.setRecipients(Message.RecipientType.BCC, bcc.toArray(new InternetAddress[0]));
            message.setRecipients(Message.RecipientType.CC, cc.toArray(new InternetAddress[0]));

            if (!listAttached.isEmpty()) {
                for (Attached attached : listAttached) {
                    MimeBodyPart attachedPart = new MimeBodyPart();
                    DataSource source = new ByteArrayDataSource(attached.getFile(), attached.getType());
                    attachedPart.setDataHandler(new DataHandler(source));
                    attachedPart.setFileName(attached.getName());
                    multipart.addBodyPart(attachedPart);
                }
            }

            message.setSubject(subject);
            message.setSentDate(new Date());

            MimeBodyPart textPart = new MimeBodyPart();
            textPart.setContent(body.toString(), subtype + "; charset=" + StandardCharsets.UTF_8.name());
            multipart.addBodyPart(textPart);

            message.setContent(multipart);

            Transport.send(message);
        }
    }

    private InternetAddress createInternetAddress(String value) throws AddressException, UnsupportedEncodingException {
        String[] address = value.split("\\s*:\\s*");
        if (address.length == 1) {
            InternetAddress internetAddress = new InternetAddress(address[0]);
            internetAddress.validate();
            return internetAddress;
        } else if (address.length == 2) {
            InternetAddress internetAddress = new InternetAddress(address[0], address[1]);
            internetAddress.validate();
            return internetAddress;
        }

        throw new AddressException();
    }

    @Override
    public EmailProxy getProxy() {
        try {
            return new EmailProxyImpl(SessionProduces.getSession());
        } catch (NamingException e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException();
        }
    }

    @Override
    public void addAttached(Attached attached) {
        listAttached.add(attached);
    }

    @Override
    public void addReplyTo(String to) {
        if (to == null)
            return;
        if (this.replyTo.length() > 0)
            this.replyTo.append(", ");
        this.replyTo.append(to);
    }

    @Override
    public void addTo(String to) {
        if (to == null)
            return;
        if (this.to.length() > 0)
            this.to.append(", ");
        this.to.append(to);
    }

    @Override
    public void addBcc(String bcc) {
        if (bcc == null)
            return;
        if (this.bcc.length() > 0)
            this.bcc.append(", ");
        this.bcc.append(bcc);
    }

    @Override
    public void addCc(String cc) {
        if (cc == null)
            return;
        if (this.cc.length() > 0)
            this.cc.append(", ");
        this.cc.append(cc);
    }

    @Override
    public void clearAttached() {
        this.listAttached.clear();
    }

    @Override
    public void clearReplyTo() {
        this.replyTo.setLength(0);
    }

    @Override
    public void clearTo() {
        this.to.setLength(0);
    }

    @Override
    public void clearBcc() {
        this.bcc.setLength(0);
    }

    @Override
    public void clearCc() {
        this.cc.setLength(0);
    }

}