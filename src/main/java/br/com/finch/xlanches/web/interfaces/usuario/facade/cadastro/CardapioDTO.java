package br.com.finch.xlanches.web.interfaces.usuario.facade.cadastro;

import br.com.finch.xlanches.web.domain.model.vendas.Cardapio;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Filipe Bojikian Rissi
 */
public class CardapioDTO implements Serializable {

    private static final long serialVersionUID = -7801067560625455630L;

    private Integer id;

    @NotNull
    @Size(max = 55)
    private String nome;

    @Size(max = 180)
    private String observacao;

    @NotNull
    private Integer promocaoId;

    private Map<Integer, BigDecimal> ingredientes;

    public CardapioDTO() {
    }

    public CardapioDTO(Cardapio cardapio) {
        if (cardapio != null) {
            this.id = cardapio.getId();
            this.nome = cardapio.getNome();
            this.observacao = cardapio.getObservacao();
            this.promocaoId = cardapio.getPromocao().getId();
            this.ingredientes = new HashMap<>();
            cardapio.getIngredientes().forEach(item -> this.ingredientes.put(item.getIngrediente().getId(), item.getValor()));
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Integer getPromocaoId() {
        return promocaoId;
    }

    public void setPromocaoId(Integer promocaoId) {
        this.promocaoId = promocaoId;
    }

    public Map<Integer, BigDecimal> getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(Map<Integer, BigDecimal> ingredientes) {
        this.ingredientes = ingredientes;
    }

}
