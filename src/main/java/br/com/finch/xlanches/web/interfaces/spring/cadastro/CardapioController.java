package br.com.finch.xlanches.web.interfaces.spring.cadastro;

import br.com.finch.xlanches.web.application.CardapioService;
import br.com.finch.xlanches.web.interfaces.spring.error.EntityNotFoundException;
import br.com.finch.xlanches.web.interfaces.usuario.facade.cadastro.CardapioDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.validation.Valid;

/**
 * @author Filipe Bojikian Rissi
 */
@Controller
@RequestMapping(value = "/cardapio")
public class CardapioController {

    @Inject
    private CardapioService cardapioService;

    @RequestMapping(method = RequestMethod.GET)
    public String init() {
        return "cadastro/cardapio/listar";
    }

    @ResponseBody
    @RequestMapping(value = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String listar(int start, int length) {
        JsonObject jsonObject = cardapioService.listar(start, length);
        return jsonObject.toString();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String editar(@PathVariable("id") Integer id, Model model) {
        CardapioDTO cardapio = cardapioService.editar(id);
        if (cardapio == null) {
            throw new EntityNotFoundException();
        }
        model.addAttribute("cardapio", cardapio);
        return "cadastro/cardapio/formulario";
    }

    @RequestMapping(value = "/cadastrar", method = RequestMethod.GET)
    public String cadastrar(Model model) {
        model.addAttribute("cardapio", new CardapioDTO());
        return "cadastro/cardapio/formulario";
    }

    @RequestMapping(value = "/cadastrar", method = RequestMethod.PUT)
    public String alterar(@ModelAttribute("cardapio") @Valid CardapioDTO cardapio, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "cadastro/cardapio/formulario :: form";
        }
        cardapioService.alterar(cardapio);
        model.addAttribute("redirect", "/cardapio");
        return "redirect :: url";
    }

    @RequestMapping(value = "/cadastrar", method = RequestMethod.POST)
    public String persistir(@ModelAttribute("cardapio") @Valid CardapioDTO cardapio, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "cadastro/cardapio/formulario :: form";
        }
        cardapioService.persistir(cardapio);
        model.addAttribute("redirect", "/cardapio");
        return "redirect :: url";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deletar(@PathVariable("id") Integer id) {
        cardapioService.deletar(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}