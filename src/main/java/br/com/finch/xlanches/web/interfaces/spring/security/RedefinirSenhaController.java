package br.com.finch.xlanches.web.interfaces.spring.security;

import br.com.finch.xlanches.web.application.UsuarioService;
import br.com.finch.xlanches.web.domain.model.exception.ConfirmarSenhaException;
import br.com.finch.xlanches.web.domain.model.exception.SenhaInvalidaException;
import br.com.finch.xlanches.web.domain.model.usuario.Papel;
import br.com.finch.xlanches.web.infrastructure.spring.security.authentication.User;
import br.com.finch.xlanches.web.interfaces.usuario.facade.usuario.ResetarSenhaDTO;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;
import javax.validation.Valid;

/**
 * @author Filipe Bojikian Rissi
 */
@Controller
@RequestMapping(value = "/redefinir_senha")
public class RedefinirSenhaController {

    @Inject
    private UsuarioService usuarioService;

    @RequestMapping(method = RequestMethod.GET)
    public String init(Model model) {
        model.addAttribute("resetarSenha", new ResetarSenhaDTO());
        return "security/redefinir_senha";
    }

    @RequestMapping(method = RequestMethod.PUT)
    public String redefinirSenha(@AuthenticationPrincipal User usuario, @ModelAttribute("resetarSenha") @Valid ResetarSenhaDTO resetarSenha, Model model) throws SenhaInvalidaException, ConfirmarSenhaException {
        usuarioService.alterarSenha(resetarSenha.getSenha(), resetarSenha.getConfirmarNovaSenha(), resetarSenha.getConfirmarNovaSenha());

        if (Papel.Tipo.ADMIN.equals(usuario.getPrincipal().getPapel().getTipo()))
            model.addAttribute("redirect", "/administracao");
        else if (Papel.Tipo.USER.equals(usuario.getPrincipal().getPapel().getTipo()))
            model.addAttribute("redirect", "/usuario");
        return "redirect :: url";
    }

}