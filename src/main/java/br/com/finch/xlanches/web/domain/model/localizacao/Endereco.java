/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.finch.xlanches.web.domain.model.localizacao;

import br.com.finch.xlanches.web.domain.model.identificacao.CEP;
import br.com.finch.xlanches.web.domain.model.shared.ValuesObject;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Filipe Bojikian Rissi
 */
@Embeddable
public class Endereco implements ValuesObject<Endereco> {

    private static final long serialVersionUID = 9206063568617486383L;

    @Basic(optional = false)
    @NotNull
    @Size(max = 185)
    @Column(name = "logradouro")
    private final String logradouro;

    @Size(max = 15)
    @Column(name = "numero")
    private final String numero;

    @Size(max = 85)
    @Column(name = "complemento")
    private final String complemento;

    @Size(max = 155)
    @Column(name = "referencia")
    private final String referencia;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 185)
    @Column(name = "bairro")
    private final String bairro;

    @Basic(optional = false)
    @NotNull
    @Column(name = "cep")
    private final CEP cep;

    @NotNull
    @JoinColumn(name = "cidade_id", referencedColumnName = "id")
    @ManyToOne(cascade = CascadeType.DETACH, optional = false)
    private final Cidade cidade;

    protected Endereco() {
        super();
        this.logradouro = null;
        this.numero = null;
        this.complemento = null;
        this.referencia = null;
        this.cep = null;
        this.bairro = null;
        this.cidade = null;
    }

    public Endereco(String logradouro, String numero, CEP cep, String bairro, Cidade cidade) {
        this(logradouro, numero, null, null, cep, bairro, cidade);
    }

    public Endereco(String logradouro, String numero, String complemento, String referencia, CEP cep, String bairro, Cidade cidade) {
        super();
        assert (logradouro != null);
        assert (numero != null);
        assert (cep != null);
        assert (bairro != null);
        assert (cidade != null);

        this.logradouro = logradouro;
        this.numero = numero;
        this.complemento = complemento;
        this.referencia = referencia;
        this.cep = cep;
        this.bairro = bairro;
        this.cidade = cidade;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public String getReferencia() {
        return referencia;
    }

    public CEP getCep() {
        return cep;
    }

    public String getBairro() {
        return bairro;
    }

    public Cidade getCidade() {
        return cidade;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(this.logradouro)
                .append(this.numero)
                .append(this.complemento)
                .append(this.referencia)
                .append(this.cep)
                .append(this.bairro)
                .append(this.cidade).toHashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (object == null)
            return false;

        if (this == object)
            return true;

        if (!(object instanceof Endereco))
            return false;

        Endereco other = (Endereco) object;
        return sameValue(other);
    }

    @Override
    public boolean sameValue(Endereco other) {
        return new EqualsBuilder()
                .append(this.logradouro, other.logradouro)
                .append(this.numero, other.numero)
                .append(this.complemento, other.complemento)
                .append(this.referencia, other.referencia)
                .append(this.cep, other.cep)
                .append(this.bairro, other.bairro)
                .append(this.cidade, other.cidade).isEquals();
    }

    @Override
    public int compareTo(Endereco other) {
        return new CompareToBuilder()
                .append(this.logradouro, other.logradouro)
                .append(this.numero, other.numero)
                .append(this.complemento, other.complemento)
                .append(this.referencia, other.referencia)
                .append(this.cep, other.cep)
                .append(this.bairro, other.bairro)
                .append(this.cidade, other.cidade).toComparison();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(getClass().getSimpleName());
        sb.append('{');
        sb.append("logradouro='").append(logradouro).append('\'');
        sb.append(", numero='").append(numero).append('\'');
        sb.append(", cep='").append(cep).append('\'');
        sb.append(", bairro='").append(bairro).append('\'');
        sb.append('}');
        return sb.toString();
    }

}