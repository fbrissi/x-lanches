package br.com.finch.xlanches.web.infrastructure.persistence.interceptor;

import br.com.finch.xlanches.web.infrastructure.cdi.DataSourceProduces;
import br.com.finch.xlanches.web.infrastructure.persistence.exception.PersistenceMessageException;
import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.springframework.jdbc.support.SQLErrorCodeSQLExceptionTranslator;
import org.springframework.jdbc.support.SQLExceptionTranslator;
import org.springframework.jdbc.support.SQLStateSQLExceptionTranslator;

import javax.annotation.Priority;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.persistence.PersistenceException;
import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * @author Filipe Bojikian Rissi
 */
@Interceptor
@Transactional
@Priority(Interceptor.Priority.PLATFORM_BEFORE)
public class TransactionInterceptor {

    @AroundInvoke
    public Object intercept(InvocationContext context) throws Exception {
        try {
            return context.proceed();
        } catch (PersistenceException ex) {
            SQLExceptionTranslator sqlExceptionTranslator = getExceptionTranslator(DataSourceProduces.getDataSource("java:/comp/env/jdbc/xlanches-ds-postgres"));
            throw new PersistenceMessageException(sqlExceptionTranslator.translate(context.getTarget().getClass().getName(), null, getSQLException(ex)));
        }
    }

    private SQLException getSQLException(PersistenceException ex) {
        Throwable rootCause = null;
        Throwable cause = ex.getCause();
        while (cause != null && (cause != rootCause) && !(rootCause instanceof SQLException)) {
            rootCause = cause;
            cause = cause.getCause();
        }
        return (SQLException) rootCause;
    }

    private SQLExceptionTranslator getExceptionTranslator(DataSource dataSource) {
        if (dataSource != null) {
            return new SQLErrorCodeSQLExceptionTranslator(dataSource);
        } else {
            return new SQLStateSQLExceptionTranslator();
        }
    }

}
