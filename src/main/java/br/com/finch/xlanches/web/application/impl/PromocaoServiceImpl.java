package br.com.finch.xlanches.web.application.impl;

import br.com.finch.xlanches.web.application.PromocaoService;
import br.com.finch.xlanches.web.domain.model.vendas.IngredienteRepository;
import br.com.finch.xlanches.web.domain.model.vendas.Promocao;
import br.com.finch.xlanches.web.domain.model.vendas.PromocaoRepository;
import br.com.finch.xlanches.web.infrastructure.util.JSONCreate;
import br.com.finch.xlanches.web.interfaces.usuario.facade.cadastro.PromocaoDTO;
import org.apache.deltaspike.jpa.api.transaction.Transactional;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.json.JsonObject;
import java.util.List;

/**
 * @author by Filipe Bojikian Rissi
 */
@Named
@RequestScoped
public class PromocaoServiceImpl implements PromocaoService {

    private final PromocaoRepository promocaoRepository;
    private final IngredienteRepository ingredienteRepository;

    protected PromocaoServiceImpl() {
        promocaoRepository = null;
        ingredienteRepository = null;
    }

    @Inject
    public PromocaoServiceImpl(PromocaoRepository promocaoRepository, IngredienteRepository ingredienteRepository) {
        this.promocaoRepository = promocaoRepository;
        this.ingredienteRepository = ingredienteRepository;
    }

    @Override
    public JsonObject listar(int page, int result) {
        List<Promocao> list = promocaoRepository.list(page, result);
        return JSONCreate.toJSONList(list, promocaoRepository.count());
    }

    @Override
    public PromocaoDTO editar(Integer id) {
        Promocao promocao = promocaoRepository.find(id);
        return promocao == null ? null : new PromocaoDTO(promocao);
    }

    @Override
    @Transactional
    public void persistir(PromocaoDTO promocaoDTO) {
        Promocao promocao = new Promocao(promocaoDTO.getNome(),
                promocaoDTO.getDesconto(), promocaoDTO.getTipoDesconto());
        promocaoDTO.getIngredientes().forEach(item -> promocao.addIngrediente(item.getQuantidade(), ingredienteRepository.find(item.getIngrediente().getId())));
        promocaoRepository.persist(promocao);
    }

    @Override
    @Transactional
    public void alterar(PromocaoDTO promocaoDTO) {
        Promocao promocao = promocaoRepository.find(promocaoDTO.getId());
        promocao.setNome(promocaoDTO.getNome());
        promocao.setDesconto(promocaoDTO.getDesconto());
        promocao.setTipoDesconto(promocaoDTO.getTipoDesconto());
        promocao.removerTodosIngrediente();
        promocaoDTO.getIngredientes().forEach(item -> promocao.addIngrediente(item.getQuantidade(), ingredienteRepository.find(item.getIngrediente().getId())));
    }

    @Override
    @Transactional
    public void deletar(Integer id) {
        Promocao promocao = promocaoRepository.find(id);
        promocaoRepository.remove(promocao);
    }

}
