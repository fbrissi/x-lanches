package br.com.finch.xlanches.web.application.impl;

import br.com.finch.xlanches.web.application.IngredienteService;
import br.com.finch.xlanches.web.domain.model.vendas.Ingrediente;
import br.com.finch.xlanches.web.domain.model.vendas.IngredienteRepository;
import br.com.finch.xlanches.web.infrastructure.util.JSONCreate;
import br.com.finch.xlanches.web.interfaces.usuario.facade.cadastro.IngredienteDTO;
import org.apache.deltaspike.jpa.api.transaction.Transactional;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.json.JsonObject;
import java.util.List;

/**
 * @author by Filipe Bojikian Rissi
 */
@Named
@RequestScoped
public class IngredienteServiceImpl implements IngredienteService {

    private final IngredienteRepository ingredienteRepository;

    protected IngredienteServiceImpl() {
        ingredienteRepository = null;
    }

    @Inject
    public IngredienteServiceImpl(IngredienteRepository ingredienteRepository) {
        this.ingredienteRepository = ingredienteRepository;
    }

    @Override
    public JsonObject listarTodos() {
        List<Ingrediente> list = ingredienteRepository.listAll();
        return JSONCreate.toJSONList(list, ingredienteRepository.count());
    }

    @Override
    public JsonObject listar(int page, int result) {
        List<Ingrediente> list = ingredienteRepository.list(page, result);
        return JSONCreate.toJSONList(list, ingredienteRepository.count());
    }

    @Override
    public IngredienteDTO editar(Integer id) {
        Ingrediente ingrediente = ingredienteRepository.find(id);
        return ingrediente == null ? null : new IngredienteDTO(ingrediente);
    }

    @Override
    @Transactional
    public void persistir(IngredienteDTO ingredienteDTO) {
        Ingrediente ingrediente = new Ingrediente(ingredienteDTO.getNome(),
                ingredienteDTO.getObservacao(), ingredienteDTO.getValor());
        ingredienteRepository.persist(ingrediente);
    }

    @Override
    @Transactional
    public void alterar(IngredienteDTO ingredienteDTO) {
        Ingrediente ingrediente = ingredienteRepository.find(ingredienteDTO.getId());
        ingrediente.setNome(ingredienteDTO.getNome());
        ingrediente.setObservacao(ingredienteDTO.getObservacao());
        ingrediente.setValor(ingredienteDTO.getValor());
    }

    @Override
    @Transactional
    public void deletar(Integer id) {
        Ingrediente ingrediente = ingredienteRepository.find(id);
        ingredienteRepository.remove(ingrediente);
    }

}
