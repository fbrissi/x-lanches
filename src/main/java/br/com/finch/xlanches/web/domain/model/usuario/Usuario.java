package br.com.finch.xlanches.web.domain.model.usuario;

import br.com.finch.xlanches.web.domain.model.contato.Telefone;
import br.com.finch.xlanches.web.domain.model.exception.ConfirmarSenhaException;
import br.com.finch.xlanches.web.domain.model.exception.SenhaInvalidaException;
import br.com.finch.xlanches.web.domain.model.localizacao.Endereco;
import br.com.finch.xlanches.web.domain.model.shared.AuditingEntity;
import br.com.finch.xlanches.web.infrastructure.validation.annotation.TelefoneValid;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author Filipe Bojikian Rissi
 */
@Entity
@Table(name = "usuario")
@SecondaryTable(name = "redefinir_senha", pkJoinColumns = @PrimaryKeyJoinColumn(name = "usuario_id", referencedColumnName = "id"))
@SequenceGenerator(name = "usuario_id_seq", sequenceName = "usuario_id_seq", allocationSize = 1)
@NamedQueries({
        @NamedQuery(name = UsuarioRepository.findByEmail, query = "SELECT u FROM Usuario u WHERE u.email = :email")
})
public class Usuario extends AuditingEntity<Usuario, Integer> {

    private static final long serialVersionUID = -4399917150319026921L;

    @Id
    @GeneratedValue(generator = "usuario_id_seq")
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 20, max = 64)
    @Column(name = "senha")
    private String senha;

    @Pattern(regexp = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message = "{xlanches.validator.constraints.Email.message}")
    @Basic(optional = false)
    @NotNull
    @Size(min = 8, max = 180)
    @Column(name = "email")
    private String email;

    @Basic(optional = false)
    @NotNull
    @Size(max = 32)
    @Column(name = "nome")
    private String nome;

    @TelefoneValid
    @Column(name = "telefone")
    private Telefone telefone;

    @Basic(optional = false)
    @NotNull
    @Column(name = "registrado", insertable = false, updatable = false)
    private LocalDateTime registrado;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "criado", column = @Column(table = "redefinir_senha")),
            @AttributeOverride(name = "expira", column = @Column(table = "redefinir_senha")),
            @AttributeOverride(name = "senha", column = @Column(table = "redefinir_senha"))
    })
    private RedefinirSenha redefinirSenha;

    @ElementCollection
    @MapKeyColumn(name = "tipo")
    @CollectionTable(name = "endereco", joinColumns = @JoinColumn(name = "usuario_id", referencedColumnName = "id"))
    private Map<String, Endereco> enderecos;

    @NotNull
    @ManyToOne(optional = false, cascade = CascadeType.DETACH)
    private Papel papel;

    protected Usuario() {
        super();
    }

    protected Usuario(String email, String nome, Telefone telefone, Papel papel) {
        super();
        assert (email != null);
        assert (nome != null);

        this.email = email;
        this.nome = nome;
        this.telefone = telefone;
        this.registrado = LocalDateTime.now();
        this.papel = papel;
        this.enderecos = new HashMap<>();
    }

    @Override
    public Integer getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Telefone getTelefone() {
        return telefone;
    }

    public void setTelefone(Telefone telefone) {
        this.telefone = telefone;
    }

    public LocalDateTime getRegistrado() {
        return registrado;
    }

    protected RedefinirSenha getRedefinirSenha() {
        return redefinirSenha;
    }

    public Map<String, Endereco> getEnderecos() {
        return enderecos;
    }

    public Papel getPapel() {
        return papel;
    }

    public String redefinirSenha() {
        final String senha = RedefinirSenha.novaSenha();
        redefinirSenha = new RedefinirSenha(senha);
        return senha;
    }

    protected void atribuirSenha(String senha, String confirmarSenha) throws SenhaInvalidaException {
        assert (senha != null);
        assert (confirmarSenha != null);

        if (!senha.equals(confirmarSenha))
            throw new SenhaInvalidaException();
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        this.senha = passwordEncoder.encode(senha);
    }

    public void alterarSenha(String senha, String novaSenha, String confirmarSenha) throws SenhaInvalidaException, ConfirmarSenhaException {
        assert (senha != null);
        assert (novaSenha != null);
        assert (confirmarSenha != null);

        if (!novaSenha.equals(confirmarSenha))
            throw new ConfirmarSenhaException();

        autenticar(senha);
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        this.senha = passwordEncoder.encode(novaSenha);
    }

    public void autenticar(String senha) throws SenhaInvalidaException {
        assert (senha != null);

        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        if (!passwordEncoder.matches(senha, this.senha)) {
            if (isRedefinirSenha()) {
                getRedefinirSenha().autenticar(senha);
            } else {
                throw new SenhaInvalidaException();
            }
        }
    }

    public boolean isRedefinirSenha() {
        if (getRedefinirSenha() == null)
            return false;

        return getRedefinirSenha().isValid();
    }

    public boolean isAdministrador() {
        return Papel.Tipo.ADMIN.equals(papel.getTipo());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public boolean equals(Object object) {
        if (object == null)
            return false;

        if (this == object)
            return true;

        if (!(object instanceof Usuario))
            return false;

        Usuario other = (Usuario) object;
        return sameId(other);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Usuario{");
        sb.append("id=").append(id);
        sb.append(", email='").append(email).append('\'');
        sb.append(", nome='").append(nome).append('\'');
        sb.append(", telefone=").append(telefone);
        sb.append(", registrado=").append(registrado.format(DateTimeFormatter.ISO_DATE_TIME));
        sb.append('}');
        return sb.toString();
    }

}