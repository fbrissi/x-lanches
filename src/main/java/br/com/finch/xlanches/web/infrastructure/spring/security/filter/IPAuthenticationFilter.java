package br.com.finch.xlanches.web.infrastructure.spring.security.filter;

import br.com.finch.xlanches.web.infrastructure.spring.security.authentication.DefaultUsuarioAuthentication;
import br.com.finch.xlanches.web.infrastructure.spring.security.authentication.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Filipe Bojikian Rissi
 */
public class IPAuthenticationFilter extends GenericFilterBean {

    private static final String HEADER_AUTHORIZED_IP = "authorizedIp";
    private static final Set<String> authorizedIp;

    static {
        String value = System.getProperty(HEADER_AUTHORIZED_IP);
        if (value == null || "".equals(value)) {
            value = "localhost;127.0.0.1;172.17.0.1";
        } else {
            value += ";localhost;127.0.0.1;172.17.0.1";
        }
        authorizedIp = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(value.split(";"))));
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();

        if (SecurityContextHolder.getContext().getAuthentication() == null
                && Boolean.valueOf(request.getHeader(HEADER_AUTHORIZED_IP))
                && authorizedIp.contains(request.getRemoteAddr())) {
            SecurityContextHolder.getContext().setAuthentication(createAuthentication(request));
        }

        chain.doFilter(req, res);
    }

    protected Authentication createAuthentication(HttpServletRequest request) {
        User user = new User(request.getRemoteAddr());
        Authentication auth = new DefaultUsuarioAuthentication(user, null, user.getAuthorities());
        return auth;
    }

}