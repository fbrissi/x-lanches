package br.com.finch.xlanches.web.application;

import br.com.finch.xlanches.web.interfaces.usuario.facade.cadastro.PromocaoDTO;

import javax.json.JsonObject;

/**
 * @author by Filipe Bojikian Rissi
 */
public interface PromocaoService {

    JsonObject listar(int page, int result);

    PromocaoDTO editar(Integer id);

    void persistir(PromocaoDTO promocaoDTO);

    void alterar(PromocaoDTO promocaoDTO);

    void deletar(Integer id);

}
