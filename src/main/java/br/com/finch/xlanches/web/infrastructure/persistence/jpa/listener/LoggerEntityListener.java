package br.com.finch.xlanches.web.infrastructure.persistence.jpa.listener;

import br.com.finch.xlanches.web.domain.model.shared.AuditingEntity;
import br.com.finch.xlanches.web.infrastructure.persistence.bigdata.model.UserInformation;
import br.com.finch.xlanches.web.infrastructure.util.CDIBeanCreate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;

/**
 * @author Filipe Bojikian Rissi
 */
public class LoggerEntityListener {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private static final String INFO_MESSAGE = "JSON: {}";

    private UserInformation userInformation = CDIBeanCreate.createBean(UserInformation.class);

    @PrePersist
    public void prePersist(AuditingEntity adminEntity) {
        logger.info(INFO_MESSAGE, userInformation.clone(UserInformation.PersistEvent.PRE_PERSIST, adminEntity).toJSON());
    }

    @PostPersist
    public void postPersist(AuditingEntity adminEntity) {
        logger.info(INFO_MESSAGE, userInformation.clone(UserInformation.PersistEvent.POS_PERSIST, adminEntity).toJSON());
    }

    @PreUpdate
    public void preUpdate(AuditingEntity adminEntity) {
        logger.info(INFO_MESSAGE, userInformation.clone(UserInformation.PersistEvent.PRE_UPDATE, adminEntity).toJSON());
    }

    @PostUpdate
    public void postUpdate(AuditingEntity adminEntity) {
        logger.info(INFO_MESSAGE, userInformation.clone(UserInformation.PersistEvent.POS_UPDATE, adminEntity).toJSON());
    }

    @PreRemove
    public void preRemove(AuditingEntity adminEntity) {
        logger.info(INFO_MESSAGE, userInformation.clone(UserInformation.PersistEvent.PRE_REMOVE, adminEntity).toJSON());
    }

    @PostRemove
    public void postRemove(AuditingEntity adminEntity) {
        logger.info(INFO_MESSAGE, userInformation.clone(UserInformation.PersistEvent.POS_REMOVE, adminEntity).toJSON());
    }

}