package br.com.finch.xlanches.web.infrastructure.persistence.jpa.converter;

import br.com.finch.xlanches.web.domain.model.usuario.Papel;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * @author Filipe Bojikian Rissi
 */
@Converter(autoApply = true)
public class TipoPapelConverter implements AttributeConverter<Papel.Tipo, String> {

    @Override
    public String convertToDatabaseColumn(Papel.Tipo value) {
        return value == null ? null : value.getValor();
    }

    @Override
    public Papel.Tipo convertToEntityAttribute(String value) {
        return value == null ? null : Papel.Tipo.fromValor(value);
    }

}