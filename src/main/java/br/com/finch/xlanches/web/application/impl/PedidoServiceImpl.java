package br.com.finch.xlanches.web.application.impl;

import br.com.finch.xlanches.web.application.PedidoService;
import br.com.finch.xlanches.web.domain.model.vendas.Pedido;
import br.com.finch.xlanches.web.domain.model.vendas.PedidoRepository;
import br.com.finch.xlanches.web.infrastructure.util.JSONCreate;
import br.com.finch.xlanches.web.interfaces.usuario.facade.cadastro.PedidoDTO;
import org.apache.deltaspike.jpa.api.transaction.Transactional;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.json.JsonObject;
import java.util.List;

/**
 * @author by Filipe Bojikian Rissi
 */
@Named
@RequestScoped
public class PedidoServiceImpl implements PedidoService {


    private final PedidoRepository pedidoRepository;

    protected PedidoServiceImpl() {
        pedidoRepository = null;
    }

    @Inject
    public PedidoServiceImpl(PedidoRepository pedidoRepository) {
        this.pedidoRepository = pedidoRepository;
    }

    @Override
    public JsonObject listar(int page, int result) {
        List<Pedido> list = pedidoRepository.list(page, result);
        return JSONCreate.toJSONList(list, pedidoRepository.count());
    }

    @Override
    public PedidoDTO editar(Long id) {
        Pedido pedido = pedidoRepository.find(id);
        return pedido == null ? null : new PedidoDTO(pedido);
    }

    @Override
    @Transactional
    public void persistir(PedidoDTO pedidoDTO) {
    }

    @Override
    @Transactional
    public void alterar(PedidoDTO pedidoDTO) {
    }

    @Override
    @Transactional
    public void deletar(Long id) {
        Pedido pedido = pedidoRepository.find(id);
        pedidoRepository.remove(pedido);
    }

}
