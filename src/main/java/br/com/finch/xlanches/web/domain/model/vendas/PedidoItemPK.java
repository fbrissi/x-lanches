package br.com.finch.xlanches.web.domain.model.vendas;

import br.com.finch.xlanches.web.domain.model.shared.ValuesObject;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Objects;

/**
 * @author Filipe Bojikian Rissi
 */
@Embeddable
public class PedidoItemPK implements ValuesObject<PedidoItemPK> {

    private static final long serialVersionUID = 7268158253986756754L;

    @Column(name = "pedido_id")
    private Long pedidoId;

    @Column(name = "pedido_seq")
    private Integer pedidoSeq;

    protected PedidoItemPK() {
        super();
    }

    protected PedidoItemPK(Integer pedidoSeq) {
        this.pedidoSeq = pedidoSeq;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null)
            return false;

        if (this == object)
            return true;

        if (!(object instanceof PedidoItemPK))
            return false;

        PedidoItemPK other = (PedidoItemPK) object;
        return sameValue(other);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pedidoId, pedidoSeq);
    }

    @Override
    public boolean sameValue(PedidoItemPK other) {
        return new EqualsBuilder()
                .append(this.pedidoId, other.pedidoId)
                .append(this.pedidoSeq, other.pedidoSeq)
                .isEquals();
    }

    @Override
    public int compareTo(PedidoItemPK other) {
        return new CompareToBuilder()
                .append(this.pedidoId, other.pedidoId)
                .append(this.pedidoSeq, other.pedidoSeq)
                .toComparison();
    }

}
