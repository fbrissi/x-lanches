package br.com.finch.xlanches.web.infrastructure.persistence.jpa.converter;

import br.com.finch.xlanches.web.domain.model.contato.Telefone;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * @author Filipe Bojikian Rissi
 */
@Converter(autoApply = true)
public class TelefoneConverter implements AttributeConverter<Telefone, String> {

    @Override
    public String convertToDatabaseColumn(Telefone value) {
        return value == null ? null : value.toString();
    }

    @Override
    public Telefone convertToEntityAttribute(String value) {
        return value == null ? null : new Telefone(value);
    }

}