package br.com.finch.xlanches.web.domain.model.shared;

import br.com.finch.xlanches.web.infrastructure.persistence.jpa.listener.LoggerEntityListener;

import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

/**
 * @author Filipe Bojikian Rissi
 */
@MappedSuperclass
@EntityListeners(LoggerEntityListener.class)
public abstract class AuditingEntity<T extends EntityObject, ID extends Comparable> implements EntityObject<T, ID> {
}