package br.com.finch.xlanches.web.infrastructure.cdi;

import br.com.finch.xlanches.web.domain.model.usuario.Usuario;
import br.com.finch.xlanches.web.domain.model.usuario.UsuarioRepository;
import br.com.finch.xlanches.web.infrastructure.spring.security.authentication.UsuarioAuthentication;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

/**
 * @author Filipe Bojikian Rissi
 */
public class UsuarioLogadoProduces {

    @Inject
    private UsuarioRepository usuarioRepository;

    @Produces
    @UsuarioLogado
    public Usuario getUsuario() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication instanceof UsuarioAuthentication) {
            Usuario usuario = usuarioRepository.findByEmail(authentication.getName());

            return usuario;
        }

        return null;
    }

}