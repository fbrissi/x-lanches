package br.com.finch.xlanches.web.infrastructure.validation.annotation;

import br.com.finch.xlanches.web.infrastructure.validation.logic.TelefoneValidatorLogic;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;

/**
 * @author Filipe Bojikian Rissi
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({FIELD, METHOD})
@Constraint(validatedBy = TelefoneValidatorLogic.class)
public @interface TelefoneValid {

    String message() default "{xlanches.validator.constraints.Telefone.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}