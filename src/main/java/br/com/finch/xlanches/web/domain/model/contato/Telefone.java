package br.com.finch.xlanches.web.domain.model.contato;

import br.com.finch.xlanches.web.domain.model.shared.ValuesObject;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * @author Filipe Bojikian Rissi
 */
public class Telefone implements ValuesObject<Telefone> {

    private static final long serialVersionUID = 931259742689569494L;

    private final String DDI;
    private final String DDD;
    private final String numero;

    public Telefone(String numero) {
        super();
        assert (numero != null);
        assert (numero.length() == 10 || numero.length() == 18);

        String value = numero.replaceAll("\\D", "");
        this.DDI = "5";
        this.DDD = value.substring(0, 2);
        this.numero = value.substring(2);
    }

    public String getDDI() {
        return DDI;
    }

    public String getDDD() {
        return DDD;
    }

    public String getNumero() {
        return numero;
    }

    public String toString(boolean formatado) {
        int index = numero.length() - 4;
        return formatado ? String.format("(%s) %s-%s", DDD, numero.substring(0, index),
                numero.substring(index, numero.length())) : toString();
    }

    @Override
    public String toString() {
        return DDD + numero;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(this.DDI)
                .append(this.DDD)
                .append(this.numero).toHashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (object == null)
            return false;

        if (this == object)
            return true;

        if (!(object instanceof Telefone))
            return false;

        Telefone other = (Telefone) object;
        return sameValue(other);
    }

    @Override
    public boolean sameValue(Telefone other) {
        return new EqualsBuilder()
                .append(this.DDI, other.DDI)
                .append(this.DDD, other.DDD)
                .append(this.numero, other.numero).isEquals();
    }

    @Override
    public int compareTo(Telefone other) {
        return new CompareToBuilder()
                .append(this.DDI, other.DDI)
                .append(this.DDD, other.DDD)
                .append(this.numero, other.numero).toComparison();
    }

}