package br.com.finch.xlanches.web.infrastructure.validation.logic;

import br.com.finch.xlanches.web.domain.model.identificacao.CEP;
import br.com.finch.xlanches.web.infrastructure.validation.annotation.CEPValid;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author Filipe Bojikian Rissi
 */
public class CEPValidatorLogic implements ConstraintValidator<CEPValid, CEP> {

    @Override
    public void initialize(CEPValid cnpj) {
    }

    @Override
    public boolean isValid(CEP cep, ConstraintValidatorContext context) {
        if (cep != null) {
            if (cep.toString().trim().length() == 0) {
                return true;
            } else {
                return cep.toString().trim().length() == 8;
            }
        }

        return true;
    }

}