package br.com.finch.xlanches.web.domain.model.localizacao;

import br.com.finch.xlanches.web.domain.model.shared.EntityRepository;

/**
 * @author Filipe Bojikian Rissi
 */
public interface EstadoRepository extends EntityRepository<Estado, Integer> {

    String findAll = "Estado.findAll";
    String findById = "Estado.findById";
    String findByCodigo = "Estado.findByCodigo";
    String selectByNome = "Estado.selectByNome";
    String findBySigla = "Estado.findBySigla";

}
