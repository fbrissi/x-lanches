/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.finch.xlanches.web.domain.model.localizacao;

import br.com.finch.xlanches.web.domain.model.shared.EntityObject;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author Filipe Bojikian Rissi
 */
@Entity
@Table(name = "estado")
@SequenceGenerator(name = "estado_id_seq", sequenceName = "estado_id_seq", allocationSize = 1)
@NamedQueries({
        @NamedQuery(name = EstadoRepository.findAll, query = "SELECT e FROM Estado e"),
        @NamedQuery(name = EstadoRepository.findById, query = "SELECT e FROM Estado e WHERE e.id = :id"),
        @NamedQuery(name = EstadoRepository.selectByNome, query = "SELECT e FROM Estado e WHERE e.nome like :nome"),
        @NamedQuery(name = EstadoRepository.findBySigla, query = "SELECT e FROM Estado e WHERE e.sigla = :sigla"),
        @NamedQuery(name = EstadoRepository.findByCodigo, query = "SELECT e FROM Estado e WHERE e.codigo = :codigo")})
public class Estado implements EntityObject<Estado, Integer> {

    private static final long serialVersionUID = -5364616859755863229L;

    @Id
    @GeneratedValue(generator = "estado_id_seq")
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "codigo")
    private String codigo;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "nome")
    private String nome;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "sigla")
    private String sigla;

    @OneToMany(mappedBy = "estado", fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    private Set<Cidade> cidades;

    protected Estado() {
        super();
    }

    public Estado(String codigo, String nome, String sigla) {
        super();
        assert (codigo != null);
        assert (nome != null);
        assert (sigla != null);

        this.codigo = codigo;
        this.nome = nome;
        this.sigla = sigla;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public Set<Cidade> getCidades() {
        if (cidades == null)
            cidades = new HashSet<>();
        return Collections.unmodifiableSet(cidades);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.id);
    }

    @Override
    public boolean equals(Object object) {
        if (object == null)
            return false;

        if (this == object)
            return true;

        if (!(object instanceof Estado))
            return false;

        Estado other = (Estado) object;
        return sameId(other);
    }

    @Override
    public boolean sameId(Estado other) {
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(getClass().getSimpleName());
        sb.append('{');
        sb.append("codigo='").append(codigo).append('\'');
        sb.append(", nome='").append(nome).append('\'');
        sb.append(", sigla='").append(sigla).append('\'');
        sb.append('}');
        return sb.toString();
    }
}