package br.com.finch.xlanches.web.interfaces.spring.shared;

import br.com.finch.xlanches.web.domain.service.EmailProxy;
import br.com.finch.xlanches.web.domain.service.EmailService;
import br.com.finch.xlanches.web.domain.service.ReCaptchaService;
import br.com.finch.xlanches.web.interfaces.usuario.facade.usuario.ContatoDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Filipe Bojikian Rissi
 */
@Controller
public class HomeController {

    @Inject
    private ReCaptchaService reCaptchaService;

    @Inject
    private EmailService emailService;

    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String init(Model model) {
        model.addAttribute("contato", new ContatoDTO());
        return "index";
    }

    @RequestMapping(value = "/contato", method = RequestMethod.POST)
    public String contato(HttpServletRequest request, @ModelAttribute("contato") @Valid ContatoDTO contato, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "index :: form";
        }

        try {
            if (!reCaptchaService.verify(contato.getCaptcha(), request.getRemoteAddr()).isSuccess()) {
                return "index :: form";
            }

            emailService.addTo("email.teste.sistemas@gmail.com");
            EmailProxy proxy = emailService.getProxy();

            Map<String, String> replace = new HashMap<>();
            replace.put("${TITLE}", contato.getName());
            replace.put("${NAME}", contato.getName());
            replace.put("${EMAIL}", contato.getEmail());
            replace.put("${MESSAGE}", contato.getMessage());

            proxy.send("Contato de: ".concat(contato.getName()), Paths.get(HomeController.class.getResource("/br/com/finch/xlanches/web/infrastructure/email/contato.html").toURI()), replace);
        } catch (MessagingException | IOException | URISyntaxException e) {
            model.addAttribute("severe", true);
            return "index :: form";
        }

        return "index :: form";
    }

}