package br.com.finch.xlanches.web.domain.model.vendas;

import br.com.finch.xlanches.web.domain.model.localizacao.Endereco;
import br.com.finch.xlanches.web.domain.model.shared.AuditingEntity;
import br.com.finch.xlanches.web.domain.model.usuario.Usuario;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author Filipe Bojikian Rissi
 */
@Entity
@Table(name = "pedido")
@SequenceGenerator(name = "pedido_id_seq", sequenceName = "pedido_id_seq", allocationSize = 1)
public class Pedido extends AuditingEntity<Pedido, Long> {

    private static final long serialVersionUID = 7008429218559606565L;

    public enum Status {
        SOLICITADO("S", "Solicitado"),
        APROVADO("A", "Aprovado"),
        RECUSADO("R", "Recusado"),
        ENTREGUE("E", "Entregue"),
        FINALIZADO("F", "Finalizado");

        private final String valor;
        private final String descricao;

        Status(String valor, String descricao) {
            this.valor = valor;
            this.descricao = descricao;
        }

        public String getValor() {
            return valor;
        }

        public String getDescricao() {
            return descricao;
        }

        public static Status fromValor(String value) {
            for (Status tipo : Status.values())
                if (tipo.valor.equals(value))
                    return tipo;

            return null;
        }

    }

    @Id
    @GeneratedValue(generator = "pedido_id_seq")
    @Column(name = "id")
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Size(max = 12)
    @Column(name = "numero")
    private String numero;

    @Basic(optional = false)
    @NotNull
    @Column(name = "registrado", insertable = false, updatable = false)
    private LocalDateTime registrado;

    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private Status status;

    @Size(max = 180)
    @Column(name = "observacao")
    private String observacao;

    @Column(name = "finalizado", insertable = false)
    private LocalDateTime finalizado;

    @Basic(optional = false)
    @NotNull
    @Column(name = "valor")
    private BigDecimal valor;

    @Basic(optional = false)
    @NotNull
    @Column(name = "desconto")
    private BigDecimal desconto;

    @Basic(optional = false)
    @NotNull
    @Column(name = "taxa")
    private BigDecimal taxa;

    @Column(name = "pago")
    private BigDecimal pago;

    @NotNull
    @ManyToOne(optional = false, cascade = CascadeType.DETACH)
    private Usuario usuario;

    @NotNull
    @Size(max = 35)
    @Column(name = "endereco_entrega")
    private String enderecoEntrega;

    @OneToMany(mappedBy = "pedido", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<PedidoItem> itens;

    protected Pedido() {
        super();
        this.valor = BigDecimal.ZERO;
        this.desconto = BigDecimal.ZERO;
        this.taxa = BigDecimal.ZERO;
        this.registrado = LocalDateTime.now();
        this.itens = new ArrayList<>();
    }

    public Pedido(String observacao, Usuario usuario) {
        super();
        assert (usuario != null);

        this.valor = BigDecimal.ZERO;
        this.desconto = BigDecimal.ZERO;
        this.taxa = BigDecimal.ZERO;
        this.observacao = observacao;
        this.usuario = usuario;
        this.registrado = LocalDateTime.now();
        this.itens = new ArrayList<>();
    }

    @Override
    public Long getId() {
        return id;
    }

    public String getNumero() {
        return numero;
    }

    public LocalDateTime getRegistrado() {
        return registrado;
    }

    public Status getStatus() {
        return status;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public LocalDateTime getFinalizado() {
        return finalizado;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public BigDecimal getDesconto() {
        return desconto;
    }

    public BigDecimal getTaxa() {
        return taxa;
    }

    public BigDecimal getPago() {
        return pago;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public Endereco getEnderecoEntrega() {
        return usuario.getEnderecos().get(enderecoEntrega);
    }

    public void setEnderecoEntrega(Endereco enderecoEntrega) {
        assert (enderecoEntrega != null);

        this.enderecoEntrega = usuario.getEnderecos().entrySet().stream().filter(value -> value.getValue().equals(enderecoEntrega)).findAny().get().getKey();
    }

    public List<PedidoItem> getPedidoItems() {
        return Collections.unmodifiableList(itens);
    }

    public boolean addItem(BigDecimal quantidade, Cardapio cardapio) {
        assert (quantidade != null);
        assert (cardapio != null);

        PedidoItem pedidoItem = new PedidoItem(itens.size(), quantidade, cardapio, this);
        if (this.itens.add(pedidoItem)) {
            valor = valor.add(pedidoItem.getValor());
            desconto = desconto.add(pedidoItem.getDesconto());
            return true;
        }

        return false;
    }

    public void removerItem(int index) {
        assert (index < this.itens.size());

        PedidoItem pedidoItem = this.itens.remove(index);
        if (pedidoItem != null) {
            valor = valor.subtract(pedidoItem.getValor());
            desconto = desconto.subtract(pedidoItem.getDesconto());
        }
    }

    public void removerTodosItens() {
        this.itens.clear();
        valor = BigDecimal.ZERO;
    }

    public boolean addIngrediente(int indexItem, BigDecimal quantidade, Ingrediente ingrediente) {
        assert (ingrediente != null);

        IngredienteItem pedidoItemIngrediente = new IngredienteItem(quantidade, ingrediente);
        PedidoItem pedidoItem = itens.get(indexItem);
        if (pedidoItem.addIngrediente(pedidoItemIngrediente)) {
            valor = valor.add(pedidoItem.getValor());
            desconto = desconto.add(pedidoItem.getDesconto());
        }

        return false;
    }

    public void removerIngrediente(int indexItem, IngredienteItem ingrediente) {
        assert (indexItem < this.itens.size());

        PedidoItem pedidoItem = this.itens.get(indexItem);
        if (pedidoItem != null) {
            if (pedidoItem.removerIngrediente(ingrediente)) {
                valor = valor.subtract(ingrediente.getValor());
                desconto = desconto.subtract(pedidoItem.getDesconto());
            }
        }
    }

    public void removerTodosIngredientes(int indexItem) {
        PedidoItem pedidoItem = this.itens.get(indexItem);
        valor = valor.subtract(pedidoItem.getValorIngredientes());
        desconto = desconto.subtract(pedidoItem.getDesconto());
        pedidoItem.removerTodosIngrediente();
    }

    public BigDecimal getTotal() {
        return valor.subtract(desconto);
    }

    @Override
    public boolean equals(Object object) {
        if (object == null)
            return false;

        if (this == object)
            return true;

        if (!(object instanceof Pedido))
            return false;

        Pedido other = (Pedido) object;
        return sameId(other);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.id);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Pedido{");
        sb.append("id=").append(id);
        sb.append(", numero='").append(numero).append('\'');
        sb.append(", registrado=").append(registrado.format(DateTimeFormatter.ISO_DATE_TIME));
        sb.append(", status=").append(status);
        sb.append(", valor=").append(valor);
        sb.append(", usuario=").append(usuario);
        sb.append('}');
        return sb.toString();
    }

}