package br.com.finch.xlanches.web.application;

import br.com.finch.xlanches.web.interfaces.usuario.facade.cadastro.PedidoDTO;

import javax.json.JsonObject;

/**
 * @author by Filipe Bojikian Rissi
 */
public interface PedidoService {

    JsonObject listar(int page, int result);

    PedidoDTO editar(Long id);

    void persistir(PedidoDTO pedidoDTO);

    void alterar(PedidoDTO pedidoDTO);

    void deletar(Long id);

}
