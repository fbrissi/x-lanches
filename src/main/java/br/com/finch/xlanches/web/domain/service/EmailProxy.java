package br.com.finch.xlanches.web.domain.service;

import javax.mail.MessagingException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;

/**
 * @author Filipe Bojikian Rissi
 */
public interface EmailProxy {

    void send(String subject, Path html) throws MessagingException, IOException;

    void send(String subject, Path html, Map<String, String> replace) throws MessagingException, IOException;

}