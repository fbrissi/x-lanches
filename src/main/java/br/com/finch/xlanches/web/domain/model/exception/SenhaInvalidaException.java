package br.com.finch.xlanches.web.domain.model.exception;

import br.com.finch.xlanches.web.domain.model.shared.ResourceBundleException;

/**
 * @author Filipe Bojikian Rissi
 */
public class SenhaInvalidaException extends ResourceBundleException {

    private static final long serialVersionUID = 1693037001161028416L;

    public SenhaInvalidaException() {
        super("{xlanches.exception.SenhaInvalidaException.message}");
    }

}