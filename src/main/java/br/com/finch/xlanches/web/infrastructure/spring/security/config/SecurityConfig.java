package br.com.finch.xlanches.web.infrastructure.spring.security.config;

import br.com.finch.xlanches.web.infrastructure.spring.security.authentication.AuthenticationSuccessHandler;
import br.com.finch.xlanches.web.infrastructure.spring.security.authentication.CsrfRequestMatcher;
import br.com.finch.xlanches.web.infrastructure.spring.security.authentication.Role;
import br.com.finch.xlanches.web.infrastructure.spring.security.authentication.UsuarioAuthenticationProvider;
import br.com.finch.xlanches.web.infrastructure.spring.security.filter.IPAuthenticationFilter;
import br.com.finch.xlanches.web.infrastructure.spring.security.filter.PaginaManutencaoFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.filter.HiddenHttpMethodFilter;

import javax.inject.Inject;

/**
 * @author Filipe Bojikian Rissi
 */
@Configuration
@EnableWebSecurity
@ComponentScan({
        "br.com.finch.xlanches.web.infrastructure.spring.security.authentication"
})
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Inject
    private UsuarioAuthenticationProvider authenticationProvider;

    @Inject
    private AuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider);
    }

    @Inject
    public SavedRequestAwareAuthenticationSuccessHandler savedRequestAwareAuthenticationSuccessHandler() {
        SavedRequestAwareAuthenticationSuccessHandler auth = new SavedRequestAwareAuthenticationSuccessHandler();
        auth.setTargetUrlParameter("targetUrl");
        return auth;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().requireCsrfProtectionMatcher(new CsrfRequestMatcher());
        http.addFilterBefore(new PaginaManutencaoFilter(), AnonymousAuthenticationFilter.class);
        http.addFilterBefore(new IPAuthenticationFilter(), AnonymousAuthenticationFilter.class);
        http.addFilterBefore(new HiddenHttpMethodFilter(), AnonymousAuthenticationFilter.class);
        http.exceptionHandling()
                .accessDeniedPage("/erro/acesso_negado")

                .and().authorizeRequests()

                .antMatchers("/img/**").permitAll()
                .antMatchers("/css/**").permitAll()
                .antMatchers("/js/**").permitAll()
                .antMatchers("/plugins/**").permitAll()
                .antMatchers("/ajax/**").permitAll()
                .antMatchers("/contato").permitAll()
                .antMatchers("/registrar").permitAll()
                .antMatchers("/registrado").permitAll()
                .antMatchers("/login/recuperar-senha").permitAll()
                .antMatchers("/erro/**").permitAll()

                .antMatchers("/validar/**").permitAll()

                .antMatchers("/administracao").hasRole(Role.ADMIN)
                .antMatchers("/ingrediente").hasRole(Role.ADMIN)
                .antMatchers("/promocao").hasRole(Role.ADMIN)
                .antMatchers("/cardapio").hasRole(Role.ADMIN)

                .antMatchers("/usuario").hasAnyRole(Role.USER)

                .antMatchers("/redefinir-senha").hasAnyRole(Role.ADMIN, Role.USER)
                .antMatchers("/pedido").hasAnyRole(Role.ADMIN, Role.USER)
                .antMatchers("/endereco").hasAnyRole(Role.ADMIN, Role.USER)

                .anyRequest().authenticated()

                .and()

                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/login?logout")
                .permitAll()

                .and()

                .formLogin()
                .successHandler(authenticationSuccessHandler)

                .loginPage("/login")
                .failureUrl("/login?falha")
                .permitAll();
    }

}