package br.com.finch.xlanches.web.infrastructure.persistence.jpa.repository;

import br.com.finch.xlanches.web.domain.model.localizacao.Cidade;
import br.com.finch.xlanches.web.domain.model.localizacao.CidadeRepository;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author Filipe Bojikian Rissi
 */
@Named
@RequestScoped
public class CidadeRepositoryImpl extends AbstractRepository<Cidade, Integer> implements CidadeRepository {

    protected CidadeRepositoryImpl() {
    }

    @Inject
    public CidadeRepositoryImpl(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public List<Cidade> listByEstado(Integer estado) {
        TypedQuery<Cidade> query = createNamedQuery(CidadeRepository.selectByEstado, new Parameter("id", estado));
        try {
            return query.getResultList();
        } catch (NoResultException ex) {
            return null;
        }
    }

    @Override
    public Cidade findByCodigo(String codigo) {
        TypedQuery<Cidade> query = createNamedQuery(CidadeRepository.findByCodigo, new Parameter("codigo", codigo));
        try {
            return query.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    @Override
    public List<Cidade> listByNome(String name) {
        TypedQuery<Cidade> query = createNamedQuery(CidadeRepository.selectByNome, new Parameter("nome", name));
        try {
            return query.getResultList();
        } catch (NoResultException ex) {
            return null;
        }
    }

}