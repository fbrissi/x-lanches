package br.com.finch.xlanches.web.infrastructure.persistence.bigdata.model;

import br.com.finch.xlanches.web.domain.model.usuario.Usuario;
import br.com.finch.xlanches.web.infrastructure.cdi.UsuarioLogado;

import javax.enterprise.inject.Produces;
import javax.servlet.http.HttpServletRequest;

/**
 * @author Filipe Bojikian Rissi
 */
public class UserInformationProduces {

    @Produces
    public UserInformation createUserInformation(HttpServletRequest request, @UsuarioLogado Usuario usuario) {
        return new UserInformation(usuario == null ? null : usuario.getEmail(), request.getRemoteAddr(), request.getRemotePort());
    }

}