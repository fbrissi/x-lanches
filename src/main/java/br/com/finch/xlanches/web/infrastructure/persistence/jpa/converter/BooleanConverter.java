package br.com.finch.xlanches.web.infrastructure.persistence.jpa.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * @author Filipe Bojikian Rissi
 */
@Converter(autoApply = true)
public class BooleanConverter implements AttributeConverter<Boolean, String> {

    @Override
    public String convertToDatabaseColumn(Boolean value) {
        return value == null ? "F" : value ? "T" : "F";
    }

    @Override
    public Boolean convertToEntityAttribute(String value) {
        return value == null ? false : "T".equalsIgnoreCase(value);
    }

}