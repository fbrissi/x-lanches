package br.com.finch.xlanches.web.infrastructure.spring.config;

import br.com.finch.xlanches.web.application.*;
import br.com.finch.xlanches.web.domain.service.EmailService;
import br.com.finch.xlanches.web.domain.service.ReCaptchaService;
import br.com.finch.xlanches.web.infrastructure.util.CDIBeanCreate;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Filipe Bojikian Rissi
 */
@Service
@RequestScope
public class CDIBeanService {

    @Bean
    @RequestScope
    public EmailService emailService() {
        return CDIBeanCreate.createBean(EmailService.class);
    }

    @Bean
    @RequestScope
    public ReCaptchaService reCaptchaService() {
        return CDIBeanCreate.createBean(ReCaptchaService.class);
    }

    @Bean
    @RequestScope
    public UsuarioService usuarioService() {
        return CDIBeanCreate.createBean(UsuarioService.class);
    }

    @Bean
    @RequestScope
    public IngredienteService cadastroService() {
        return CDIBeanCreate.createBean(IngredienteService.class);
    }

    @Bean
    @RequestScope
    public PromocaoService promocaoService() {
        return CDIBeanCreate.createBean(PromocaoService.class);
    }

    @Bean
    @RequestScope
    public CardapioService cardapioService() {
        return CDIBeanCreate.createBean(CardapioService.class);
    }

    @Bean
    @RequestScope
    public PedidoService pedidoService() {
        return CDIBeanCreate.createBean(PedidoService.class);
    }

}