package br.com.finch.xlanches.web.infrastructure.spring.security.authentication;

import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Pattern;

/**
 * @author Filipe Bojikian Rissi
 */
public class CsrfRequestMatcher implements RequestMatcher {

    private final Pattern allowedMethods = Pattern.compile("^GET$");

    private final AntPathRequestMatcher[] requestMatchers = {
            new AntPathRequestMatcher("/rest/**")
    };

    @Override
    public boolean matches(HttpServletRequest request) {
        if (allowedMethods.matcher(request.getMethod()).matches()) {
            return false;
        }

        for (AntPathRequestMatcher rm : requestMatchers) {
            if (rm.matches(request)) {
                return false;
            }
        }

        return true;
    }

}