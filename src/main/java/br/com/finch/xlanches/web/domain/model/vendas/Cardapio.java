package br.com.finch.xlanches.web.domain.model.vendas;

import br.com.finch.xlanches.web.domain.model.shared.AuditingEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author Filipe Bojikian Rissi
 */
@Entity
@Table(name = "cardapio")
@SequenceGenerator(name = "cardapio_id_seq", sequenceName = "cardapio_id_seq", allocationSize = 1)
public class Cardapio extends AuditingEntity<Cardapio, Integer> {

    private static final long serialVersionUID = -4461773267415995842L;

    @Id
    @GeneratedValue(generator = "cardapio_id_seq")
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Size(max = 55)
    @Column(name = "nome")
    private String nome;

    @Size(max = 180)
    @Column(name = "observacao")
    private String observacao;

    @NotNull
    @ManyToOne(cascade = CascadeType.DETACH, optional = false)
    @JoinColumn(name = "promocao_id", referencedColumnName = "id")
    private Promocao promocao;

    @ElementCollection
    @CollectionTable(name = "cardapio_item", joinColumns = @JoinColumn(name = "cardapio_id", referencedColumnName = "id"))
    private Set<IngredienteItem> ingredientes;

    protected Cardapio() {
        super();
        this.ingredientes = new HashSet<>();
    }

    public Cardapio(String nome, String observacao, Promocao promocao) {
        super();
        assert (nome != null);

        this.nome = nome;
        this.observacao = observacao;
        this.promocao = promocao;
        this.ingredientes = new HashSet<>();
    }

    @Override
    public Integer getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Promocao getPromocao() {
        return promocao;
    }

    public void setPromocao(Promocao promocao) {
        this.promocao = promocao;
    }

    public Set<IngredienteItem> getIngredientes() {
        return Collections.unmodifiableSet(ingredientes);
    }

    public boolean addIngrediente(BigDecimal quantidade, Ingrediente ingrediente) {
        assert (quantidade != null);
        assert (ingrediente != null);

        return this.ingredientes.add(new IngredienteItem(quantidade, ingrediente));
    }

    public void removerTodosIngrediente() {
        this.ingredientes.clear();
    }

    public boolean removerIngrediente(IngredienteItem ingredienteItem) {
        assert (ingredienteItem != null);

        return this.ingredientes.remove(ingredienteItem);
    }

    public BigDecimal getValor() {
        return ingredientes.stream().map(item -> item.getValor()).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @Override
    public boolean equals(Object object) {
        if (object == null)
            return false;

        if (this == object)
            return true;

        if (!(object instanceof Cardapio))
            return false;

        Cardapio other = (Cardapio) object;
        return sameId(other);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.id);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Cardapio{");
        sb.append("id=").append(id);
        sb.append(", nome='").append(nome).append('\'');
        sb.append(", observacao='").append(observacao).append('\'');
        sb.append(", promocao=").append(promocao);
        sb.append('}');
        return sb.toString();
    }

}