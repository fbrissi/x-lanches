package br.com.finch.xlanches.web.domain.model.usuario;

import br.com.finch.xlanches.web.domain.model.shared.EntityRepository;

/**
 * @author Filipe Bojikian Rissi
 */
public interface PapelRepository extends EntityRepository<Papel, Integer> {

    String findByTipo = "Papel.findByTipo";

    Papel findByTipo(Papel.Tipo tipo);

}
