package br.com.finch.xlanches.web.infrastructure.persistence.jpa.repository;

import br.com.finch.xlanches.web.domain.model.vendas.Pedido;
import br.com.finch.xlanches.web.domain.model.vendas.PedidoRepository;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

/**
 * @author Filipe Bojikian Rissi
 */
@Named
@RequestScoped
public class PedidoRepositoryImpl extends AbstractRepository<Pedido, Long> implements PedidoRepository {

    protected PedidoRepositoryImpl() {
    }

    @Inject
    public PedidoRepositoryImpl(EntityManager entityManager) {
        super(entityManager);
    }

}