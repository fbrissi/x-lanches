package br.com.finch.xlanches.web.application.impl;

import br.com.finch.xlanches.web.application.UsuarioService;
import br.com.finch.xlanches.web.domain.model.contato.Telefone;
import br.com.finch.xlanches.web.domain.model.exception.ConfirmarSenhaException;
import br.com.finch.xlanches.web.domain.model.exception.EmailException;
import br.com.finch.xlanches.web.domain.model.exception.EncryptionException;
import br.com.finch.xlanches.web.domain.model.exception.SenhaInvalidaException;
import br.com.finch.xlanches.web.domain.model.usuario.PapelRepository;
import br.com.finch.xlanches.web.domain.model.usuario.Usuario;
import br.com.finch.xlanches.web.domain.model.usuario.UsuarioFactory;
import br.com.finch.xlanches.web.domain.model.usuario.UsuarioRepository;
import br.com.finch.xlanches.web.domain.service.EmailProxy;
import br.com.finch.xlanches.web.domain.service.EmailService;
import br.com.finch.xlanches.web.domain.service.exception.EnvioEmailException;
import br.com.finch.xlanches.web.domain.service.exception.ExpiredException;
import br.com.finch.xlanches.web.domain.service.exception.InvalidException;
import br.com.finch.xlanches.web.infrastructure.cdi.UsuarioLogado;
import br.com.finch.xlanches.web.infrastructure.util.Constants;
import br.com.finch.xlanches.web.interfaces.usuario.facade.usuario.UsuarioDTO;
import org.apache.deltaspike.jpa.api.transaction.Transactional;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.mail.MessagingException;
import java.io.IOException;
import java.io.StringReader;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * @author by Filipe Bojikian Rissi
 */
@Named
@RequestScoped
public class UsuarioServiceImpl implements UsuarioService {

    private static final byte[] IV = "PLXEICHTGFYWMDCH".getBytes(StandardCharsets.UTF_8);
    private static final byte[] KEY = "ONshEoF9K4UJCp08".getBytes(StandardCharsets.UTF_8);

    private final Usuario usuario;
    private final EmailService emailService;
    private final UsuarioRepository usuarioRepository;
    private final PapelRepository papelRepository;

    protected UsuarioServiceImpl() {
        usuario = null;
        emailService = null;
        usuarioRepository = null;
        papelRepository = null;
    }

    @Inject
    public UsuarioServiceImpl(@UsuarioLogado Usuario usuario, EmailService emailService,
                              UsuarioRepository usuarioRepository, PapelRepository papelRepository) {
        this.usuario = usuario;
        this.emailService = emailService;
        this.usuarioRepository = usuarioRepository;
        this.papelRepository = papelRepository;
    }

    @Override
    public Usuario find(String email) {
        return usuarioRepository.findByEmail(email);
    }

    @Override
    @Transactional
    public void alterarSenha(String senha, String novaSenha, String confirmarNovaSenha) throws SenhaInvalidaException, ConfirmarSenhaException {
        usuario.alterarSenha(senha, novaSenha, confirmarNovaSenha);
    }

    @Override
    @Transactional
    public void mergeUsuario(UsuarioDTO usuarioDTO) throws EnvioEmailException, EncryptionException, EmailException {
        assert (usuarioDTO != null);

        usuario.setNome(usuarioDTO.getNome());
        usuario.setTelefone(usuarioDTO.getTelefone());

        if (!usuario.getEmail().equals(usuarioDTO.getEmail())) {
            if (usuarioRepository.findByEmail(usuarioDTO.getEmail()) != null) {
                throw new EmailException();
            }

            try {
                emailService.addTo(usuario.getEmail());
                EmailProxy proxy = emailService.getProxy();

                StringBuilder link = new StringBuilder(Constants.XLANCHES_CONTEXTPATH);
                StringBuilder hash = new StringBuilder();

                Cipher encripta = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
                SecretKeySpec key = new SecretKeySpec(KEY, "AES");
                encripta.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(IV));

                link.append("/validar/email/alterar/aprovar?chave=");
                hash.append(Json.createObjectBuilder().add("e-mail",
                        Json.createObjectBuilder()
                                .add("current", usuario.getEmail())
                                .add("override", usuarioDTO.getEmail()))
                        .add("expira", DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss").format(LocalDateTime.now().plusHours(48))).build().toString());
                link.append(URLEncoder.encode(new String(Base64.getEncoder().encode(encripta.doFinal(hash.toString().getBytes(StandardCharsets.UTF_8))), StandardCharsets.UTF_8), "UTF-8"));

                Map<String, String> replace = new HashMap<>();
                replace.put("${TO}", usuario.getNome());
                replace.put("${EMAIL}", usuarioDTO.getEmail());
                replace.put("${LINK}", link.toString());

                proxy.send("Solicitação Para Alterar Senha", Paths.get(UsuarioServiceImpl.class.getResource("/br/com/finch/xlanches/web/infrastructure/email/alterar_email.html").toURI()), replace);
            } catch (MessagingException | IOException | URISyntaxException e) {
                throw new EnvioEmailException();
            } catch (NoSuchPaddingException | InvalidKeyException | NoSuchProviderException | BadPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
                throw new EncryptionException();
            }
        }
    }

    @Override
    @Transactional
    public String updateEmail(String chave) throws EnvioEmailException, EncryptionException, InvalidException, ExpiredException, EmailException {
        try {
            Cipher decripta = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
            SecretKeySpec key = new SecretKeySpec(KEY, "AES");
            decripta.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(IV));

            String jsonValue = new String(decripta.doFinal(Base64.getDecoder().decode(chave.getBytes(StandardCharsets.UTF_8))), StandardCharsets.UTF_8);
            try (JsonReader reader = Json.createReader(new StringReader(jsonValue))) {
                JsonObject jsonObject = reader.readObject();
                JsonObject email = jsonObject.getJsonObject("e-mail");

                String current = email.getString("current");
                String override = email.getString("override");

                Usuario usuario = find(current);

                if (usuarioRepository.findByEmail(override) != null) {
                    throw new EmailException();
                }

                if (usuario == null) {
                    throw new InvalidException();
                }

                LocalDateTime expira = LocalDateTime.parse(jsonObject.getString("expira"), DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));

                if (LocalDateTime.now().isAfter(expira)) {
                    throw new ExpiredException();
                }

                usuario.setEmail(override);
                return override;
            }
        } catch (NoSuchPaddingException | InvalidKeyException | NoSuchProviderException | BadPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
            throw new EncryptionException();
        }
    }

    @Override
    public Usuario loadByEmail(String email) throws EmailException {
        Usuario usuario = find(email);

        if (usuario == null)
            throw new EmailException();

        return usuario;
    }

    @Override
    @Transactional
    public void redefinirSenha(String email) throws EmailException, EnvioEmailException {
        Usuario usuario = find(email);

        if (usuario == null)
            throw new EmailException();

        try {
            emailService.addTo(usuario.getEmail());
            EmailProxy proxy = emailService.getProxy();

            Map<String, String> replace = new HashMap<>();
            replace.put("${TO}", usuario.getNome());
            replace.put("${SENHA}", usuario.redefinirSenha());

            proxy.send("Solicitação Para Alterar Senha", Paths.get(UsuarioServiceImpl.class.getResource("/br/com/finch/xlanches/web/infrastructure/email/redefinir_senha.html").toURI()), replace);
        } catch (MessagingException | IOException | URISyntaxException e) {
            throw new EnvioEmailException();
        }
    }

    @Override
    @Transactional
    public void criarUsuario(String email, String nome, Telefone telefone, String senha, String confirmarSenha) throws SenhaInvalidaException, EmailException, EnvioEmailException {
        Usuario usuario = UsuarioFactory.criarUsuario(email, nome, telefone, senha, confirmarSenha, papelRepository);

        if (usuarioRepository.findByEmail(email) != null) {
            throw new EmailException();
        }

        usuarioRepository.persist(usuario);

        try {
            emailService.addTo(usuario.getEmail());
            EmailProxy proxy = emailService.getProxy();

            Map<String, String> replace = new HashMap<>();
            replace.put("${TO}", usuario.getNome());

            proxy.send("Cadastrado no X-Lanches", Paths.get(UsuarioServiceImpl.class.getResource("/br/com/finch/xlanches/web/infrastructure/email/registrado.html").toURI()), replace);
        } catch (MessagingException | IOException | URISyntaxException e) {
            throw new EnvioEmailException();
        }
    }

}
