package br.com.finch.xlanches.web.domain.service;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.mail.MessagingException;
import java.io.StringReader;

/**
 * @author Filipe Bojikian Rissi
 */
public interface ReCaptchaService {

    String RECAPTCHA_RESPONSE = "g-recaptcha-response";

    class CaptchaResponse {

        private final boolean success;

        public CaptchaResponse(String success) {
            try (final JsonReader jsonReader = Json.createReader(new StringReader(success))) {
                JsonObject jsonObject = jsonReader.readObject();

                this.success = jsonObject.getBoolean("success");
            }
        }

        public boolean isSuccess() {
            return success;
        }

    }

    CaptchaResponse verify(final String response, final String remoteIP) throws MessagingException;

}