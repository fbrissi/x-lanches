package br.com.finch.xlanches.web.infrastructure.spring.config;

import liquibase.integration.servlet.LiquibaseServletListener;
import org.apache.commons.lang3.SystemUtils;
import org.jboss.weld.environment.servlet.Listener;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.*;
import java.util.Arrays;
import java.util.HashSet;

/**
 * @author Filipe Bojikian Rissi
 */
public class WebApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    private static final long MAX_FILE_SIZE = -1L;
    private static final long MAX_REQUEST_SIZE = -1L;
    private static final int FILE_SIZE_THRESHOLD = 0;

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        servletContext.addListener(LiquibaseServletListener.class);
        servletContext.addListener(Listener.class);

        super.onStartup(servletContext);

        servletContext.addListener(RequestContextListener.class);

        servletContext.getSessionCookieConfig().setName("x-lanches");
        servletContext.getSessionCookieConfig().setHttpOnly(true);
        servletContext.setSessionTrackingModes(new HashSet<>(Arrays.asList(SessionTrackingMode.COOKIE)));

        servletContext.setInitParameter("liquibase.changelog", "br/com/finch/xlanches/web/database/changelog/db.changelog-master.xml");
        servletContext.setInitParameter("liquibase.datasource", "java:/comp/env/jdbc/xlanches-ds-postgres");
        servletContext.setInitParameter("liquibase.onerror.fail", "true");
    }

    @Override
    protected void customizeRegistration(ServletRegistration.Dynamic registration) {
        registration.setMultipartConfig(getMultipartConfigElement());
    }

    private MultipartConfigElement getMultipartConfigElement() {
        MultipartConfigElement multipartConfigElement = new MultipartConfigElement(SystemUtils.JAVA_IO_TMPDIR, MAX_FILE_SIZE, MAX_REQUEST_SIZE, FILE_SIZE_THRESHOLD);
        return multipartConfigElement;
    }

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{AppConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[0];
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

}
