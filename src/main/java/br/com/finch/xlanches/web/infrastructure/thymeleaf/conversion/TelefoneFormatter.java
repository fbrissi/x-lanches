package br.com.finch.xlanches.web.infrastructure.thymeleaf.conversion;

import br.com.finch.xlanches.web.domain.model.contato.Telefone;
import org.springframework.format.Formatter;

import java.util.Locale;

/**
 * @author Filipe Bojikian Rissi
 */
public class TelefoneFormatter implements Formatter<Telefone> {

    @Override
    public Telefone parse(final String text, final Locale locale) {
        return new Telefone(text);
    }

    @Override
    public String print(final Telefone object, final Locale locale) {
        return object.toString(true);
    }

}