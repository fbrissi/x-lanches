package br.com.finch.xlanches.web.interfaces.usuario.facade.usuario;

import br.com.finch.xlanches.web.infrastructure.validation.annotation.PassWordConfirm;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author Filipe Bojikian Rissi
 */
@PassWordConfirm(password = "novaSenha", confirm = "confirmarNovaSenha")
public class ResetarSenhaDTO implements Serializable {

    private static final long serialVersionUID = -3318177281615428224L;

    @NotNull
    @Size(min = 8, max = 40)
    private String senha;

    @NotNull
    @Size(min = 8, max = 40)
    private String novaSenha;

    @NotNull
    @Size(min = 8, max = 40)
    private String confirmarNovaSenha;

    public ResetarSenhaDTO() {
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getNovaSenha() {
        return novaSenha;
    }

    public void setNovaSenha(String novaSenha) {
        this.novaSenha = novaSenha;
    }

    public String getConfirmarNovaSenha() {
        return confirmarNovaSenha;
    }

    public void setConfirmarNovaSenha(String confirmarNovaSenha) {
        this.confirmarNovaSenha = confirmarNovaSenha;
    }

}
