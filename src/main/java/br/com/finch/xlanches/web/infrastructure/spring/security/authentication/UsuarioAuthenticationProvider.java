package br.com.finch.xlanches.web.infrastructure.spring.security.authentication;

import br.com.finch.xlanches.web.application.UsuarioService;
import br.com.finch.xlanches.web.domain.model.exception.EmailException;
import br.com.finch.xlanches.web.domain.model.exception.SenhaInvalidaException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author Filipe Bojikian Rissi
 */
@Named
public class UsuarioAuthenticationProvider implements AuthenticationProvider {

    @Inject
    private UsuarioService usuarioService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String email = authentication.getName();
        String password = (String) authentication.getCredentials();
        User user;

        try {
            user = new User(usuarioService.loadByEmail(email));
        } catch (EmailException e) {
            throw new BadCredentialsException("Usuário não encontrado");
        }

        if (user == null)
            throw new BadCredentialsException("Usuário não encontrado");

        try {
            user.autenticar(password);
        } catch (SenhaInvalidaException e) {
            throw new BadCredentialsException("Senha inválida");
        }

        return new DefaultUsuarioAuthentication(user, password, user.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }

}