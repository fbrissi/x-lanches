/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.finch.xlanches.web.domain.model.shared;

import javax.persistence.metamodel.SingularAttribute;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * @author Filipe Bojikian Rissi
 */
public interface EntityRepository<T extends EntityObject, ID extends Comparable> {

    int JDBC_BATCH_WRITING = 500;

    enum OrderBy {

        ASC, DESC;
    }

    void persist(T entity);

    void remove(T entity);

    T merge(T entity);

    T find(ID id);

    T first();

    T last();

    T next(ID id);

    T previous(ID id);

    long count();

    List<T> listAll();

    List<T> list(Map<SingularAttribute<T, ? extends Serializable>, Object> param);

    List<T> list(int page, int result);

    void forEachBatch(Collection<T> collection, Consumer<? super T> action);

}
