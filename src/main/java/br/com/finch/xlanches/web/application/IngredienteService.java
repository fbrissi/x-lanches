package br.com.finch.xlanches.web.application;

import br.com.finch.xlanches.web.interfaces.usuario.facade.cadastro.IngredienteDTO;

import javax.json.JsonObject;

/**
 * @author by Filipe Bojikian Rissi
 */
public interface IngredienteService {

    JsonObject listarTodos();

    JsonObject listar(int page, int result);

    IngredienteDTO editar(Integer id);

    void persistir(IngredienteDTO ingredienteDTO);

    void alterar(IngredienteDTO ingredienteDTO);

    void deletar(Integer id);

}
