package br.com.finch.xlanches.web.infrastructure.persistence.jpa.repository;

import br.com.finch.xlanches.web.domain.model.vendas.Cardapio;
import br.com.finch.xlanches.web.domain.model.vendas.CardapioRepository;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

/**
 * @author Filipe Bojikian Rissi
 */
@Named
@RequestScoped
public class CardapioRepositoryImpl extends AbstractRepository<Cardapio, Integer> implements CardapioRepository {

    protected CardapioRepositoryImpl() {
    }

    @Inject
    public CardapioRepositoryImpl(EntityManager entityManager) {
        super(entityManager);
    }

}