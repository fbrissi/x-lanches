package br.com.finch.xlanches.web.infrastructure.thymeleaf.conversion;

import org.springframework.context.MessageSource;
import org.springframework.format.Formatter;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * @author Filipe Bojikian Rissi
 */
public class LocalDateTimeFormatter implements Formatter<LocalDateTime> {

    @Inject
    private MessageSource messageSource;

    @Override
    public LocalDateTime parse(final String text, final Locale locale) {
        final DateTimeFormatter dateFormat = createDateFormat(locale);
        return LocalDateTime.parse(text, dateFormat);
    }

    @Override
    public String print(final LocalDateTime object, final Locale locale) {
        final DateTimeFormatter dateFormat = createDateFormat(locale);
        return dateFormat.format(object);
    }

    private DateTimeFormatter createDateFormat(final Locale locale) {
        final String format = this.messageSource.getMessage("localdatetime.format", null, locale);
        return DateTimeFormatter.ofPattern(format);
    }

}