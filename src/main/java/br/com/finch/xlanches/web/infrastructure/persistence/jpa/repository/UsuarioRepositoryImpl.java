package br.com.finch.xlanches.web.infrastructure.persistence.jpa.repository;

import br.com.finch.xlanches.web.domain.model.usuario.Usuario;
import br.com.finch.xlanches.web.domain.model.usuario.UsuarioRepository;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 * @author Filipe Bojikian Rissi
 */
@Named
@RequestScoped
public class UsuarioRepositoryImpl extends AbstractRepository<Usuario, Integer> implements UsuarioRepository {

    public UsuarioRepositoryImpl() {
    }

    @Inject
    public UsuarioRepositoryImpl(EntityManager entityManager) {
        super(entityManager);
    }


    @Override
    public Usuario findByEmail(String email) {
        TypedQuery<Usuario> query = createNamedQuery(UsuarioRepository.findByEmail, new Parameter("email", email));
        try {
            return query.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

}