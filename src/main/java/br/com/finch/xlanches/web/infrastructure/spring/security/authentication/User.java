package br.com.finch.xlanches.web.infrastructure.spring.security.authentication;

import br.com.finch.xlanches.web.domain.model.exception.SenhaInvalidaException;
import br.com.finch.xlanches.web.domain.model.usuario.Usuario;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Filipe Bojikian Rissi
 */
public class User implements Serializable, UserDetails {

    private static final long serialVersionUID = 1860425854897202912L;

    private Usuario principal;
    private Usuario usuario;
    private String email;
    private String password;
    private Set<Role> authorities;

    public User(Usuario usuario) {
        this.principal = usuario;
        this.email = usuario.getEmail();
        try {
            Field fieldSenha = Usuario.class.getDeclaredField("senha");
            AccessController.doPrivileged((PrivilegedAction) () -> {
                fieldSenha.setAccessible(true);
                return null;
            });

            this.password = (String) fieldSenha.get(usuario);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException();
        }
        this.authorities = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(new Role(this))));
        this.usuario = usuario;
    }

    public User(String ip) {
        this.usuario = null;
        this.email = ip;
        this.authorities = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(new Role(this))));
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Usuario getPrincipal() {
        return principal;
    }

    public void autenticar(String senha) throws SenhaInvalidaException {
        if (principal != null)
            principal.autenticar(senha);
    }

    @Override
    public Set<Role> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}