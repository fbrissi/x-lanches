package br.com.finch.xlanches.web.infrastructure.spring.config;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.context.request.WebRequest;

import javax.inject.Inject;

/**
 * @author Filipe Bojikian Rissi
 */
@ControllerAdvice
public class GlobalBindingInitializer {

    @Inject
    private MessageSource messageSource;

    @InitBinder
    public void registerCustomEditors(WebDataBinder binder, WebRequest request) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

}