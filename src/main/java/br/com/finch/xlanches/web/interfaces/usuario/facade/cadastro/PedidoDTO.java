package br.com.finch.xlanches.web.interfaces.usuario.facade.cadastro;

import br.com.finch.xlanches.web.domain.model.vendas.Pedido;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author by Filipe Bojikian Rissi
 */
public class PedidoDTO implements Serializable {

    private static final long serialVersionUID = 423629871262530114L;

    private Long id;

    @NotNull
    @Size(max = 12)
    private String numero;

    @NotNull
    private LocalDateTime registrado;

    @NotNull
    private Pedido.Status status;

    @Size(max = 180)
    private String observacao;

    private LocalDateTime finalizado;

    @NotNull
    private BigDecimal valor;

    @NotNull
    private BigDecimal desconto;

    @NotNull
    private BigDecimal taxa;

    private BigDecimal pago;

    @NotNull
    private Integer usuarioId;

    public PedidoDTO() {
    }

    public PedidoDTO(Pedido pedido) {
        if (pedido != null) {
            this.id = pedido.getId();
            this.numero = pedido.getNumero();
            this.registrado = pedido.getRegistrado();
            this.status = pedido.getStatus();
            this.observacao = pedido.getObservacao();
            this.finalizado = pedido.getFinalizado();
            this.valor = pedido.getValor();
            this.desconto = pedido.getDesconto();
            this.taxa = pedido.getTaxa();
            this.pago = pedido.getPago();
            this.usuarioId = pedido.getUsuario().getId();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public LocalDateTime getRegistrado() {
        return registrado;
    }

    public void setRegistrado(LocalDateTime registrado) {
        this.registrado = registrado;
    }

    public Pedido.Status getStatus() {
        return status;
    }

    public void setStatus(Pedido.Status status) {
        this.status = status;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public LocalDateTime getFinalizado() {
        return finalizado;
    }

    public void setFinalizado(LocalDateTime finalizado) {
        this.finalizado = finalizado;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public BigDecimal getDesconto() {
        return desconto;
    }

    public void setDesconto(BigDecimal desconto) {
        this.desconto = desconto;
    }

    public BigDecimal getTaxa() {
        return taxa;
    }

    public void setTaxa(BigDecimal taxa) {
        this.taxa = taxa;
    }

    public BigDecimal getPago() {
        return pago;
    }

    public void setPago(BigDecimal pago) {
        this.pago = pago;
    }

    public Integer getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Integer usuarioId) {
        this.usuarioId = usuarioId;
    }

}
