package br.com.finch.xlanches.web.domain.model.shared;

/**
 * @author Filipe Bojikian Rissi
 */
public interface EnumDescription {

    String name();

    String getValor();

    String getDescricao();

}
