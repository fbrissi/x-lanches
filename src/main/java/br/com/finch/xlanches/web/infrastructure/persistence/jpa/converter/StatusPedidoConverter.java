package br.com.finch.xlanches.web.infrastructure.persistence.jpa.converter;

import br.com.finch.xlanches.web.domain.model.vendas.Pedido;
import br.com.finch.xlanches.web.domain.model.vendas.Promocao;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * @author Filipe Bojikian Rissi
 */
@Converter(autoApply = true)
public class StatusPedidoConverter implements AttributeConverter<Pedido.Status, String> {

    @Override
    public String convertToDatabaseColumn(Pedido.Status value) {
        return value == null ? null : value.getValor();
    }

    @Override
    public Pedido.Status convertToEntityAttribute(String value) {
        return value == null ? null : Pedido.Status.fromValor(value);
    }

}