package br.com.finch.xlanches.web.infrastructure.persistence.bigdata.model;

import br.com.finch.xlanches.web.domain.model.shared.EntityObject;
import br.com.finch.xlanches.web.domain.model.shared.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import java.io.StringReader;
import java.util.Objects;

/**
 * @author Filipe Bojikian Rissi
 */
public class UserInformation {

    private static final Logger logger = LoggerFactory.getLogger(UserInformation.class);

    public enum PersistEvent {
        PRE_PERSIST,
        POS_PERSIST,
        PRE_UPDATE,
        POS_UPDATE,
        PRE_REMOVE,
        POS_REMOVE;
    }

    private final String email;
    private final String ip;
    private final int port;
    private final PersistEvent persistEvent;
    private final EntityObject entityObject;
    private final String entityClassName;

    public UserInformation(String email, String ip, int port) {
        this(email, ip, port, null, null);
    }

    private UserInformation(String email, String ip, int port, PersistEvent persistEvent, EntityObject entityObject) {
        this.email = email;
        this.ip = ip;
        this.port = port;
        this.persistEvent = persistEvent;
        this.entityObject = entityObject;
        this.entityClassName = entityObject != null ? entityObject.getClass().getName() : null;
    }

    public String getEmail() {
        return email;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public PersistEvent getPersistEvent() {
        return persistEvent;
    }

    public EntityObject getEntityObject() {
        return entityObject;
    }

    public UserInformation clone(PersistEvent persistEvent, EntityObject entityObject) {
        return new UserInformation(email, ip, port, persistEvent, entityObject);
    }

    public JsonObject toJSON() {
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();

        if (email == null) {
            jsonObjectBuilder.addNull("email");
        } else {
            jsonObjectBuilder.add("email", email);
        }
        jsonObjectBuilder.add("ip", ip);
        jsonObjectBuilder.add("port", port);
        jsonObjectBuilder.add("persistEvent", persistEvent.name());
        jsonObjectBuilder.add("entityObject", entityObject.toJSON());
        jsonObjectBuilder.add("entityClassName", entityClassName);

        return jsonObjectBuilder.build();
    }

    public static UserInformation fromJSON(String json) {
        try (JsonReader reader = Json.createReader(new StringReader(json))) {
            JsonObject jsonObject = reader.readObject();
            JsonObject entityObject = jsonObject.getJsonObject("entityObject");
            return new UserInformation(jsonObject.isNull("email") ? null : jsonObject.getString("email"), jsonObject.getString("ip"),
                    jsonObject.getInt("port"), PersistEvent.valueOf(jsonObject.getString("persistEvent")), entityObject == null ? null : (EntityObject) JSONObject.fromJSON((Class) Class.forName(jsonObject.getString("entityClassName")), entityObject.toString()));
        } catch (ClassNotFoundException e) {
            logger.error("Erro na conversão do JSON para objeto.", e);
            return null;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserInformation that = (UserInformation) o;
        return port == that.port &&
                Objects.equals(email, that.email) &&
                Objects.equals(ip, that.ip) &&
                persistEvent == that.persistEvent &&
                Objects.equals(entityObject, that.entityObject) &&
                Objects.equals(entityClassName, that.entityClassName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, ip, port, persistEvent, entityObject, entityClassName);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserInformation{");
        sb.append("email='").append(email).append('\'');
        sb.append(", ip='").append(ip).append('\'');
        sb.append(", port=").append(port);
        sb.append(", persistEvent=").append(persistEvent);
        sb.append(", entityObject=").append(entityObject.getId());
        sb.append(", entityClassName=").append(entityClassName);
        sb.append('}');
        return sb.toString();
    }

}