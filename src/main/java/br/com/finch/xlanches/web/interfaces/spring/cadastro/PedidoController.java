package br.com.finch.xlanches.web.interfaces.spring.cadastro;

import br.com.finch.xlanches.web.application.PedidoService;
import br.com.finch.xlanches.web.interfaces.spring.error.EntityNotFoundException;
import br.com.finch.xlanches.web.interfaces.usuario.facade.cadastro.PedidoDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.validation.Valid;

/**
 * @author Filipe Bojikian Rissi
 */
@Controller
@RequestMapping(value = "/pedido")
public class PedidoController {

    @Inject
    private PedidoService pedidoService;

    @RequestMapping(method = RequestMethod.GET)
    public String init() {
        return "cadastro/pedido/listar";
    }

    @ResponseBody
    @RequestMapping(value = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String listar(int start, int length) {
        JsonObject jsonObject = pedidoService.listar(start, length);
        return jsonObject.toString();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String editar(@PathVariable("id") Long id, Model model) {
        PedidoDTO pedido = pedidoService.editar(id);
        if (pedido == null) {
            throw new EntityNotFoundException();
        }
        model.addAttribute("pedido", pedido);
        return "cadastro/pedido/formulario";
    }

    @RequestMapping(value = "/cadastrar", method = RequestMethod.GET)
    public String cadastrar(Model model) {
        model.addAttribute("pedido", new PedidoDTO());
        return "cadastro/pedido/formulario";
    }

    @RequestMapping(value = "/cadastrar", method = RequestMethod.PUT)
    public String alterar(@ModelAttribute("pedido") @Valid PedidoDTO pedido, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "cadastro/pedido/formulario :: form";
        }
        pedidoService.alterar(pedido);
        model.addAttribute("redirect", "/pedido");
        return "redirect :: url";
    }

    @RequestMapping(value = "/cadastrar", method = RequestMethod.POST)
    public String persistir(@ModelAttribute("pedido") @Valid PedidoDTO pedido, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "cadastro/pedido/formulario :: form";
        }
        pedidoService.persistir(pedido);
        model.addAttribute("redirect", "/pedido");
        return "redirect :: url";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deletar(@PathVariable("id") Long id) {
        pedidoService.deletar(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}