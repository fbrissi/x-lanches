package br.com.finch.xlanches.web.domain.model.vendas;

import br.com.finch.xlanches.web.domain.model.shared.AuditingEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * @author Filipe Bojikian Rissi
 */
@Entity
@Table(name = "ingrediente")
@SequenceGenerator(name = "ingrediente_id_seq", sequenceName = "ingrediente_id_seq", allocationSize = 1)
public class Ingrediente extends AuditingEntity<Ingrediente, Integer> {

    private static final long serialVersionUID = -7516765028029820765L;

    @Id
    @GeneratedValue(generator = "ingrediente_id_seq")
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Size(max = 55)
    @Column(name = "nome")
    private String nome;

    @Size(max = 180)
    @Column(name = "observacao")
    private String observacao;

    @Basic(optional = false)
    @NotNull
    @Column(name = "valor")
    private BigDecimal valor;

    protected Ingrediente() {
        super();
    }

    public Ingrediente(String nome, String observacao, BigDecimal valor) {
        super();
        assert (nome != null);
        assert (valor != null);

        this.nome = nome;
        this.observacao = observacao;
        this.valor = valor;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null)
            return false;

        if (this == object)
            return true;

        if (!(object instanceof Ingrediente))
            return false;

        Ingrediente other = (Ingrediente) object;
        return sameId(other);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.id);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Ingrediente{");
        sb.append("id=").append(id);
        sb.append(", nome='").append(nome).append('\'');
        sb.append(", observacao='").append(observacao).append('\'');
        sb.append(", valor=").append(valor);
        sb.append('}');
        return sb.toString();
    }

}