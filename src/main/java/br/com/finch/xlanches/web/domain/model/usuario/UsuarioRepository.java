package br.com.finch.xlanches.web.domain.model.usuario;

import br.com.finch.xlanches.web.domain.model.shared.EntityRepository;

/**
 * @author Filipe Bojikian Rissi
 */
public interface UsuarioRepository extends EntityRepository<Usuario, Integer> {

    String findByEmail = "Usuario.findByEmail";

    Usuario findByEmail(String email);

}
