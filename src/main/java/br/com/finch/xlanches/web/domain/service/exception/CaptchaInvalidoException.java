package br.com.finch.xlanches.web.domain.service.exception;

import org.springframework.transaction.TransactionException;

/**
 * @author Filipe Bojikian Rissi
 */
public class CaptchaInvalidoException extends TransactionException {

    private static final long serialVersionUID = -2774903810510797093L;

    public CaptchaInvalidoException(String msg) {
        super(msg);
    }

    public CaptchaInvalidoException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
