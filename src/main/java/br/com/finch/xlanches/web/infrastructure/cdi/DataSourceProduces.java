package br.com.finch.xlanches.web.infrastructure.cdi;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * @author Filipe Bojikian Rissi
 */
public class DataSourceProduces {

    public static DataSource getDataSource(String dataSource) throws NamingException {
        return (DataSource) new InitialContext().lookup(dataSource);
    }

}
