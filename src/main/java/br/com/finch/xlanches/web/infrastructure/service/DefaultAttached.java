package br.com.finch.xlanches.web.infrastructure.service;

import br.com.finch.xlanches.web.domain.service.Attached;

/**
 * @author Filipe Bojikian Rissi
 */
public class DefaultAttached implements Attached {

    private static final long serialVersionUID = -7996665921486092573L;

    private byte[] file;

    private String name;

    private String description;

    private String type;

    public DefaultAttached(byte[] file, String name, String description, String type) {
        this.file = file.clone();
        this.name = name;
        this.description = description;
        this.type = type;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public byte[] getFile() {
        return file.clone();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }
}