package br.com.finch.xlanches.web.domain.model.shared;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author Filipe Bojikian Rissi
 */
public class ResourceBundleException extends Exception {

    private static final long serialVersionUID = 2412558567562695745L;

    private static final String DEFAULT_MESSAGE_BUNDLE = "br.com.finch.xlanches.web.infrastructure.exception.Messages";

    public ResourceBundleException() {
    }

    public ResourceBundleException(String message) {
        super(message);
    }

    public ResourceBundleException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResourceBundleException(Throwable cause) {
        super(cause);
    }

    public ResourceBundleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    @Override
    public String getMessage() {
        String message = super.getMessage();

        if (message == null)
            return null;

        if (message.matches("\\{[\\w.]+\\}")) {
            ResourceBundle bundle = ResourceBundle.getBundle(DEFAULT_MESSAGE_BUNDLE, Locale.getDefault());
            return bundle.getString(message.substring(1, message.length() - 1));
        }

        return message;
    }

}