package br.com.finch.xlanches.web.infrastructure.cdi;

import javax.mail.Session;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * @author Filipe Bojikian Rissi
 */
public class SessionProduces {

    public static Session getSession() throws NamingException {
        return (Session) new InitialContext().lookup("java:comp/env/mail/Session");
    }

}