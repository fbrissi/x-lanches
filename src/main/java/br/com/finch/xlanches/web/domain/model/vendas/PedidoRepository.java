package br.com.finch.xlanches.web.domain.model.vendas;

import br.com.finch.xlanches.web.domain.model.shared.EntityRepository;

/**
 * @author Filipe Bojikian Rissi
 */
public interface PedidoRepository extends EntityRepository<Pedido, Long> {
}