package br.com.finch.xlanches.web.interfaces.usuario.facade.cadastro;

import br.com.finch.xlanches.web.domain.model.vendas.Ingrediente;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Filipe Bojikian Rissi
 */
public class IngredienteDTO implements Serializable {

    private static final long serialVersionUID = 6231292846941702467L;

    private Integer id;

    @NotNull
    @Size(max = 55)
    private String nome;

    @Size(max = 180)
    private String observacao;

    @NotNull
    private BigDecimal valor;


    public IngredienteDTO() {
    }

    public IngredienteDTO(Ingrediente ingrediente) {
        if (ingrediente != null) {
            this.id = ingrediente.getId();
            this.nome = ingrediente.getNome();
            this.observacao = ingrediente.getObservacao();
            this.valor = ingrediente.getValor();
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

}
