package br.com.finch.xlanches.web.interfaces.usuario.facade.cadastro;

import br.com.finch.xlanches.web.domain.model.vendas.Promocao;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Filipe Bojikian Rissi
 */
public class PromocaoDTO implements Serializable {

    private static final long serialVersionUID = -1165123606206019036L;

    private Integer id;

    @NotNull
    @Size(max = 55)
    private String nome;

    @NotNull
    private BigDecimal desconto;

    @NotNull
    private Promocao.TipoDesconto tipoDesconto;

    @NotNull
    private List<IngredienteItemDTO> ingredientes;

    public PromocaoDTO() {
        ingredientes = new ArrayList<>();
    }

    public PromocaoDTO(Promocao promocao) {
        if (promocao != null) {
            this.id = promocao.getId();
            this.nome = promocao.getNome();
            this.desconto = promocao.getDesconto();
            this.tipoDesconto = promocao.getTipoDesconto();
            this.ingredientes = new ArrayList<>();
            promocao.getIngredientes().forEach(item -> this.ingredientes.add(new IngredienteItemDTO(item)));
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BigDecimal getDesconto() {
        return desconto;
    }

    public void setDesconto(BigDecimal desconto) {
        this.desconto = desconto;
    }

    public Promocao.TipoDesconto getTipoDesconto() {
        return tipoDesconto;
    }

    public void setTipoDesconto(Promocao.TipoDesconto tipoDesconto) {
        this.tipoDesconto = tipoDesconto;
    }

    public List<IngredienteItemDTO> getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(List<IngredienteItemDTO> ingredientes) {
        this.ingredientes = ingredientes;
    }

}