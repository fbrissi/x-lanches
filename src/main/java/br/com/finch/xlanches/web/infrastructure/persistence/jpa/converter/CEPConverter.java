package br.com.finch.xlanches.web.infrastructure.persistence.jpa.converter;

import br.com.finch.xlanches.web.domain.model.identificacao.CEP;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * @author Filipe Bojikian Rissi
 */
@Converter(autoApply = true)
public class CEPConverter implements AttributeConverter<CEP, String> {

    @Override
    public String convertToDatabaseColumn(CEP value) {
        return value == null ? null : value.toString();
    }

    @Override
    public CEP convertToEntityAttribute(String value) {
        return value == null ? null : new CEP(value);
    }

}