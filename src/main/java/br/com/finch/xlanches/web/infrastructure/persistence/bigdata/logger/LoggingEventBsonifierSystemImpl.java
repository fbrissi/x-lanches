package br.com.finch.xlanches.web.infrastructure.persistence.bigdata.logger;

import br.com.finch.xlanches.web.domain.model.shared.EnumDescription;
import br.com.finch.xlanches.web.domain.model.shared.ValuesObject;
import br.com.finch.xlanches.web.infrastructure.persistence.bigdata.model.UserInformation;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.apache.log4j.spi.LoggingEvent;
import org.log4mongo.LoggingEventBsonifierImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Filipe Bojikian Rissi
 */
public class LoggingEventBsonifierSystemImpl extends LoggingEventBsonifierImpl {

    final Logger logger = LoggerFactory.getLogger(getClass());

    public static final String JSON_MESSAGE = "JSON: ";
    private static final String KEY_TYPE_LOGGER = "type";
    private static final String KEY_USER_INFORMATION = "userInformation";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_IP = "ip";
    private static final String KEY_PORT = "port";
    private static final String KEY_EVENT = "event";
    private static final String KEY_ENTITY = "entity";

    private static final String TYPE_LOGGER_SYSTEM = "SYSTEM";
    private static final String TYPE_LOGGER_PERSISTENCE = "PERSISTENCE";

    @Override
    public DBObject bsonify(LoggingEvent loggingEvent) {
        String renderedMessage = loggingEvent.getRenderedMessage();
        if (!renderedMessage.startsWith(JSON_MESSAGE)) {
            DBObject dbObject = super.bsonify(loggingEvent);
            dbObject.put(KEY_TYPE_LOGGER, TYPE_LOGGER_SYSTEM);
            return dbObject;
        }

        DBObject clientInfo = new BasicDBObject();
        UserInformation userInformation = UserInformation.fromJSON(renderedMessage.substring(JSON_MESSAGE.length()));
        clientInfo.put(KEY_EMAIL, userInformation.getEmail());
        clientInfo.put(KEY_IP, userInformation.getIp());
        clientInfo.put(KEY_PORT, userInformation.getPort());
        clientInfo.put(KEY_EVENT, userInformation.getPersistEvent().name());
        Object entityObjectDB = objectSafePut(userInformation.getEntityObject());
        if (entityObjectDB != null)
            clientInfo.put(KEY_ENTITY, entityObjectDB);

        DBObject dbObject = super.bsonify(loggingEvent);
        dbObject.put(KEY_TYPE_LOGGER, TYPE_LOGGER_PERSISTENCE);
        dbObject.put(KEY_USER_INFORMATION, clientInfo);
        return dbObject;
    }

    protected Object objectSafePut(final Object entityObject) {
        if (entityObject != null) {
            DBObject entityObjectDB = new BasicDBObject();

            Class clazz = entityObject.getClass();
            try {
                do {
                    for (Field field : clazz.getDeclaredFields()) {
                        if (Modifier.isFinal(field.getModifiers()))
                            continue;

                        AccessController.doPrivileged((PrivilegedAction<Object>) () -> {
                            field.setAccessible(true);
                            return null;
                        });

                        if (EnumDescription.class.isAssignableFrom(field.getType())) {
                            DBObject objectEnum = new BasicDBObject();
                            EnumDescription enumDescription = (EnumDescription) field.get(entityObject);
                            if (enumDescription != null) {
                                objectEnum.put("name", enumDescription.name());
                                objectEnum.put("descricao", enumDescription.getDescricao());
                                objectEnum.put("valor", enumDescription.getValor());
                                entityObjectDB.put(field.getName(), objectEnum);
                            } else {
                                entityObjectDB.put(field.getName(), null);
                            }
                        } else if (Enum.class.isAssignableFrom(field.getType())) {
                            Enum value = ((Enum) field.get(entityObject));
                            if (value != null) {
                                entityObjectDB.put(field.getName(), value.name());
                            } else {
                                entityObjectDB.put(field.getName(), null);
                            }
                        } else if (BigDecimal.class.isAssignableFrom(field.getType())) {
                            entityObjectDB.put(field.getName(), field.get(entityObject));
                        } else if (BigInteger.class.isAssignableFrom(field.getType())) {
                            entityObjectDB.put(field.getName(), field.get(entityObject));
                        } else if (Boolean.class.isAssignableFrom(field.getType())) {
                            entityObjectDB.put(field.getName(), field.get(entityObject));
                        } else if (Double.class.isAssignableFrom(field.getType())) {
                            entityObjectDB.put(field.getName(), field.get(entityObject));
                        } else if (Integer.class.isAssignableFrom(field.getType())) {
                            entityObjectDB.put(field.getName(), field.get(entityObject));
                        } else if (Long.class.isAssignableFrom(field.getType())) {
                            entityObjectDB.put(field.getName(), field.get(entityObject));
                        } else if (LocalDate.class.isAssignableFrom(field.getType())) {
                            LocalDate date = ((LocalDate) field.get(entityObject));
                            if (date != null) {
                                entityObjectDB.put(field.getName(), date.format(DateTimeFormatter.ISO_LOCAL_DATE));
                            } else {
                                entityObjectDB.put(field.getName(), null);
                            }
                        } else if (LocalDateTime.class.isAssignableFrom(field.getType())) {
                            LocalDateTime date = ((LocalDateTime) field.get(entityObject));
                            if (date != null) {
                                entityObjectDB.put(field.getName(), date.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
                            } else {
                                entityObjectDB.put(field.getName(), null);
                            }
                        } else if (String.class.isAssignableFrom(field.getType())) {
                            entityObjectDB.put(field.getName(), field.get(entityObject));
                        } else if (ValuesObject.class.isAssignableFrom(field.getType())) {
                            Object object = objectSafePut(field.get(entityObject));
                            if (object != null) {
                                entityObjectDB.put(field.getName(), object);
                            } else {
                                entityObjectDB.put(field.getName(), null);
                            }
                        }
                    }
                    clazz = clazz.getSuperclass();
                } while (clazz != null);
            } catch (IllegalAccessException e) {
                logger.error("Erro na conversão do objeto para persistência DBObject.", e);
            }

            return entityObjectDB;
        }

        return null;
    }

}