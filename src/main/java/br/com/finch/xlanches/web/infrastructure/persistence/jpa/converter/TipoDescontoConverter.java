package br.com.finch.xlanches.web.infrastructure.persistence.jpa.converter;

import br.com.finch.xlanches.web.domain.model.vendas.Promocao;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * @author Filipe Bojikian Rissi
 */
@Converter(autoApply = true)
public class TipoDescontoConverter implements AttributeConverter<Promocao.TipoDesconto, String> {

    @Override
    public String convertToDatabaseColumn(Promocao.TipoDesconto value) {
        return value == null ? null : value.getValor();
    }

    @Override
    public Promocao.TipoDesconto convertToEntityAttribute(String value) {
        return value == null ? null : Promocao.TipoDesconto.fromValor(value);
    }

}