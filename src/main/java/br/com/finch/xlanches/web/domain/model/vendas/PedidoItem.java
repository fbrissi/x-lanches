package br.com.finch.xlanches.web.domain.model.vendas;

import br.com.finch.xlanches.web.domain.model.shared.EntityObject;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author Filipe Bojikian Rissi
 */
@Entity
@Table(name = "pedido_item")
public class PedidoItem implements EntityObject<PedidoItem, PedidoItemPK> {

    private static final long serialVersionUID = -1350849086704364549L;

    @EmbeddedId
    private PedidoItemPK id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "quantidade")
    private BigDecimal quantidade;

    @NotNull
    @JoinColumn(name = "cardapio_id", referencedColumnName = "id")
    @ManyToOne(cascade = CascadeType.DETACH, optional = false)
    private Cardapio cardapio;

    @NotNull
    @MapsId("pedidoId")
    @JoinColumn(name = "pedido_id", referencedColumnName = "id")
    @ManyToOne(cascade = CascadeType.DETACH, optional = false)
    private Pedido pedido;

    @ElementCollection
    @CollectionTable(name = "pedido_item_ingrediente", joinColumns = {
            @JoinColumn(name = "pedido_id", referencedColumnName = "pedido_id"),
            @JoinColumn(name = "pedido_seq", referencedColumnName = "pedido_seq")
    })
    private Set<IngredienteItem> ingredientes;

    protected PedidoItem() {
        super();
        this.ingredientes = new HashSet<>();
    }

    protected PedidoItem(Integer pedidoSeq, BigDecimal quantidade, Cardapio cardapio, Pedido pedido) {
        this.id = new PedidoItemPK(pedidoSeq);
        this.quantidade = quantidade;
        this.cardapio = cardapio;
        this.pedido = pedido;
        this.ingredientes = new HashSet<>();
    }

    @Override
    public PedidoItemPK getId() {
        return id;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public Cardapio getCardapio() {
        return cardapio;
    }

    public Set<IngredienteItem> getIngredientes() {
        return Collections.unmodifiableSet(ingredientes);
    }

    public boolean addIngrediente(IngredienteItem pedidoItemIngrediente) {
        assert (pedidoItemIngrediente != null);

        return this.ingredientes.add(pedidoItemIngrediente);
    }

    public boolean removerIngrediente(IngredienteItem pedidoItemIngrediente) {
        assert (pedidoItemIngrediente != null);

        return this.ingredientes.remove(pedidoItemIngrediente);
    }

    public void removerTodosIngrediente() {
        this.ingredientes.clear();
    }

    public BigDecimal getValor() {
        return getValorIngredientes().add(quantidade.multiply(cardapio.getValor()));
    }

    public BigDecimal getDesconto() {
        BigDecimal desconto = BigDecimal.ZERO;
        Promocao promocao = cardapio.getPromocao();
        if (promocao != null) {
            Promocao.TipoDesconto tipoDesconto = promocao.getTipoDesconto();
            boolean isDesconto = true;
            for (IngredienteItem ingredienteItem : promocao.getIngredientes()) {
                BigDecimal count = countIngredientes(ingredienteItem.getIngrediente());
                if (BigDecimal.ZERO.equals(ingredienteItem.getQuantidade()) && BigDecimal.ZERO.equals(count)) {
                    continue;
                }

                if (BigDecimal.ZERO.equals(count.remainder(ingredienteItem.getQuantidade()))) {
                    if (Promocao.TipoDesconto.QUANTIDADE.equals(tipoDesconto)) {
                        desconto = desconto.add(count.divide(ingredienteItem.getQuantidade())
                                .multiply(ingredienteItem.getIngrediente().getValor()));
                    }

                    continue;
                }

                isDesconto = false;
                break;
            }

            if (isDesconto) {
                if (Promocao.TipoDesconto.PERCENTUAL.equals(tipoDesconto)) {
                    BigDecimal fator = promocao.getDesconto().divide(new BigDecimal(100));
                    desconto = desconto.add(desconto.multiply(fator));
                }
            }

        }

        return desconto;
    }

    public BigDecimal getValorIngredientes() {
        return ingredientes.stream().map(item -> item.getValor()).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private BigDecimal countIngredientes(final Ingrediente ingrediente) {
        return ingredientes.stream().filter(item -> item.getIngrediente().equals(ingrediente)).map(item -> item.getIngrediente().getValor()).reduce(BigDecimal.ZERO, BigDecimal::add)
                .add(cardapio.getIngredientes().stream().filter(item -> item.getIngrediente().equals(ingrediente)).map(item -> item.getIngrediente().getValor()).reduce(BigDecimal.ZERO, BigDecimal::add));
    }

    @Override
    public boolean equals(Object object) {
        if (object == null)
            return false;

        if (this == object)
            return true;

        if (!(object instanceof PedidoItem))
            return false;

        PedidoItem other = (PedidoItem) object;
        return sameId(other);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.id);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PedidoItem{");
        sb.append(", quantidade=").append(quantidade);
        sb.append(", cardapio=").append(cardapio);
        sb.append(", ingredientes=").append(Arrays.toString(ingredientes.toArray()));
        sb.append('}');
        return sb.toString();
    }

}