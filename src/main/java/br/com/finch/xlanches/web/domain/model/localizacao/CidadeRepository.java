package br.com.finch.xlanches.web.domain.model.localizacao;

import br.com.finch.xlanches.web.domain.model.shared.EntityRepository;

import java.util.List;

/**
 * @author Filipe Bojikian Rissi
 */
public interface CidadeRepository extends EntityRepository<Cidade, Integer> {

    String findAll = "Cidade.findAll";
    String findById = "Cidade.findById";
    String findByCodigo = "Cidade.findByCodigo";
    String selectByNome = "Cidade.selectByNome";
    String selectByEstado = "Cidade.selectByEstado";

    List<Cidade> listByEstado(Integer estado);

    List<Cidade> listByNome(String name);

    Cidade findByCodigo(String codigo);

}
