package br.com.finch.xlanches.web.interfaces.spring.usuario;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Filipe Bojikian Rissi
 */
@Controller
@RequestMapping(value = "/usuario")
public class UsuarioController {

    @RequestMapping(method = RequestMethod.GET)
    public String init() {
        return "usuario/home";
    }

}