package br.com.finch.xlanches.web.infrastructure.persistence.exception;

import org.springframework.dao.*;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.InvalidResultSetAccessException;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author Filipe Bojikian Rissi
 */
public class PersistenceMessageException extends RuntimeException {

    private static final long serialVersionUID = -5352102673081740002L;

    private static final String DEFAULT_MESSAGE_BUNDLE = "br.com.finch.xlanches.web.infrastructure.persistence.Errors";

    private String key;
    private String error;

    public PersistenceMessageException() {
    }

    public PersistenceMessageException(String message) {
        super(message);
    }

    public PersistenceMessageException(Throwable cause) {
        super(cause);

        if (cause == null) {
            error = "{xlanches.persistence.errors.message.unexpected.error}";
            key = "{xlanches.persistence.errors.message.unexpected.message}";
        } else if (cause instanceof BadSqlGrammarException) {
            error = "{xlanches.persistence.errors.message.invalid_sql.error}";
            key = "{xlanches.persistence.errors.message.invalid_sql.message}";
        } else if (cause instanceof InvalidResultSetAccessException) {
            error = "{xlanches.persistence.errors.message.invalid_result.error}";
            key = "{xlanches.persistence.errors.message.invalid_result.message}";
        } else if (cause instanceof DuplicateKeyException) {
            error = "{xlanches.persistence.errors.message.primary_constraint.error}";
            key = "{xlanches.persistence.errors.message.primary_constraint.message}";
        } else if (cause instanceof DataIntegrityViolationException) {
            error = "{xlanches.persistence.errors.message.violation_constraint.error}";
            key = "{xlanches.persistence.errors.message.violation_constraint.message}";
        } else if (cause instanceof PermissionDeniedDataAccessException) {
            error = "{xlanches.persistence.errors.message.permission_denied.error}";
            key = "{xlanches.persistence.errors.message.permission_denied.message}";
        } else if (cause instanceof DataAccessResourceFailureException) {
            error = "{xlanches.persistence.errors.message.falied_connect.error}";
            key = "{xlanches.persistence.errors.message.falied_connect.message}";
        } else if (cause instanceof TransientDataAccessResourceException) {
            error = "{xlanches.persistence.errors.message.transient_connect.error}";
            key = "{xlanches.persistence.errors.message.transient_connect.message}";
        } else if (cause instanceof CannotAcquireLockException) {
            error = "{xlanches.persistence.errors.message.lock.error}";
            key = "{xlanches.persistence.errors.message.lock.message}";
        } else if (cause instanceof DeadlockLoserDataAccessException) {
            error = "{xlanches.persistence.errors.message.deadlock.error}";
            key = "{xlanches.persistence.errors.message.deadlock.message}";
        } else if (cause instanceof CannotSerializeTransactionException) {
            error = "{xlanches.persistence.errors.message.serializable.error}";
            key = "{xlanches.persistence.errors.message.serializable.message}";
        } else {
            error = "{xlanches.persistence.errors.message.unexpected.error}";
            key = "{xlanches.persistence.errors.message.unexpected.message}";
        }
    }

    private String getResource(String key) {
        if (key == null)
            return null;

        if (key.matches("\\{[\\w.]+\\}")) {
            ResourceBundle bundle = ResourceBundle.getBundle(DEFAULT_MESSAGE_BUNDLE, Locale.getDefault());
            return bundle.getString(key.substring(1, key.length() - 1));
        }

        return null;
    }

    @Override
    public String getMessage() {
        return getResource(key != null ? key : super.getMessage());
    }

    public String getError() {
        return getResource(error != null ? error : super.getMessage());
    }

}