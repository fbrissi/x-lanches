package br.com.finch.xlanches.web.infrastructure.spring.security.authentication;

import org.springframework.security.core.Authentication;

/**
 * @author Filipe Bojikian Rissi
 */
public interface UsuarioAuthentication extends Authentication {

    User getUser();

}