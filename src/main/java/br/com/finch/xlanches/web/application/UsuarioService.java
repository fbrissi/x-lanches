package br.com.finch.xlanches.web.application;

import br.com.finch.xlanches.web.domain.model.contato.Telefone;
import br.com.finch.xlanches.web.domain.model.exception.ConfirmarSenhaException;
import br.com.finch.xlanches.web.domain.model.exception.EmailException;
import br.com.finch.xlanches.web.domain.model.exception.EncryptionException;
import br.com.finch.xlanches.web.domain.model.exception.SenhaInvalidaException;
import br.com.finch.xlanches.web.domain.model.usuario.Usuario;
import br.com.finch.xlanches.web.domain.service.exception.EnvioEmailException;
import br.com.finch.xlanches.web.domain.service.exception.ExpiredException;
import br.com.finch.xlanches.web.domain.service.exception.InvalidException;
import br.com.finch.xlanches.web.interfaces.usuario.facade.usuario.UsuarioDTO;

/**
 * @author by Filipe Bojikian Rissi
 */
public interface UsuarioService {

    Usuario find(String email);

    void redefinirSenha(String email) throws EmailException, EnvioEmailException;

    Usuario loadByEmail(String email) throws EmailException;

    void alterarSenha(String senha, String novaSenha, String confirmarNovaSenha) throws SenhaInvalidaException, ConfirmarSenhaException;

    void mergeUsuario(UsuarioDTO perfilDTO) throws EnvioEmailException, EncryptionException, EmailException;

    String updateEmail(String chave) throws EnvioEmailException, EncryptionException, InvalidException, ExpiredException, EmailException;

    void criarUsuario(String email, String nome, Telefone telefone, String senha, String confirmarSenha) throws SenhaInvalidaException, EmailException, EnvioEmailException;

}
