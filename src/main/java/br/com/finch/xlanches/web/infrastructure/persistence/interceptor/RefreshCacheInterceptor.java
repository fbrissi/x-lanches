package br.com.finch.xlanches.web.infrastructure.persistence.interceptor;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;

/**
 * @author Filipe Bojikian Rissi
 */
@Interceptor
@RefreshCache
@Priority(Interceptor.Priority.LIBRARY_AFTER)
public class RefreshCacheInterceptor {

    @Inject
    private EntityManager entityManager;

    @AroundInvoke
    public Object intercept(InvocationContext context) throws Exception {
        try {
            return context.proceed();
        } catch (Exception ex) {
            throw ex;
        } finally {
            entityManager.getEntityManagerFactory().getCache().evictAll();
        }
    }

}
