package br.com.finch.xlanches.web.application;

import br.com.finch.xlanches.web.interfaces.usuario.facade.cadastro.CardapioDTO;

import javax.json.JsonObject;

/**
 * @author by Filipe Bojikian Rissi
 */
public interface CardapioService {

    JsonObject listar(int page, int result);

    CardapioDTO editar(Integer id);

    void persistir(CardapioDTO cardapioDTO);

    void alterar(CardapioDTO cardapioDTO);

    void deletar(Integer id);

}
