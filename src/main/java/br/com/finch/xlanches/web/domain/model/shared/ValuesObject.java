package br.com.finch.xlanches.web.domain.model.shared;

/**
 * @author Filipe Bojikian Rissi
 */
public interface ValuesObject<T extends ValuesObject<T>> extends JSONObject, Comparable<T> {

    boolean sameValue(T other);

}