package br.com.finch.xlanches.web.infrastructure.thymeleaf.conversion;

import org.springframework.context.MessageSource;
import org.springframework.format.Formatter;

import javax.inject.Inject;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * @author Filipe Bojikian Rissi
 */
public class LocalDateFormatter implements Formatter<LocalDate> {

    @Inject
    private MessageSource messageSource;

    @Override
    public LocalDate parse(final String text, final Locale locale) {
        final DateTimeFormatter dateFormat = createDateFormat(locale);
        return LocalDate.parse(text, dateFormat);
    }

    @Override
    public String print(final LocalDate object, final Locale locale) {
        final DateTimeFormatter dateFormat = createDateFormat(locale);
        return dateFormat.format(object);
    }

    private DateTimeFormatter createDateFormat(final Locale locale) {
        final String format = this.messageSource.getMessage("localdate.format", null, locale);
        return DateTimeFormatter.ofPattern(format);
    }

}