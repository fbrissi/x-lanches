package br.com.finch.xlanches.web.domain.model.shared;

import java.util.Objects;

/**
 * @author Filipe Bojikian Rissi
 */
public interface EntityObject<T extends EntityObject, ID extends Comparable> extends JSONObject {

    ID getId();

    default boolean sameId(T other) {
        return Objects.equals(this.getId(), other.getId());
    }

}