package br.com.finch.xlanches.web.interfaces.usuario.facade.cadastro;

import br.com.finch.xlanches.web.domain.model.vendas.IngredienteItem;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Filipe Bojikian Rissi
 */
public class IngredienteItemDTO implements Serializable {

    private static final long serialVersionUID = -4054558689273104746L;

    @NotNull
    private BigDecimal quantidade;

    @NotNull
    private IngredienteDTO ingrediente;

    public IngredienteItemDTO() {
    }

    public IngredienteItemDTO(IngredienteItem ingredienteItem) {
        if (ingredienteItem != null) {
            this.quantidade = ingredienteItem.getQuantidade();
            this.ingrediente = new IngredienteDTO(ingredienteItem.getIngrediente());
        }
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public IngredienteDTO getIngrediente() {
        return ingrediente;
    }

    public void setIngrediente(IngredienteDTO ingrediente) {
        this.ingrediente = ingrediente;
    }

}
