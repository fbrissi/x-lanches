package br.com.finch.xlanches.web.interfaces.spring.shared;

import br.com.finch.xlanches.web.infrastructure.persistence.exception.PersistenceMessageException;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/**
 * @author Filipe Bojikian Rissi
 */
@Controller
@RequestMapping(value = "/erro")
public class ErroController {

    @Inject
    private MessageSource messageSource;

    @RequestMapping(value = "/acesso_negado")
    public String acessoNegado() {
        return "erro/acesso_negado";
    }

    @RequestMapping(value = "/400")
    public String erro400() {
        return "erro/400";
    }

    @RequestMapping(value = "/404")
    public String erro404() {
        return "erro/404";
    }

    @RequestMapping(value = "/405")
    public String erro405() {
        return "erro/405";
    }

    @RequestMapping("/exception")
    public String exception(HttpServletRequest request, Model model, Locale locale) {
        model.addAttribute("errorCode", request.getAttribute("javax.servlet.error.status_code"));
        Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");
        if (throwable.getCause() != null) {
            throwable = throwable.getCause();
        }
        if (throwable instanceof PersistenceMessageException) {
            model.addAttribute("errorTitle", ((PersistenceMessageException) throwable).getError());
            model.addAttribute("errorMessage", throwable.getMessage());
        } else {
            model.addAttribute("errorTitle", messageSource.getMessage("xlanches.error.title", null, locale));
            model.addAttribute("errorMessage", throwable.getMessage());
        }
        return "erro/exception";
    }

}
