package br.com.finch.xlanches.web.infrastructure.spring.security.filter;

import br.com.finch.xlanches.web.infrastructure.spring.security.authentication.DefaultUsuarioAuthentication;
import br.com.finch.xlanches.web.infrastructure.spring.security.authentication.User;
import org.springframework.security.core.Authentication;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author Filipe Bojikian Rissi
 */
public class PaginaManutencaoFilter extends GenericFilterBean {

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();

        if ("/manutencao".equals(request.getServletPath())) {
            chain.doFilter(req, res);
        }

        chain.doFilter(req, res);
    }

    protected Authentication createAuthentication(HttpServletRequest request) {
        User user = new User(request.getRemoteAddr());
        Authentication auth = new DefaultUsuarioAuthentication(user, null, user.getAuthorities());
        return auth;
    }

}