/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.finch.xlanches.web.domain.model.localizacao;

import br.com.finch.xlanches.web.domain.model.shared.EntityObject;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

/**
 * @author Filipe Bojikian Rissi
 */
@Entity
@Table(name = "cidade")
@SequenceGenerator(name = "cidade_id_seq", sequenceName = "cidade_id_seq", allocationSize = 1)
@NamedQueries({
        @NamedQuery(name = CidadeRepository.findAll, query = "SELECT c FROM Cidade c"),
        @NamedQuery(name = CidadeRepository.findById, query = "SELECT c FROM Cidade c WHERE c.id = :id"),
        @NamedQuery(name = CidadeRepository.findByCodigo, query = "SELECT c FROM Cidade c WHERE c.codigo = :codigo"),
        @NamedQuery(name = CidadeRepository.selectByNome, query = "SELECT c FROM Cidade c WHERE c.nome like :nome"),
        @NamedQuery(name = CidadeRepository.selectByEstado, query = "SELECT c FROM Cidade c LEFT OUTER JOIN c.estado e WHERE e.id = :id")})
public class Cidade implements EntityObject<Cidade, Integer> {

    private static final long serialVersionUID = 289250263050622754L;

    @Id
    @GeneratedValue(generator = "cidade_id_seq")
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 7, max = 7)
    @Column(name = "codigo")
    private String codigo;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "nome")
    private String nome;

    @NotNull
    @JoinColumn(name = "estado_id", referencedColumnName = "id")
    @ManyToOne(cascade = CascadeType.DETACH, optional = false)
    private Estado estado;

    protected Cidade() {
        super();
    }

    public Cidade(String codigo, String nome, Estado estado) {
        super();
        assert (codigo != null);
        assert (nome != null);
        assert (estado != null);

        this.codigo = codigo;
        this.nome = nome;
        this.estado = estado;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.id);
    }

    @Override
    public boolean equals(Object object) {
        if (object == null)
            return false;

        if (this == object)
            return true;

        if (!(object instanceof Cidade))
            return false;

        Cidade other = (Cidade) object;
        return sameId(other);
    }

    @Override
    public boolean sameId(Cidade other) {
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(getClass().getSimpleName());
        sb.append('{');
        sb.append("codigo='").append(codigo).append('\'');
        sb.append(", nome='").append(nome).append('\'');
        sb.append(", estado=").append(estado.getSigla());
        sb.append('}');
        return sb.toString();
    }
}