package br.com.finch.xlanches.web.interfaces.usuario.facade.usuario;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author Filipe Bojikian Rissi
 */
public class ContatoDTO implements Serializable {

    private static final long serialVersionUID = 683334825075685458L;

    @NotNull
    @Size(min = 8, max = 20)
    private String name;

    @NotNull
    @Size(min = 8, max = 100)
    @Pattern(regexp = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message = "{xlanches.validator.constraints.Email.message}")
    private String email;

    @NotNull
    @Size(min = 10, max = 1000)
    private String message;

    private String captcha;

    public ContatoDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

}
