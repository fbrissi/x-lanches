package br.com.finch.xlanches.web.domain.service.exception;

import br.com.finch.xlanches.web.domain.model.shared.ResourceBundleException;

/**
 * @author by Filipe Bojikian Rissi
 */
public class InvalidException extends ResourceBundleException {

    private static final long serialVersionUID = 643008560163648010L;

}
