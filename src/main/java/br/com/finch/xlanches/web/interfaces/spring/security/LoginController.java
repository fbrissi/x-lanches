package br.com.finch.xlanches.web.interfaces.spring.security;

import br.com.finch.xlanches.web.application.UsuarioService;
import br.com.finch.xlanches.web.domain.model.exception.EmailException;
import br.com.finch.xlanches.web.domain.service.exception.EnvioEmailException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.inject.Inject;

/**
 * @author Filipe Bojikian Rissi
 */
@Controller
@RequestMapping(value = "/login")
public class LoginController {

    @Inject
    private UsuarioService usuarioService;

    @RequestMapping(method = RequestMethod.GET)
    public String init() {
        return "security/login";
    }

    @RequestMapping(value = "/recuperar-senha", method = RequestMethod.PUT)
    public String recuperarSenha(@RequestParam(value = "email") String email, Model model) {
        try {
            usuarioService.redefinirSenha(email);
        } catch (EmailException e) {
            model.addAttribute("severe", true);
            return "security/login :: form";
        } catch (EnvioEmailException e) {
            model.addAttribute("severe", true);
            return "security/login :: form";
        }

        return "security/login :: form";
    }

}