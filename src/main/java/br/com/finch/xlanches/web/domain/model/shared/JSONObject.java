package br.com.finch.xlanches.web.domain.model.shared;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import java.io.Serializable;
import java.io.StringReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Filipe Bojikian Rissi
 */
public interface JSONObject extends Serializable {

    Class<?> PKG = JSONObject.class;
    Logger logger = LoggerFactory.getLogger(PKG);

    default JsonObject toJSON() {
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();

        Class clazz = this.getClass();
        try {
            do {
                for (Field field : clazz.getDeclaredFields()) {
                    if (Modifier.isFinal(field.getModifiers()))
                        continue;

                    AccessController.doPrivileged((PrivilegedAction<Object>) () -> {
                        field.setAccessible(true);
                        return null;
                    });

                    if (field.get(this) == null) {
                        jsonObjectBuilder.addNull(field.getName());
                    } else if (EnumDescription.class.isAssignableFrom(field.getType())) {
                        JsonObjectBuilder enumBuilder = Json.createObjectBuilder();
                        EnumDescription enumDescription = (EnumDescription) field.get(this);
                        enumBuilder.add("name", enumDescription.name());
                        enumBuilder.add("descricao", enumDescription.getDescricao());
                        enumBuilder.add("valor", enumDescription.getValor());
                        jsonObjectBuilder.add(field.getName(), enumBuilder);
                    } else if (Enum.class.isAssignableFrom(field.getType())) {
                        jsonObjectBuilder.add(field.getName(), ((Enum) field.get(this)).name());
                    } else if (BigDecimal.class.isAssignableFrom(field.getType())) {
                        jsonObjectBuilder.add(field.getName(), (BigDecimal) field.get(this));
                    } else if (BigInteger.class.isAssignableFrom(field.getType())) {
                        jsonObjectBuilder.add(field.getName(), (BigInteger) field.get(this));
                    } else if (Boolean.class.isAssignableFrom(field.getType())) {
                        jsonObjectBuilder.add(field.getName(), (Boolean) field.get(this));
                    } else if (Double.class.isAssignableFrom(field.getType())) {
                        jsonObjectBuilder.add(field.getName(), (Double) field.get(this));
                    } else if (Integer.class.isAssignableFrom(field.getType())) {
                        jsonObjectBuilder.add(field.getName(), (Integer) field.get(this));
                    } else if (Long.class.isAssignableFrom(field.getType())) {
                        jsonObjectBuilder.add(field.getName(), (Long) field.get(this));
                    } else if (LocalDate.class.isAssignableFrom(field.getType())) {
                        jsonObjectBuilder.add(field.getName(), ((LocalDate) field.get(this)).format(DateTimeFormatter.ISO_LOCAL_DATE));
                    } else if (LocalDateTime.class.isAssignableFrom(field.getType())) {
                        jsonObjectBuilder.add(field.getName(), ((LocalDateTime) field.get(this)).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
                    } else if (String.class.isAssignableFrom(field.getType())) {
                        jsonObjectBuilder.add(field.getName(), (String) field.get(this));
                    } else if (JSONObject.class.isAssignableFrom(field.getType())) {
                        jsonObjectBuilder.add(field.getName(), ((JSONObject) field.get(this)).toJSON());
                    }
                }
                clazz = clazz.getSuperclass();
            } while (clazz != null);
        } catch (Throwable e) {
            logger.error("Erro na conversão do objeto para JSON.", e);
        }

        return jsonObjectBuilder.build();
    }

    static <T extends JSONObject> T fromJSON(final Class<T> entityClass, final String json) {
        try (JsonReader reader = Json.createReader(new StringReader(json))) {
            JsonObject jsonObject = reader.readObject();

            if (jsonObject == null) {
                return null;
            }

            try {
                Constructor<T> constructor = entityClass.getDeclaredConstructor();
                AccessController.doPrivileged((PrivilegedAction<Object>) () -> {
                    constructor.setAccessible(true);
                    return null;
                });
                T entity = constructor.newInstance();
                Class clazz = entity.getClass();
                do {
                    for (Field field : clazz.getDeclaredFields()) {
                        if (Modifier.isFinal(field.getModifiers()))
                            continue;

                        AccessController.doPrivileged((PrivilegedAction<Object>) () -> {
                            field.setAccessible(true);
                            return null;
                        });

                        if (!jsonObject.containsKey(field.getName()) || jsonObject.isNull(field.getName())) {
                            field.set(entity, null);
                        }else if (EnumDescription.class.isAssignableFrom(field.getType())) {
                            JsonObject jsonObjectEnum = jsonObject.getJsonObject(field.getName());
                            field.set(entity, Enum.valueOf((Class) field.getType(), jsonObjectEnum.getString("name")));
                        } else if (Enum.class.isAssignableFrom(field.getType())) {
                            field.set(entity, Enum.valueOf((Class) field.getType(), jsonObject.getString(field.getName())));
                        } else if (BigDecimal.class.isAssignableFrom(field.getType())) {
                            field.set(entity, jsonObject.getJsonNumber(field.getName()).bigDecimalValue());
                        } else if (BigInteger.class.isAssignableFrom(field.getType())) {
                            field.set(entity, jsonObject.getJsonNumber(field.getName()).bigIntegerValue());
                        } else if (Boolean.class.isAssignableFrom(field.getType())) {
                            field.set(entity, jsonObject.getBoolean(field.getName()));
                        } else if (Double.class.isAssignableFrom(field.getType())) {
                            field.set(entity, jsonObject.getJsonNumber(field.getName()).doubleValue());
                        } else if (Integer.class.isAssignableFrom(field.getType())) {
                            field.set(entity, jsonObject.getJsonNumber(field.getName()).intValue());
                        } else if (Long.class.isAssignableFrom(field.getType())) {
                            field.set(entity, jsonObject.getJsonNumber(field.getName()).longValue());
                        } else if (LocalDate.class.isAssignableFrom(field.getType())) {
                            field.set(entity, LocalDate.parse(jsonObject.getString(field.getName()), DateTimeFormatter.ISO_LOCAL_DATE));
                        } else if (LocalDateTime.class.isAssignableFrom(field.getType())) {
                            field.set(entity, LocalDateTime.parse(jsonObject.getString(field.getName()), DateTimeFormatter.ISO_LOCAL_DATE_TIME));
                        } else if (String.class.isAssignableFrom(field.getType())) {
                            field.set(entity, jsonObject.getString(field.getName()));
                        } else if (JSONObject.class.isAssignableFrom(field.getType())) {
                            field.set(entity, JSONObject.fromJSON((Class) field.getType(), jsonObject.getJsonObject(field.getName()).toString()));
                        }
                    }
                    clazz = clazz.getSuperclass();
                } while (clazz != null);

                return entity;
            } catch (Throwable e) {
                logger.error("Erro na conversão do JSON para objeto.", e);
            }

            return null;
        }

    }

}