package br.com.finch.xlanches.web.domain.model.vendas;

import br.com.finch.xlanches.web.domain.model.shared.ValuesObject;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * @author Filipe Bojikian Rissi
 */
@Embeddable
public class IngredienteItem implements ValuesObject<IngredienteItem> {

    private static final long serialVersionUID = -6135996661246144680L;

    @Basic(optional = false)
    @NotNull
    @Column(name = "quantidade")
    private final BigDecimal quantidade;

    @NotNull
    @JoinColumn(name = "ingrediente_id", referencedColumnName = "id")
    @ManyToOne(cascade = CascadeType.DETACH, optional = false)
    private final Ingrediente ingrediente;

    protected IngredienteItem() {
        super();
        this.quantidade = null;
        this.ingrediente = null;
    }

    protected IngredienteItem(BigDecimal quantidade, Ingrediente ingrediente) {
        super();
        assert (quantidade != null);
        assert (ingrediente != null);

        this.quantidade = quantidade;
        this.ingrediente = ingrediente;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public Ingrediente getIngrediente() {
        return ingrediente;
    }

    public BigDecimal getValor() {
        return quantidade.multiply(ingrediente.getValor());
    }

    @Override
    public boolean equals(Object object) {
        if (object == null)
            return false;

        if (this == object)
            return true;

        if (!(object instanceof IngredienteItem))
            return false;

        IngredienteItem other = (IngredienteItem) object;
        return sameValue(other);
    }

    @Override
    public int hashCode() {
        return Objects.hash(quantidade, ingrediente);
    }

    @Override
    public boolean sameValue(IngredienteItem other) {
        return new EqualsBuilder()
                .append(this.quantidade, other.quantidade)
                .append(this.ingrediente, other.ingrediente)
                .isEquals();
    }

    @Override
    public int compareTo(IngredienteItem other) {
        return new CompareToBuilder()
                .append(this.quantidade, other.quantidade)
                .append(this.ingrediente, other.ingrediente)
                .toComparison();
    }

}