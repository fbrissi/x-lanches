package br.com.finch.xlanches.web.infrastructure.persistence.jpa.repository;

import br.com.finch.xlanches.web.domain.model.vendas.Promocao;
import br.com.finch.xlanches.web.domain.model.vendas.PromocaoRepository;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

/**
 * @author Filipe Bojikian Rissi
 */
@Named
@RequestScoped
public class PromocaoRepositoryImpl extends AbstractRepository<Promocao, Integer> implements PromocaoRepository {

    protected PromocaoRepositoryImpl() {
    }

    @Inject
    public PromocaoRepositoryImpl(EntityManager entityManager) {
        super(entityManager);
    }

}