package br.com.finch.xlanches.web.infrastructure.util;

import br.com.finch.xlanches.web.domain.model.shared.JSONObject;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import java.util.List;

/**
 * @author Filipe Bojikian Rissi
 */
public class JSONCreate {

    private JSONCreate() {
    }

    public static JsonObject toJSONList(List<? extends JSONObject> list, long count) {
        JsonObjectBuilder dataBuilder = Json.createObjectBuilder();
        dataBuilder.add("iTotalRecords", count);
        dataBuilder.add("iTotalDisplayRecords", count);

        JsonArrayBuilder listBuilder = Json.createArrayBuilder();

        for (JSONObject jsonObject : list) {
            listBuilder.add(jsonObject.toJSON());
        }

        dataBuilder.add("aaData", listBuilder);
        return dataBuilder.build();
    }

}
