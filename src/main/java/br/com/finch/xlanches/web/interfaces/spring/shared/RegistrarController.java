package br.com.finch.xlanches.web.interfaces.spring.shared;

import br.com.finch.xlanches.web.application.UsuarioService;
import br.com.finch.xlanches.web.domain.model.exception.EmailException;
import br.com.finch.xlanches.web.domain.model.exception.SenhaInvalidaException;
import br.com.finch.xlanches.web.domain.service.EmailService;
import br.com.finch.xlanches.web.domain.service.ReCaptchaService;
import br.com.finch.xlanches.web.domain.service.exception.EnvioEmailException;
import br.com.finch.xlanches.web.interfaces.usuario.facade.usuario.UsuarioDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * @author Filipe Bojikian Rissi
 */
@Controller
@RequestMapping(value = "/registrar")
public class RegistrarController {

    @Inject
    private UsuarioService usuarioService;

    @Inject
    private ReCaptchaService reCaptchaService;

    @Inject
    private EmailService emailService;

    @RequestMapping(method = RequestMethod.GET)
    public String init(Model model) {
        model.addAttribute("usuario", new UsuarioDTO());
        return "usuario/registrar";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String registrar(HttpServletRequest request, @ModelAttribute("usuario") @Valid UsuarioDTO usuario, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "usuario/registrar";
        }

        try {
            if (!reCaptchaService.verify(request.getParameter(ReCaptchaService.RECAPTCHA_RESPONSE), request.getRemoteAddr()).isSuccess()) {
                return "usuario/registrar";
            }
        } catch (MessagingException e) {
            model.addAttribute("severe", true);
            return "usuario/registrar";
        }

        try {
            usuarioService.criarUsuario(usuario.getEmail(), usuario.getNome(), usuario.getTelefone(), usuario.getSenha(), usuario.getConfirmarSenha());
        } catch (SenhaInvalidaException e) {
            result.addError(new FieldError("usuario", "senha", e.getMessage()));
            return "usuario/registrar";
        } catch (EmailException e) {
            result.addError(new FieldError("usuario", "email", e.getMessage()));
            return "usuario/registrar";
        } catch (EnvioEmailException e) {
            model.addAttribute("severe", true);
            return "usuario/registrar";
        }

        return "usuario/registrado";
    }

}