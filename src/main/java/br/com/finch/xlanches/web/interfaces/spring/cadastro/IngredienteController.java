package br.com.finch.xlanches.web.interfaces.spring.cadastro;

import br.com.finch.xlanches.web.application.IngredienteService;
import br.com.finch.xlanches.web.interfaces.spring.error.EntityNotFoundException;
import br.com.finch.xlanches.web.interfaces.usuario.facade.cadastro.IngredienteDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.validation.Valid;

/**
 * @author Filipe Bojikian Rissi
 */
@Controller
@RequestMapping(value = "/ingrediente")
public class IngredienteController {

    @Inject
    private IngredienteService ingredienteService;

    @RequestMapping(method = RequestMethod.GET)
    public String init() {
        return "cadastro/ingrediente/listar";
    }

    @ResponseBody
    @RequestMapping(value = "/listar", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String listarTodos() {
        JsonObject jsonObject = ingredienteService.listarTodos();
        return jsonObject.toString();
    }

    @ResponseBody
    @RequestMapping(value = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String listar(int start, int length) {
        JsonObject jsonObject = ingredienteService.listar(start, length);
        return jsonObject.toString();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String editar(@PathVariable("id") Integer id, Model model) {
        IngredienteDTO ingrediente = ingredienteService.editar(id);
        if (ingrediente == null) {
            throw new EntityNotFoundException();
        }
        model.addAttribute("ingrediente", ingrediente);
        return "cadastro/ingrediente/formulario";
    }

    @RequestMapping(value = "/cadastrar", method = RequestMethod.GET)
    public String cadastrar(Model model) {
        model.addAttribute("ingrediente", new IngredienteDTO());
        return "cadastro/ingrediente/formulario";
    }

    @RequestMapping(value = "/cadastrar", method = RequestMethod.PUT)
    public String alterar(@ModelAttribute("ingrediente") @Valid IngredienteDTO ingrediente, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "cadastro/ingrediente/formulario :: form";
        }
        ingredienteService.alterar(ingrediente);
        model.addAttribute("redirect", "/ingrediente");
        return "redirect :: url";
    }

    @RequestMapping(value = "/cadastrar", method = RequestMethod.POST)
    public String persistir(@ModelAttribute("ingrediente") @Valid IngredienteDTO ingrediente, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "cadastro/ingrediente/formulario :: form";
        }
        ingredienteService.persistir(ingrediente);
        model.addAttribute("redirect", "/ingrediente");
        return "redirect :: url";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deletar(@PathVariable("id") Integer id) {
        ingredienteService.deletar(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}