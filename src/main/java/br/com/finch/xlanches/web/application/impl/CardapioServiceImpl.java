package br.com.finch.xlanches.web.application.impl;

import br.com.finch.xlanches.web.application.CardapioService;
import br.com.finch.xlanches.web.domain.model.vendas.Cardapio;
import br.com.finch.xlanches.web.domain.model.vendas.CardapioRepository;
import br.com.finch.xlanches.web.domain.model.vendas.PromocaoRepository;
import br.com.finch.xlanches.web.infrastructure.util.JSONCreate;
import br.com.finch.xlanches.web.interfaces.usuario.facade.cadastro.CardapioDTO;
import org.apache.deltaspike.jpa.api.transaction.Transactional;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.json.JsonObject;
import java.util.List;

/**
 * @author by Filipe Bojikian Rissi
 */
@Named
@RequestScoped
public class CardapioServiceImpl implements CardapioService {


    private final CardapioRepository cardapioRepository;
    private final PromocaoRepository promocaoRepository;

    protected CardapioServiceImpl() {
        cardapioRepository = null;
        promocaoRepository = null;
    }

    @Inject
    public CardapioServiceImpl(CardapioRepository cardapioRepository, PromocaoRepository promocaoRepository) {
        this.cardapioRepository = cardapioRepository;
        this.promocaoRepository = promocaoRepository;
    }

    @Override
    public JsonObject listar(int page, int result) {
        List<Cardapio> list = cardapioRepository.list(page, result);
        return JSONCreate.toJSONList(list, cardapioRepository.count());
    }

    @Override
    public CardapioDTO editar(Integer id) {
        Cardapio cardapio = cardapioRepository.find(id);
        return cardapio == null ? null : new CardapioDTO(cardapio);
    }

    @Override
    @Transactional
    public void persistir(CardapioDTO cardapioDTO) {
        Cardapio cardapio = new Cardapio(cardapioDTO.getNome(),
                cardapioDTO.getObservacao(), promocaoRepository.find(cardapioDTO.getPromocaoId()));
        cardapioRepository.persist(cardapio);
    }

    @Override
    @Transactional
    public void alterar(CardapioDTO cardapioDTO) {
        Cardapio cardapio = cardapioRepository.find(cardapioDTO.getId());
        cardapio.setNome(cardapioDTO.getNome());
        cardapio.setObservacao(cardapioDTO.getObservacao());
        cardapio.setPromocao(promocaoRepository.find(cardapioDTO.getPromocaoId()));
    }

    @Override
    @Transactional
    public void deletar(Integer id) {
        Cardapio cardapio = cardapioRepository.find(id);
        cardapioRepository.remove(cardapio);
    }

}
