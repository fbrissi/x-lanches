package br.com.finch.xlanches.web.domain.service;

/**
 * @author Filipe Bojikian Rissi
 */
public interface EmailService {

    EmailProxy getProxy();

    void addAttached(Attached attached);

    void addReplyTo(String replyTo);

    void addTo(String to);

    void addBcc(String bcc);

    void addCc(String cc);

    void clearAttached();

    void clearReplyTo();

    void clearTo();

    void clearBcc();

    void clearCc();
}