package br.com.finch.xlanches.web.domain.model.exception;

import br.com.finch.xlanches.web.domain.model.shared.ResourceBundleException;

/**
 * @author Filipe Bojikian Rissi
 */
public class EmailException extends ResourceBundleException {

    private static final long serialVersionUID = 3793198274789600410L;

    public EmailException() {
        super("{xlanches.exception.Email.message}");
    }

}