package br.com.finch.xlanches.web.infrastructure.thymeleaf.conversion;

import br.com.finch.xlanches.web.domain.model.identificacao.CEP;
import org.springframework.format.Formatter;

import java.util.Locale;

/**
 * @author Filipe Bojikian Rissi
 */
public class CEPFormatter implements Formatter<CEP> {

    @Override
    public CEP parse(final String text, final Locale locale) {
        return new CEP(text);
    }

    @Override
    public String print(final CEP object, final Locale locale) {
        return object.toString(true);
    }

}