package br.com.finch.xlanches.web.infrastructure.service;

import br.com.finch.xlanches.web.domain.service.ReCaptchaService;
import br.com.finch.xlanches.web.infrastructure.util.Constants;
import org.apache.http.Consts;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.mail.MessagingException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Filipe Bojikian Rissi
 */
@Named
@RequestScoped
public class ReCaptchaServiceImpl implements ReCaptchaService {

    private static final Logger logger = LoggerFactory.getLogger(ReCaptchaServiceImpl.class);

    private static final String SECRET_KEY = "6LfdjxcTAAAAAOdkntnl9z7IULiGMY8tttDdUl6i";
    private static final String URL_VERIFY = "https://www.google.com/recaptcha/api/siteverify";
    private static final String USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.130 Safari/537.36";

    @Override
    public CaptchaResponse verify(final String response, final String remoteIP) throws MessagingException {
        if (!Constants.XLANCHES_RECAPTCHA) {
            return new CaptchaResponse("{\"success\": true, \"challenge_ts\": \"2014-09-23T23:03:11+0000\", \"hostname\": \"localhost\"}");
        }

        try (final CloseableHttpClient httpClient = HttpClients.createDefault()) {
            final HttpPost post = new HttpPost(URL_VERIFY);
            final List<NameValuePair> nameValuePairs = new ArrayList<>();

            nameValuePairs.add(new BasicNameValuePair("secret", SECRET_KEY));
            nameValuePairs.add(new BasicNameValuePair("response", response));
            nameValuePairs.add(new BasicNameValuePair("remoteip", remoteIP));

            post.setEntity(new UrlEncodedFormEntity(nameValuePairs, Consts.UTF_8));

            post.addHeader("User-Agent", USER_AGENT);
            post.addHeader("Accept-Language", "en-US,en;q=0.5");

            try (final CloseableHttpResponse cliResponse = httpClient.execute(post);
                 final BufferedReader reader = new BufferedReader(new InputStreamReader(cliResponse.getEntity().getContent(), StandardCharsets.UTF_8))) {
                final StringBuilder clientResponse = new StringBuilder();

                logger.debug("Requisição POST ao site: {}", SECRET_KEY);

                String line;
                while ((line = reader.readLine()) != null) {
                    clientResponse.append(line);
                }

                return new CaptchaResponse(clientResponse.toString());
            }
        } catch (IOException e) {
            logger.error("Não foi possível abrir uma conexão.", e);
            throw new MessagingException(e.getMessage());
        }
    }

}