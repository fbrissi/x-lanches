package br.com.finch.xlanches.web.infrastructure.util.context;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.json.*;
import javax.servlet.http.HttpServletResponse;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;

/**
 * @author Filipe Bojikian Rissi
 */
public class RequestContext {

    private static final String CALLBACK_PARAM = "X-CallbackParam";

    private RequestContext() {
    }

    public static void addCallbackParam(String name, Object value) {
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getResponse();
        String callbackParam = response.getHeader(CALLBACK_PARAM);
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
        if (callbackParam != null) {
            try (JsonReader reader = Json.createReader(new StringReader(callbackParam))) {
                JsonObject jsonObject = reader.readObject();
                for (Map.Entry<String, JsonValue> entry : jsonObject.entrySet()) {
                    String jsonName = entry.getKey();
                    JsonValue jsonValue = entry.getValue();
                    jsonObjectBuilder.add(jsonName, jsonValue);
                }
            }
        }

        if (value == null)
            jsonObjectBuilder.addNull(name);
        else if (value instanceof BigDecimal)
            jsonObjectBuilder.add(name, (BigDecimal) value);
        else if (value instanceof BigInteger)
            jsonObjectBuilder.add(name, (BigInteger) value);
        else if (value instanceof Boolean)
            jsonObjectBuilder.add(name, (boolean) value);
        else if (value instanceof Double)
            jsonObjectBuilder.add(name, (double) value);
        else if (value instanceof Integer)
            jsonObjectBuilder.add(name, (int) value);
        else if (value instanceof Long)
            jsonObjectBuilder.add(name, (long) value);
        else if (value instanceof String)
            jsonObjectBuilder.add(name, (String) value);
        else
            jsonObjectBuilder.add(name, value.toString());
    }

}