package br.com.finch.xlanches.web.domain.service.exception;

import br.com.finch.xlanches.web.domain.model.shared.ResourceBundleException;

/**
 * @author by Filipe Bojikian Rissi
 */
public class ExpiredException extends ResourceBundleException {

    private static final long serialVersionUID = -5661566515865376596L;

}
