package br.com.finch.xlanches.web.domain.model.usuario;

import br.com.finch.xlanches.web.domain.model.contato.Telefone;
import br.com.finch.xlanches.web.domain.model.exception.SenhaInvalidaException;

/**
 * @author Filipe Bojikian Rissi
 */
public class UsuarioFactory {

    private UsuarioFactory() {
    }

    public static Usuario criarUsuario(String email, String nome, Telefone telefone, String senha, String confirmarSenha, PapelRepository papelRepository) throws SenhaInvalidaException {
        Usuario usuario = new Usuario(email, nome, telefone, papelRepository.findByTipo(Papel.Tipo.USER));
        usuario.atribuirSenha(senha, confirmarSenha);
        return usuario;
    }

    public static Usuario criarAdministrador(String email, String nome, Telefone telefone, String senha, String confirmarSenha, PapelRepository papelRepository) throws SenhaInvalidaException {
        Usuario usuario = new Usuario(email, nome, telefone, papelRepository.findByTipo(Papel.Tipo.ADMIN));
        usuario.atribuirSenha(senha, confirmarSenha);
        return usuario;
    }

}