package br.com.finch.xlanches.web.infrastructure.validation.logic;

import br.com.finch.xlanches.web.infrastructure.validation.annotation.PassWordConfirm;
import org.apache.commons.beanutils.BeanUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.InvocationTargetException;

/**
 * @author Filipe Bojikian Rissi
 */
public class PassWordConfirmValidatorLogic implements ConstraintValidator<PassWordConfirm, Object> {

    private String passwordFieldName;
    private String confirmFieldName;

    @Override
    public void initialize(final PassWordConfirm constraintAnnotation) {
        passwordFieldName = constraintAnnotation.password();
        confirmFieldName = constraintAnnotation.confirm();
    }

    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {
        try {
            final Object password = BeanUtils.getProperty(value, passwordFieldName);
            final Object confirmPassword = BeanUtils.getProperty(value, confirmFieldName);

            return password == null && confirmPassword == null || password != null && password.equals(confirmPassword);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

}