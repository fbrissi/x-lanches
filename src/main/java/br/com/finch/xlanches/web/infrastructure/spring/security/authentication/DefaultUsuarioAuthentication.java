package br.com.finch.xlanches.web.infrastructure.spring.security.authentication;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * @author Filipe Bojikian Rissi
 */
public class DefaultUsuarioAuthentication extends UsernamePasswordAuthenticationToken implements UsuarioAuthentication {

    private static final long serialVersionUID = -8613976519898305120L;

    public DefaultUsuarioAuthentication(User user, String credentials, Collection<? extends GrantedAuthority> authorities) {
        super(user, credentials, authorities);
    }

    @Override
    public User getUser() {
        return (User) getPrincipal();
    }

}