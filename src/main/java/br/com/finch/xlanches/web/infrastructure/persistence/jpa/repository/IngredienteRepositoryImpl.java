package br.com.finch.xlanches.web.infrastructure.persistence.jpa.repository;

import br.com.finch.xlanches.web.domain.model.vendas.Ingrediente;
import br.com.finch.xlanches.web.domain.model.vendas.IngredienteRepository;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

/**
 * @author Filipe Bojikian Rissi
 */
@Named
@RequestScoped
public class IngredienteRepositoryImpl extends AbstractRepository<Ingrediente, Integer> implements IngredienteRepository {

    protected IngredienteRepositoryImpl() {
    }

    @Inject
    public IngredienteRepositoryImpl(EntityManager entityManager) {
        super(entityManager);
    }

}