package br.com.finch.xlanches.web.domain.service.exception;

import br.com.finch.xlanches.web.domain.model.shared.ResourceBundleException;

/**
 * @author by Filipe Bojikian Rissi
 */
public class EnvioEmailException extends ResourceBundleException {

    private static final long serialVersionUID = -6559616865919912988L;

}
