package br.com.finch.xlanches.web.infrastructure.util;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.UnsatisfiedResolutionException;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import java.lang.annotation.Annotation;
import java.util.Set;

/**
 * @author Filipe Bojikian Rissi
 */
public final class CDIBeanCreate {

    private CDIBeanCreate() {
    }

    public static <T> T createBean(Class<? extends T> type, Annotation... qualifiers) {
        BeanManager beanManager = CDI.current().getBeanManager();
        Set<Bean<?>> beans = beanManager.getBeans(type, qualifiers);
        Bean<?> bean = beanManager.resolve(beans);

        if (bean == null) {
            throw new UnsatisfiedResolutionException();
        }

        CreationalContext<?> ctx = beanManager.createCreationalContext(bean);
        T instance = (T) beanManager.getReference(bean, type, ctx);
        return instance;
    }

}