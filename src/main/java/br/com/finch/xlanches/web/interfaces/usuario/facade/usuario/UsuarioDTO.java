package br.com.finch.xlanches.web.interfaces.usuario.facade.usuario;

import br.com.finch.xlanches.web.domain.model.contato.Telefone;
import br.com.finch.xlanches.web.domain.model.usuario.Usuario;
import br.com.finch.xlanches.web.infrastructure.validation.annotation.PassWordConfirm;
import br.com.finch.xlanches.web.infrastructure.validation.annotation.TelefoneValid;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author Filipe Bojikian Rissi
 */
@PassWordConfirm(password = "senha", confirm = "confirmarSenha")
public class UsuarioDTO implements Serializable {

    private static final long serialVersionUID = -7176101861255687407L;

    private Integer id;

    @NotNull
    @Pattern(regexp = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message = "message = \"{xlanches.validator.constraints.Email.message}")
    @Size(min = 8, max = 180)
    private String email;

    @NotNull
    @Size(max = 32)
    private String nome;

    private LocalDateTime registrado;

    @TelefoneValid
    private Telefone telefone;

    @Size(min = 4, max = 64)
    private String senha;

    @Size(min = 4, max = 64)
    private String confirmarSenha;

    public UsuarioDTO() {
    }

    public UsuarioDTO(Usuario usuario) {
        if (usuario != null) {
            this.id = usuario.getId();
            this.email = usuario.getEmail();
            this.nome = usuario.getNome();
            this.registrado = usuario.getRegistrado();
            this.telefone = usuario.getTelefone();
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDateTime getRegistrado() {
        return registrado;
    }

    public void setRegistrado(LocalDateTime registrado) {
        this.registrado = registrado;
    }

    public Telefone getTelefone() {
        return telefone;
    }

    public void setTelefone(Telefone telefone) {
        this.telefone = telefone;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getConfirmarSenha() {
        return confirmarSenha;
    }

    public void setConfirmarSenha(String confirmarSenha) {
        this.confirmarSenha = confirmarSenha;
    }

}
