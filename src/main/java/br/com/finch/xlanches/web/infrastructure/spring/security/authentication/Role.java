package br.com.finch.xlanches.web.infrastructure.spring.security.authentication;

import br.com.finch.xlanches.web.domain.model.usuario.Usuario;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;

/**
 * @author Filipe Bojikian Rissi
 */
public class Role implements Serializable, GrantedAuthority {

    private static final long serialVersionUID = 2273970389756890379L;

    private static final String ROLE = "ROLE_";

    public static final String IP = "IP";
    public static final String ADMIN = "ADMIN";
    public static final String USER = "USER";

    private User user;

    public Role(User user) {
        this.user = user;
    }

    public Usuario getUsuario() {
        return user != null ? user.getUsuario() : null;
    }

    @Override
    public String getAuthority() {
        if (user.getUsuario() == null)
            return Role.ROLE + Role.IP;

        if (user.getUsuario().isAdministrador())
            return Role.ROLE + Role.ADMIN;

        return Role.ROLE + Role.USER;
    }

}