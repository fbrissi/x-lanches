package br.com.finch.xlanches.web.infrastructure.spring.security.authentication;

import br.com.finch.xlanches.web.domain.model.usuario.Usuario;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Filipe Bojikian Rissi
 */
@Named
public class AuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        if (authentication instanceof UsuarioAuthentication) {
            UsuarioAuthentication usuarioAuthentication = (UsuarioAuthentication) authentication;
            Usuario usuario = usuarioAuthentication.getUser().getPrincipal();

            if (usuario.isRedefinirSenha())
                setDefaultTargetUrl("/redefinir_senha");
            else if (usuario.isAdministrador())
                setDefaultTargetUrl("/administracao");
            else
                setDefaultTargetUrl("/usuario");
        }
        super.onAuthenticationSuccess(request, response, authentication);
    }

}
