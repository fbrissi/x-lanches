package br.com.finch.xlanches.web.infrastructure.persistence.jpa.repository;

import br.com.finch.xlanches.web.domain.model.shared.EntityObject;
import br.com.finch.xlanches.web.domain.model.shared.EntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.SingularAttribute;
import javax.validation.ConstraintViolationException;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.ParameterizedType;
import java.util.*;
import java.util.function.Consumer;

/**
 * @author Filipe Bojikian Rissi
 */
abstract class AbstractRepository<T extends EntityObject, ID extends Comparable> implements EntityRepository<T, ID> {

    private static final Class<?> PKG = AbstractRepository.class;
    private final Logger logger = LoggerFactory.getLogger(PKG);

    protected class Parameter {
        private final int position;
        private final String name;
        private final Object value;

        public Parameter(int position, Object value) {
            this.position = position;
            this.value = value;
            this.name = null;
        }

        public Parameter(String name, Object value) {
            this.name = name;
            this.value = value;
            this.position = -1;
        }

        public int getPosition() {
            return position;
        }

        public String getName() {
            return name;
        }

        public Object getValue() {
            return value;
        }

    }

    protected final EntityManager entityManager;
    private final Class<T> entityClass;
    private final Class<ID> idClass;

    protected AbstractRepository() {
        entityManager = null;
        entityClass = null;
        idClass = null;
    }

    protected AbstractRepository(EntityManager entityManager, Class<? extends T> entityClass, Class<? extends ID> idClass) {
        this.entityManager = entityManager;
        this.entityClass = (Class<T>) entityClass;
        this.idClass = (Class<ID>) idClass;
    }

    protected AbstractRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
        this.entityClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        this.idClass = (Class<ID>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }

    @Override
    public void persist(T entity) {
        try {
            logger.debug("Persistir entidade: {}", entity);

            entityManager.persist(entity);

            logger.debug("Entidade: {}, persistida", entity);
        } catch (ConstraintViolationException e) {
            logger.error("Exception: ");
            e.getConstraintViolations().forEach(err -> logger.error(err.toString()));
        }
    }

    @Override
    public void remove(T entity) {
        try {
            logger.debug("Remover entidade: {}", entity);

            entityManager.remove(entity);

            logger.debug("Entidade: {}, removida", entity);
        } catch (ConstraintViolationException e) {
            logger.error("Exception: ");
            e.getConstraintViolations().forEach(err -> logger.error(err.toString()));
        }
    }

    @Override
    public T merge(T entity) {
        try {
            logger.debug("Mesclar entidade: {}", entity);

            T merge = entityManager.merge(entity);

            logger.debug("Entidade: {}, mesclada", entity);
            return merge;
        } catch (ConstraintViolationException e) {
            logger.error("Exception: ");
            e.getConstraintViolations().forEach(err -> logger.error(err.toString()));
        }
        return entity;
    }

    @Override
    public T find(ID id) {
        logger.debug("Encontrar entidade com id: {}", id);

        T find = entityManager.find(entityClass, id);

        logger.debug("Entidade com id: {}, {}encontrada", id, find == null ? "não " : "");
        return find;
    }

    @Override
    public T first() {
        return first((Map<SingularAttribute<T, ? extends Serializable>, Object>) Array.newInstance(SingularAttribute.class, 0));
    }

    protected T first(SingularAttribute<T, ? extends Serializable>... order) {
        return first(null, OrderBy.ASC, order);
    }

    protected T first(OrderBy orderBy, SingularAttribute<T, ? extends Serializable>... order) {
        return first(null, orderBy, order);
    }

    protected T first(Map<SingularAttribute<T, ? extends Serializable>, Object> param) {
        return first(param, OrderBy.ASC);
    }

    protected T first(Map<SingularAttribute<T, ? extends Serializable>, Object> param, SingularAttribute<T, ? extends Serializable>... order) {
        return first(param, OrderBy.ASC, order);
    }

    protected T first(Map<SingularAttribute<T, ? extends Serializable>, Object> param, OrderBy orderBy, SingularAttribute<T, ? extends Serializable>... order) {
        logger.debug("Encontrar primeiro registro com parâmetros: {}, order by: {}, {}", param, order, orderBy);

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteria = builder.createQuery(entityClass);
        Root<T> root = criteria.from(entityClass);
        addParam(builder, criteria, root, param);
        if (order != null) {
            logger.debug("Criar order by para os campos: {}", order);

            List<Order> orders = new ArrayList();
            if (order.length == 0) {
                order = new SingularAttribute[]{entityManager.getMetamodel().entity(entityClass).getDeclaredId(idClass)};
            }
            for (SingularAttribute<T, ? extends Serializable> string : order) {
                switch (orderBy) {
                    case ASC:
                        orders.add(builder.asc(root.get(string)));
                        break;
                    case DESC:
                        orders.add(builder.desc(root.get(string)));
                        break;
                }
            }
            logger.debug("Adicionar order by: {}, a criteria", orders);

            criteria.orderBy(orders);
        }
        TypedQuery<T> tp = entityManager.createQuery(criteria);
        tp.setMaxResults(1);

        logger.debug("Trazer resultado");

        return tp.getSingleResult();
    }

    @Override
    public T last() {
        return first((Map<SingularAttribute<T, ? extends Serializable>, Object>) Array.newInstance(SingularAttribute.class, 0));
    }

    public T last(SingularAttribute<T, ? extends Serializable>... order) {
        return last(null, OrderBy.ASC, order);
    }

    protected T last(OrderBy orderBy, SingularAttribute<T, ? extends Serializable>... order) {
        return last(null, orderBy, order);
    }

    protected T last(Map<SingularAttribute<T, ? extends Serializable>, Object> param) {
        return last(param, OrderBy.ASC);
    }

    protected T last(Map<SingularAttribute<T, ? extends Serializable>, Object> param, SingularAttribute<T, ? extends Serializable>... order) {
        return last(param, OrderBy.ASC, order);
    }

    protected T last(Map<SingularAttribute<T, ? extends Serializable>, Object> param, OrderBy orderBy, SingularAttribute<T, ? extends Serializable>... order) {
        logger.debug("Encontrar último registro com parâmetros: {}, order by: {}, {}", param, order, orderBy);

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteria = builder.createQuery(entityClass);
        Root<T> root = criteria.from(entityClass);
        addParam(builder, criteria, root, param);
        if (order != null) {
            logger.debug("Criar order by para os campos: {}", order);

            List<Order> orders = new ArrayList();
            if (order.length == 0) {
                order = new SingularAttribute[]{entityManager.getMetamodel().entity(entityClass).getDeclaredId(idClass)};
            }
            for (SingularAttribute<T, ? extends Serializable> string : order) {
                switch (orderBy) {
                    case ASC:
                        orders.add(builder.desc(root.get(string)));
                        break;
                    case DESC:
                        orders.add(builder.asc(root.get(string)));
                        break;
                }
            }
            logger.debug("Adicionar order by: {}, a criteria", orders);

            criteria.orderBy(orders);
        }
        TypedQuery<T> tp = entityManager.createQuery(criteria);
        tp.setMaxResults(1);

        logger.debug("Trazer resultado");

        return tp.getSingleResult();
    }

    @Override
    public T next(ID id) {
        logger.debug("Encontrar próximo registro ao id: {}", id);

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteria = builder.createQuery(entityClass);
        Root<T> root = criteria.from(entityClass);
        criteria.where(builder.greaterThan(root.get(entityManager.getMetamodel().entity(entityClass).getDeclaredId(idClass)), id));
        criteria.orderBy(Arrays.asList(builder.asc(root.get(entityManager.getMetamodel().entity(entityClass).getDeclaredId(idClass)))));
        TypedQuery<T> tp = entityManager.createQuery(criteria);
        tp.setMaxResults(1);

        logger.debug("Trazer resultado");

        return tp.getSingleResult();
    }

    @Override
    public T previous(ID id) {
        logger.debug("Encontrar registro anterior ao id: {}", id);

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteria = builder.createQuery(entityClass);
        Root<T> root = criteria.from(entityClass);
        criteria.where(builder.lessThan(root.get(entityManager.getMetamodel().entity(entityClass).getDeclaredId(idClass)), id));
        criteria.orderBy(Arrays.asList(builder.asc(root.get(entityManager.getMetamodel().entity(entityClass).getDeclaredId(idClass)))));
        TypedQuery<T> tp = entityManager.createQuery(criteria);
        tp.setMaxResults(1);

        logger.debug("Trazer resultado");

        return tp.getSingleResult();
    }

    @Override
    public long count() {
        return count(null);
    }

    protected Number max(SingularAttribute<T, Number> property) {
        return max(null, property);
    }

    protected Number max(Map<SingularAttribute<T, ? extends Serializable>, Object> param, SingularAttribute<T, Number> property) {
        logger.debug("Trazer maior registro com parâmetros: {}", param);

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Number> criteria = builder.createQuery(Number.class);
        Root<T> root = criteria.from(entityClass);
        criteria.select(builder.max(root.<Number>get(property)));
        addParam(builder, criteria, root, param);
        logger.debug("Trazer resultado");

        return entityManager.createQuery(criteria).getSingleResult();
    }

    protected Number min(SingularAttribute<T, Number> property) {
        return min(null, property);
    }

    protected Number min(Map<SingularAttribute<T, ? extends Serializable>, Object> param, SingularAttribute<T, Number> property) {
        logger.debug("Trazer menor registro com parâmetros: {}", param);

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Number> criteria = builder.createQuery(Number.class);
        Root<T> root = criteria.from(entityClass);
        criteria.select(builder.min(root.<Number>get(property)));
        addParam(builder, criteria, root, param);

        logger.debug("Trazer resultado");

        return entityManager.createQuery(criteria).getSingleResult();
    }

    protected Number abs(Map<SingularAttribute<T, ? extends Serializable>, Object> param, SingularAttribute<T, Number> property) {
        logger.debug("Trazer valor absoluto com parâmetros: {}", param);

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Number> criteria = builder.createQuery(Number.class);
        Root<T> root = criteria.from(entityClass);
        criteria.select(builder.abs(root.<Number>get(property)));
        addParam(builder, criteria, root, param);

        logger.debug("Trazer resultado");

        return entityManager.createQuery(criteria).getSingleResult();
    }

    protected long count(Map<SingularAttribute<T, ? extends Serializable>, Object> param) {
        logger.debug("Trazer soma com parâmetros: {}", param);

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
        Root<T> root = criteria.from(entityClass);
        criteria.select(builder.count(root));
        addParam(builder, criteria, root, param);

        logger.debug("Trazer resultado");

        return entityManager.createQuery(criteria).getSingleResult();
    }

    @Override
    public List<T> listAll() {
        return listAll(new SingularAttribute[0]);
    }

    protected List<T> listAll(SingularAttribute<T, ? extends Serializable>... order) {
        return list(null, order);
    }

    @Override
    public List<T> list(Map<SingularAttribute<T, ? extends Serializable>, Object> param) {
        return list(param, 0, 0, new SingularAttribute[0]);
    }

    @Override
    public List<T> list(int page, int result) {
        return list(null, page, result, new SingularAttribute[0]);
    }

    protected List<T> list(int page, int result, SingularAttribute<T, ? extends Serializable>... order) {
        return list(null, page, result, order);
    }

    protected List<T> list(Map<SingularAttribute<T, ? extends Serializable>, Object> param, SingularAttribute<T, ? extends Serializable>... order) {
        return list(param, 0, 0, order);
    }

    protected List<T> list(Map<SingularAttribute<T, ? extends Serializable>, Object> param, int page, int result, SingularAttribute<T, ? extends Serializable>... order) {
        logger.debug("Trazer registros com parâmetros: {}", param);

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteria = builder.createQuery(entityClass);
        Root<T> root = criteria.from(entityClass);
        addParam(builder, criteria, root, param);
        if (order != null) {
            logger.debug("Criar order by para os campos: {}", order);

            List<Order> orders = new ArrayList();
            if (order.length == 0) {
                order = new SingularAttribute[]{entityManager.getMetamodel().entity(entityClass).getDeclaredId(idClass)};
            }
            for (SingularAttribute<T, ? extends Serializable> string : order) {
                orders.add(builder.asc(root.get(string)));
            }
            criteria.orderBy(orders);
        }
        TypedQuery<T> tp = entityManager.createQuery(criteria);
        if (page > 0 && result > 0) {
            logger.debug("Paginar resultados com primeiro registro: {}, máximo registro: {}", ((page - 1) * result), result);
            tp.setFirstResult(((page - 1) * result));
            tp.setMaxResults(result);
        }

        logger.debug("Trazer resultado");

        return tp.getResultList();
    }

    protected <E> TypedQuery<E> createNamedQuery(String namedQuery, Class entityClass, Parameter... param) {
        logger.debug("Criar query com NamedQuery: {}, com parâmetros: {}", namedQuery, param);

        TypedQuery<E> query = entityManager.createNamedQuery(namedQuery, entityClass);

        addParam(query, param);

        return query;
    }

    protected TypedQuery<T> createNamedQuery(String namedQuery, Parameter... param) {
        logger.debug("Criar query com NamedQuery: {}, com parâmetros: {}", namedQuery, param);

        TypedQuery<T> query = entityManager.createNamedQuery(namedQuery, entityClass);

        addParam(query, param);

        return query;
    }

    protected Query createNativeQuery(String namedQuery, Parameter... param) {
        logger.debug("Criar query com NativeQuery: {}, com parâmetros: {}", namedQuery, param);

        Query query = entityManager.createNativeQuery(namedQuery, entityClass);

        addParam(query, param);

        return query;
    }

    private void addParam(CriteriaBuilder builder, CriteriaQuery criteria, Root<T> root, Map<SingularAttribute<T, ? extends Serializable>, Object> param) {
        if (param != null) {
            logger.debug("Criar predicates com parâmetros: {}", param);

            List<Predicate> predicates = new ArrayList();
            for (Map.Entry<SingularAttribute<T, ? extends Serializable>, Object> entry : param.entrySet()) {
                SingularAttribute<T, ? extends Serializable> key = entry.getKey();
                Object value = entry.getValue();
                predicates.add(builder.equal(root.get(key), value));
            }
            criteria.where(predicates.toArray(new Predicate[0]));
        }
    }

    private void addParam(Query query, Parameter... param) {
        for (Parameter parameter : param) {
            int position = parameter.getPosition();
            String name = parameter.getName();
            Object value = parameter.getValue();

            if (position > 0) {
                query.setParameter(position, value);
            } else {
                query.setParameter(name, value);
            }

            logger.debug("Adicionado parâmetro key: {}, valor: {}", name, value);
        }
    }

    @Override
    public void forEachBatch(Collection<T> collection, Consumer<? super T> action) {
        int i = 0;
        for (T item : collection) {
            action.accept(item);
            if (++i % EntityRepository.JDBC_BATCH_WRITING == 0) {
                try {
                    entityManager.flush();
                    entityManager.clear();
                } catch (ConstraintViolationException e) {
                    logger.error("Exception: ");
                    e.getConstraintViolations().forEach(err -> logger.error(err.toString()));
                }
            }
        }
    }

}