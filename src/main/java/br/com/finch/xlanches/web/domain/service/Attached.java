package br.com.finch.xlanches.web.domain.service;

import java.io.Serializable;

/**
 * @author Filipe Bojikian Rissi
 */
public interface Attached extends Serializable {

    String getDescription();

    byte[] getFile();

    String getName();

    String getType();

}