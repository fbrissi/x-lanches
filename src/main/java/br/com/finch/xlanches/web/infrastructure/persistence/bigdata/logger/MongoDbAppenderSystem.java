package br.com.finch.xlanches.web.infrastructure.persistence.bigdata.logger;

import org.log4mongo.MongoDbAppender;

/**
 * @author Filipe Bojikian Rissi
 */
public class MongoDbAppenderSystem extends MongoDbAppender {

    @Override
    public void activateOptions() {
        super.activateOptions();
        setBsonifier(new LoggingEventBsonifierSystemImpl());
    }

}