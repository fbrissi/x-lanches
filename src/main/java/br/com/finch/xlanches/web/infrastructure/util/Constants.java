package br.com.finch.xlanches.web.infrastructure.util;

/**
 * @author Filipe Bojikian Rissi
 */
public class Constants {

    public static class Locale {

        private Locale() {
        }

        public static final java.util.Locale BRAZIL = new java.util.Locale("pt", "BR");

    }

    private Constants() {
    }

    private static final String CONTEXTPATH = "xlanches.contextPath";
    private static final String RECAPTCHA = "xlanches.recaptcha";

    public static final String XLANCHES_CONTEXTPATH;
    public static final boolean XLANCHES_RECAPTCHA;

    public static final String WORKING_SESSION_ATTRIBUTE = "working";

    static {
        if (System.getProperty(CONTEXTPATH) == null) {
            System.setProperty(CONTEXTPATH, "http://localhost:8080");
        }

        if (System.getProperty(RECAPTCHA) == null) {
            System.setProperty(RECAPTCHA, String.valueOf(Boolean.FALSE));
        }

        XLANCHES_CONTEXTPATH = System.getProperty(CONTEXTPATH);
        XLANCHES_RECAPTCHA = Boolean.parseBoolean(System.getProperty(RECAPTCHA));
    }

}