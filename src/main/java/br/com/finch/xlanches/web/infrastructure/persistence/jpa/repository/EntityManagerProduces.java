package br.com.finch.xlanches.web.infrastructure.persistence.jpa.repository;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author Filipe Bojikian Rissi
 */
public class EntityManagerProduces {

    private static final EntityManagerFactory XLANCHES_AMIGO_FACTORY = Persistence.createEntityManagerFactory("x-lanches");

    @Produces
    @RequestScoped
    public EntityManager createAppEntityManager() {
        return XLANCHES_AMIGO_FACTORY.createEntityManager();
    }

    public void closeEntityManager(@Disposes EntityManager manager) {
        if (manager.isOpen()) {
            manager.close();
        }
    }

}