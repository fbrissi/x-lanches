package br.com.finch.xlanches.web.infrastructure.thymeleaf.conversion;

import org.springframework.context.MessageSource;
import org.springframework.format.Formatter;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Locale;

/**
 * @author Filipe Bojikian Rissi
 */
public class BigDecimalFormatter implements Formatter<BigDecimal> {

    @Inject
    private MessageSource messageSource;

    @Override
    public BigDecimal parse(final String text, final Locale locale) throws ParseException {
        final DecimalFormat decimalFormat = createDecimalFormat(locale);
        decimalFormat.setParseBigDecimal(true);
        return (BigDecimal) decimalFormat.parse(text);
    }

    @Override
    public String print(final BigDecimal object, final Locale locale) {
        final DecimalFormat decimalFormat = createDecimalFormat(locale);
        return decimalFormat.format(object);
    }

    private DecimalFormat createDecimalFormat(final Locale locale) {
        final String format = this.messageSource.getMessage("decimal.format", null, locale);
        return new DecimalFormat(format);
    }

}