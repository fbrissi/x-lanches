package br.com.finch.xlanches.web.domain.model.exception;

import br.com.finch.xlanches.web.domain.model.shared.ResourceBundleException;

/**
 * @author Filipe Bojikian Rissi
 */
public class EncryptionException extends ResourceBundleException {

    private static final long serialVersionUID = -2502562715337427812L;

    public EncryptionException() {
        super("{xlanches.exception.EncryptionException.message}");
    }
}
