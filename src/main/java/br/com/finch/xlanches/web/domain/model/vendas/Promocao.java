package br.com.finch.xlanches.web.domain.model.vendas;

import br.com.finch.xlanches.web.domain.model.shared.AuditingEntity;
import br.com.finch.xlanches.web.domain.model.shared.EnumDescription;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author Filipe Bojikian Rissi
 */
@Entity
@Table(name = "promocao")
@SequenceGenerator(name = "promocao_id_seq", sequenceName = "promocao_id_seq", allocationSize = 1)
public class Promocao extends AuditingEntity<Promocao, Integer> {

    private static final long serialVersionUID = 3669351461251097885L;

    public enum TipoDesconto implements EnumDescription {
        PERCENTUAL("P", "Percentual"),
        QUANTIDADE("Q", "Quantidade");

        private final String valor;
        private final String descricao;

        TipoDesconto(String valor, String descricao) {
            this.valor = valor;
            this.descricao = descricao;
        }

        @Override
        public String getValor() {
            return valor;
        }

        @Override
        public String getDescricao() {
            return descricao;
        }

        public static TipoDesconto fromValor(String value) {
            for (TipoDesconto tipo : TipoDesconto.values())
                if (tipo.valor.equals(value))
                    return tipo;

            return null;
        }

    }

    @Id
    @GeneratedValue(generator = "promocao_id_seq")
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Size(max = 55)
    @Column(name = "nome")
    private String nome;

    @Basic(optional = false)
    @NotNull
    @Column(name = "desconto")
    private BigDecimal desconto;

    @Basic(optional = false)
    @NotNull
    @Column(name = "tipo_desconto")
    private TipoDesconto tipoDesconto;

    @ElementCollection
    @CollectionTable(name = "promocao_ingrediente", joinColumns = @JoinColumn(name = "promocao_id", referencedColumnName = "id"))
    private Set<IngredienteItem> ingredientes;

    protected Promocao() {
        super();
        this.ingredientes = new HashSet<>();
    }

    public Promocao(String nome, BigDecimal desconto, TipoDesconto tipoDesconto) {
        super();
        assert (nome != null);
        assert (desconto != null);
        assert (tipoDesconto != null);

        this.nome = nome;
        this.desconto = desconto;
        this.tipoDesconto = tipoDesconto;
        this.ingredientes = new HashSet<>();
    }

    @Override
    public Integer getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BigDecimal getDesconto() {
        return desconto;
    }

    public void setDesconto(BigDecimal desconto) {
        this.desconto = desconto;
    }

    public TipoDesconto getTipoDesconto() {
        return tipoDesconto;
    }

    public void setTipoDesconto(TipoDesconto tipoDesconto) {
        this.tipoDesconto = tipoDesconto;
    }

    public Set<IngredienteItem> getIngredientes() {
        return Collections.unmodifiableSet(ingredientes);
    }

    public boolean addIngrediente(BigDecimal quantidade, Ingrediente ingrediente) {
        assert (quantidade != null);
        assert (ingrediente != null);

        return this.ingredientes.add(new IngredienteItem(quantidade, ingrediente));
    }

    public boolean removerIngrediente(IngredienteItem pedidoItemIngrediente) {
        assert (pedidoItemIngrediente != null);

        return this.ingredientes.remove(pedidoItemIngrediente);
    }

    public void removerTodosIngrediente() {
        this.ingredientes.clear();
    }

    @Override
    public boolean equals(Object object) {
        if (object == null)
            return false;

        if (this == object)
            return true;

        if (!(object instanceof Promocao))
            return false;

        Promocao other = (Promocao) object;
        return sameId(other);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.id);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Promocao{");
        sb.append("id=").append(id);
        sb.append(", nome='").append(nome).append('\'');
        sb.append(", desconto=").append(desconto);
        sb.append(", tipoDesconto=").append(tipoDesconto);
        sb.append('}');
        return sb.toString();
    }

}