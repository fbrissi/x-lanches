package br.com.finch.xlanches.web.interfaces.spring.cadastro;

import br.com.finch.xlanches.web.application.PromocaoService;
import br.com.finch.xlanches.web.interfaces.spring.error.EntityNotFoundException;
import br.com.finch.xlanches.web.interfaces.usuario.facade.cadastro.PromocaoDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.validation.Valid;

/**
 * @author Filipe Bojikian Rissi
 */
@Controller
@RequestMapping(value = "/promocao")
public class PromocaoController {

    @Inject
    private PromocaoService promocaoService;

    @RequestMapping(method = RequestMethod.GET)
    public String init() {
        return "cadastro/promocao/listar";
    }

    @ResponseBody
    @RequestMapping(value = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String listar(int start, int length) {
        JsonObject jsonObject = promocaoService.listar(start, length);
        return jsonObject.toString();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String editar(@PathVariable("id") Integer id, Model model) {
        PromocaoDTO promocao = promocaoService.editar(id);
        if (promocao == null) {
            throw new EntityNotFoundException();
        }
        model.addAttribute("promocao", promocao);
        return "cadastro/promocao/formulario";
    }

    @RequestMapping(value = "/cadastrar", method = RequestMethod.GET)
    public String cadastrar(Model model) {
        model.addAttribute("promocao", new PromocaoDTO());
        return "cadastro/promocao/formulario";
    }

    @RequestMapping(value = "/cadastrar", method = RequestMethod.PUT)
    public String alterar(@ModelAttribute("promocao") @Valid PromocaoDTO promocao, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "cadastro/promocao/formulario :: form";
        }
        promocaoService.alterar(promocao);
        model.addAttribute("redirect", "/promocao");
        return "redirect :: url";
    }

    @RequestMapping(value = "/cadastrar", method = RequestMethod.POST)
    public String persistir(@ModelAttribute("promocao") @Valid PromocaoDTO promocao, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "cadastro/promocao/formulario :: form";
        }
        promocaoService.persistir(promocao);
        model.addAttribute("redirect", "/promocao");
        return "redirect :: url";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deletar(@PathVariable("id") Integer id) {
        promocaoService.deletar(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}