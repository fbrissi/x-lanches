package br.com.finch.xlanches.web.infrastructure.persistence.interceptor;

import br.com.finch.xlanches.web.domain.model.shared.ResourceBundleException;

/**
 * @author Filipe Bojikian Rissi
 */
public class TransactionException extends ResourceBundleException {

    private static final long serialVersionUID = -997859696065167212L;

    public TransactionException() {
    }

    public TransactionException(String message) {
        super(message);
    }

    public TransactionException(String message, Throwable cause) {
        super(message, cause);
    }

    public TransactionException(Throwable cause) {
        super(cause);
    }

    public TransactionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
