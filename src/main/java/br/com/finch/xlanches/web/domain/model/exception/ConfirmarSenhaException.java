package br.com.finch.xlanches.web.domain.model.exception;

import br.com.finch.xlanches.web.domain.model.shared.ResourceBundleException;

/**
 * @author Filipe Bojikian Rissi
 */
public class ConfirmarSenhaException extends ResourceBundleException {

    private static final long serialVersionUID = 3209538318018110892L;

    public ConfirmarSenhaException() {
        super("{xlanches.exception.ConfirmarSenha.message}");
    }

}