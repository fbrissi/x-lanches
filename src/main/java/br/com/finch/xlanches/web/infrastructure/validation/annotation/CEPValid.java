package br.com.finch.xlanches.web.infrastructure.validation.annotation;

import br.com.finch.xlanches.web.infrastructure.validation.logic.CEPValidatorLogic;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;

/**
 * @author Filipe Bojikian Rissi
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({FIELD, METHOD})
@Constraint(validatedBy = CEPValidatorLogic.class)
public @interface CEPValid {

    String message() default "{xlanches.validator.constraints.CEP.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}