package br.com.finch.xlanches.web.domain.model.identificacao;

import br.com.finch.xlanches.web.domain.model.shared.ValuesObject;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * @author Filipe Bojikian Rissi
 */
public class CEP implements ValuesObject<CEP> {

    private static final long serialVersionUID = -2357079977610642190L;

    private final String numero;
    private final String digito;

    protected CEP() {
        super();
        this.numero = null;
        this.digito = null;
    }

    public CEP(String numero) {
        super();
        assert (numero != null);
        assert (numero.length() == 8 || numero.length() == 9);

        String value = numero.replaceAll("\\D", "");
        this.numero = value.substring(0, 5);
        this.digito = value.substring(5, 8);
    }

    public String getNumero() {
        return numero;
    }

    public String getDigito() {
        return digito;
    }

    public String toString(boolean formatado) {
        return formatado ? String.format("%s-%s", numero, digito) : toString();
    }

    @Override
    public String toString() {
        return numero + digito;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(this.numero)
                .append(this.digito).toHashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (object == null)
            return false;

        if (this == object)
            return true;

        if (!(object instanceof CEP))
            return false;

        CEP other = (CEP) object;
        return sameValue(other);
    }

    @Override
    public boolean sameValue(CEP other) {
        return new EqualsBuilder()
                .append(this.numero, other.numero)
                .append(this.digito, other.digito).isEquals();
    }

    @Override
    public int compareTo(CEP other) {
        return new CompareToBuilder()
                .append(this.numero, other.numero)
                .append(this.digito, other.digito).toComparison();
    }

}