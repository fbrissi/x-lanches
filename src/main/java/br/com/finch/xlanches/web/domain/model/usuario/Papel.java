package br.com.finch.xlanches.web.domain.model.usuario;

import br.com.finch.xlanches.web.domain.model.shared.AuditingEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

/**
 * @author Filipe Bojikian Rissi
 */
@Entity
@Table(name = "papel")
@SequenceGenerator(name = "papel_id_seq", sequenceName = "papel_id_seq", allocationSize = 1)
@NamedQueries({
        @NamedQuery(name = PapelRepository.findByTipo, query = "SELECT p FROM Papel p WHERE p.tipo = :tipo")
})
public class Papel extends AuditingEntity<Papel, Integer> {

    private static final long serialVersionUID = 8504635260986346000L;

    public enum Tipo {
        ADMIN("A", "Administrador"),
        USER("U", "Usuário");

        private final String valor;
        private final String descricao;

        Tipo(String valor, String descricao) {
            this.valor = valor;
            this.descricao = descricao;
        }

        public String getValor() {
            return valor;
        }

        public String getDescricao() {
            return descricao;
        }

        public static Tipo fromValor(String value) {
            for (Tipo tipo : Tipo.values())
                if (tipo.valor.equals(value))
                    return tipo;

            return null;
        }

    }

    @Id
    @GeneratedValue(generator = "papel_id_seq")
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "tipo")
    private Tipo tipo;

    @Basic(optional = false)
    @NotNull
    @Size(max = 32)
    @Column(name = "descricao")
    private String descricao;

    protected Papel() {
        super();
    }

    public Papel(Tipo tipo, String descricao) {
        super();
        assert (tipo != null);
        assert (descricao != null);

        this.tipo = tipo;
        this.descricao = descricao;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null)
            return false;

        if (this == object)
            return true;

        if (!(object instanceof Papel))
            return false;

        Papel other = (Papel) object;
        return sameId(other);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.id);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Papel{");
        sb.append("id=").append(id);
        sb.append(", tipo=").append(tipo);
        sb.append(", descricao='").append(descricao).append('\'');
        sb.append('}');
        return sb.toString();
    }

}