package br.com.finch.xlanches.web.infrastructure.validation.logic;

import br.com.finch.xlanches.web.domain.model.contato.Telefone;
import br.com.finch.xlanches.web.infrastructure.validation.annotation.TelefoneValid;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author Filipe Bojikian Rissi
 */
public class TelefoneValidatorLogic implements ConstraintValidator<TelefoneValid, Telefone> {

    @Override
    public void initialize(TelefoneValid cnpj) {
    }

    @Override
    public boolean isValid(Telefone telefone, ConstraintValidatorContext context) {
        if (telefone != null) {
            if (telefone.toString().trim().length() == 0) {
                return true;
            } else {
                return telefone.toString().trim().length() >= 10 && telefone.toString().trim().length() <= 11;
            }
        }

        return true;
    }

}