Meta:
@author Filipe Bojikian Rissi

Narrativa:
Realizar testes ao fazer pedido e conferir os valores calculados

Cenário: Fazer pedido

Dado que o ingrediente 0 - Alface com valor 0.40
Dado que o ingrediente 1 - Bacon com valor 2.00
Dado que o ingrediente 2 - Hambúrguer de carne com valor 3.00
Dado que o ingrediente 3 - Ovo com valor 0.8
Dado que o ingrediente 4 - Queijo com valor 1.5

Dado que a promoção 0 - Light com desconto do tipo PERCENTUAL no valor de 10.0 com os ingredientes(quantidade:index) 1:0, 0:1
Dado que a promoção 1 - Muita Carne com desconto do tipo QUANTIDADE no valor de 0.0 com os ingredientes(quantidade:index) 3:2
Dado que a promoção 1 - Muito Queijo com desconto do tipo QUANTIDADE no valor de 0.0 com os ingredientes(quantidade:index) 3:4

Dado que o cardápio 0 - X-Bacon: Bacon, hambúrguer de carne e queijo com os ingredientes(quantidade:index) 1:1, 1:2, 1:4
Dado que o cardápio 1 - X-Burger: Hambúrguer de carne e queijo com promocao 0 com os ingredientes(quantidade:index) 1:2, 1:4
Dado que o cardápio 2 - X-Egg: Ovo, hambúrguer de carne e queijo com os ingredientes(quantidade:index) 1:3, 1:2, 1:4
Dado que o cardápio 3 - X-Egg Bacon: Ovo, bacon, hambúrguer de carne e queijo com promocao 1 com os ingredientes(quantidade:index) 1:3, 1:1, 1:2, 1:4

Quando fazer pedido
Quando adicionar o cardápio 0 com quantidade 1
Quando adicionar o cardápio 1 com quantidade 1
Quando adicionar o cardápio 3 com quantidade 2

Então valor do pedido deve ser 25.6
Então desconto do pedido deve ser 0.0
Então toal do pedido deve ser 25.6