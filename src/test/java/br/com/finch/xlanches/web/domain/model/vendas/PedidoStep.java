package br.com.finch.xlanches.web.domain.model.vendas;

import org.hamcrest.Matchers;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

/**
 * @author by Filipe Bojikian Rissi
 */
public class PedidoStep {

    private List<Ingrediente> ingredientes = new ArrayList<>();
    private List<Promocao> pormocoes = new ArrayList<>();
    private List<Cardapio> cardapios = new ArrayList<>();
    private Pedido pedido;

    @Given("o ingrediente $nome com valor $valor")
    public void newIngrediente(String nome, double valor) {
        Ingrediente ingrediente = spy(new Ingrediente(nome, null, BigDecimal.valueOf(valor)));
        when(ingrediente.getId()).thenReturn(Integer.parseInt(nome.substring(0, nome.indexOf(" -"))));
        ingredientes.add(ingrediente);
    }

    @Given("a promoção $nome com desconto do tipo $tipoDesconto no valor de $desconto com os ingredientes(quantidade:index) $ingredientes")
    public void newPromocao(String nome, String tipoDesconto, double desconto, String ingredientes) {
        String[] split = ingredientes.split("\\s*,\\s*");
        Promocao promocao = spy(new Promocao(nome, BigDecimal.valueOf(desconto), Promocao.TipoDesconto.valueOf(tipoDesconto)));
        when(promocao.getId()).thenReturn(Integer.parseInt(nome.substring(0, nome.indexOf(" -"))));
        for (String value : split) {
            String[] val = value.split(":");
            promocao.addIngrediente(BigDecimal.valueOf(Integer.valueOf(val[0])), this.ingredientes.get(Integer.valueOf(val[1])));
        }
        pormocoes.add(promocao);
    }

    @Given("o cardápio $nome: $observacao com os ingredientes(quantidade:index) $ingredientes")
    public void newCardapio(String nome, String observacao, String ingredientes) {
        String[] split = ingredientes.split("\\s*,\\s*");
        Cardapio cardapio = spy(new Cardapio(nome, observacao, null));
        when(cardapio.getId()).thenReturn(Integer.parseInt(nome.substring(0, nome.indexOf(" -"))));
        for (String value : split) {
            String[] val = value.split(":");
            cardapio.addIngrediente(BigDecimal.valueOf(Integer.valueOf(val[0])), this.ingredientes.get(Integer.valueOf(val[1])));
        }
        cardapios.add(cardapio);
    }

    @Given("o cardápio $nome: $observacao com promocao $index com os ingredientes(quantidade:index) $ingredientes")
    public void newCardapioCompromocao(String nome, String observacao, int index, String ingredientes) {
        String[] split = ingredientes.split("\\s*,\\s*");
        Cardapio cardapio = spy(new Cardapio(nome, observacao, null));
        when(cardapio.getId()).thenReturn(Integer.parseInt(nome.substring(0, nome.indexOf(" -"))));
        for (String value : split) {
            String[] val = value.split(":");
            cardapio.addIngrediente(BigDecimal.valueOf(Integer.valueOf(val[0])), this.ingredientes.get(Integer.valueOf(val[1])));
        }
        cardapio.setPromocao(pormocoes.get(index));
        cardapios.add(cardapio);
    }

    @When("fazer pedido")
    public void newPedido() {
        pedido = new Pedido();
    }

    @When("adicionar o cardápio $index com quantidade $quantidade")
    public void addCardapio(int index, double quantidade) {
        pedido.addItem(BigDecimal.valueOf(quantidade), cardapios.get(index));
    }

    @Then("valor do pedido deve ser $valor")
    public void verificarValor(double valor) {
        Assert.assertThat(pedido.getValor(), Matchers.comparesEqualTo(BigDecimal.valueOf(valor)));
    }

    @Then("desconto do pedido deve ser $desconto")
    public void verificarDesconto(double desconto) {
        Assert.assertThat(pedido.getDesconto(), Matchers.comparesEqualTo(BigDecimal.valueOf(desconto)));
    }

    @Then("toal do pedido deve ser $total")
    public void verificarTotal(double total) {
        Assert.assertThat(pedido.getTotal(), Matchers.comparesEqualTo(BigDecimal.valueOf(total)));
    }

}