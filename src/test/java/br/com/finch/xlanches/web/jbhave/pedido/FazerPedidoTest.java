package br.com.finch.xlanches.web.jbhave.pedido;

import br.com.finch.xlanches.web.domain.model.vendas.PedidoStep;
import br.com.finch.xlanches.web.infrastructure.cdi.SessionProduces;
import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.Keywords;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.i18n.LocalizedKeywords;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.junit.JUnitStory;
import org.jbehave.core.parsers.RegexStoryParser;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import javax.mail.Transport;
import java.util.Locale;

/**
 * @author Filipe Bojikian Rissi
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitReportingRunner.class)
@PrepareForTest({Transport.class, SessionProduces.class})
public class FazerPedidoTest extends JUnitStory {

    @Override
    public Configuration configuration() {
        Keywords keywords = new LocalizedKeywords(new Locale("pt", "BR"));
        return new MostUsefulConfiguration()
                .useKeywords(keywords)
                .useStoryParser(new RegexStoryParser(keywords))
                .useStoryLoader(new LoadFromClasspath(this.getClass()))
                .useStoryReporterBuilder(new StoryReporterBuilder()
                        .withDefaultFormats()
                        .withFormats(Format.CONSOLE, Format.TXT));
    }

    @Override
    public InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new PedidoStep());
    }

}