#!/usr/bin/env bash
su mongodb  -c "mongod &"
su postgres -c "postgres -D /var/lib/postgresql/main -c config_file=/etc/postgresql/postgresql.conf &"
su jetty -c "java -jar /usr/local/jetty/start.jar"