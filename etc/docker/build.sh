#!/usr/bin/env bash
docker build -t xlanches .
docker run -d -p 80:8080 -p 443:8443 -p 5432:5432 -e POSTGRES_PASSWORD=postgres --name x-lanches xlanches