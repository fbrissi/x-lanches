# X-LANCHES - STARTUP

---

## Descrição do problema
Somos uma startup do ramo de alimentos e precisamos de uma aplicação web para gerir nosso
negócio. Nossa especialidade é a venda de lanches, de modo que alguns lanches são opções de
cardápio e outros podem conter ingredientes personalizados.

**A seguir, apresentamos a lista de ingredientes disponíveis:**

#### Ingrediente: Valor

- Alface: R$ 0.40
- Bacon: R$ 2,00
- Hambúrguer de carne: R$ 3,00
- Ovo: R$ 0,80
- Queijo: R$ 1,50

**Segue as opções de cardápio e seus respectivos ingredientes:**

#### Lanche: Ingredientes

- X-Bacon: Bacon, hambúrguer de carne e queijo
- X-Burger: Hambúrguer de carne e queijo
- X-Egg: Ovo, hambúrguer de carne e queijo
- X-Egg Bacon: Ovo, bacon, hambúrguer de carne e queijo

O valor de cada opção do cardápio é dado pela soma dos ingredientes que compõe o lanche.
Além destas opções, o cliente pode personalizar seu lanche e escolher os ingredientes que
desejar. Nesse caso, o preço do lanche também será calculado pela soma dos ingredientes.

Existe uma exceção à regra para o cálculo de preço, quando o lanche pertencer à uma
promoção. A seguir, apresentamos a lista de promoções e suas respectivas regras de negócio:

#### **Promoção:** Regra De Negócio

**Light:** Se o lanche tem alface e não tem bacon, ganha 10% de desconto.

**Muita carne:** A cada 3 porções de carne o cliente só paga 2, a cada 6 porções, o cliente pagará
4 e assim sucessivamente.

**Muito queijo:** A cada 3 porções de queijo o cliente só paga 2, a cada 6 porções, o cliente
pagará 4 e assim sucessivamente

**Inflação:** Os valores dos ingredientes são alterados com frequência e não gostaríamos que isso
influenciasse nos testes automatizados.

---

## Designer Do Código

O código foi escrito usando a abordagem de desenvolvimento "Domain-driven design",
o motivo de ter escolhido essa abordagem é que traz um desenvolvimento claro, fácil de entender
e de fácil refatoração. O desenvolvimento em si é um pouco mais demorado que o desenvolvimentos tradicionais,
pois nessa abordagem os conceitos de Orientação a Objetos são bem empregados, e a regra de negócio faz parte do
domínio da aplicação e não são colocados em camadas separadas.

O ServletContainer escolhido foi o Jetty, por trazer alguns recursos na configuração.
O server-side foi utilizado Spring-MVC, Spring-Security, CDI, JPA (EclipseLink), Java Mail, Thymeleaf.

Obs: O motivo de ter-se escolhido o Thymeleaf como template de páginas HTML é que ele é bem fácil de
integrar com templates escritos por WebDesigner .

---

## Instruções Para Executar o Sistema

#### Pré Requisitos

- Java 8+
- Mavens 3+
- Postgres 9+
- Jetty 9.2+

---

### Postgres
 
#### Criar usuario:
 
CREATE ROLE xlanches_user LOGIN
  ENCRYPTED PASSWORD 'md5322f0ba7bfae9b0704b988bc24f0891d'
  NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
  
#### Criar banco de dados:
   
CREATE DATABASE x_lanches;
ALTER DATABASE x_lanches OWNER TO xlanches_user;

Obs: As tabelas serão criadas ao executar o projeto pela primeira vez com um FrameWork de versionamento de banco de dados (LiquiBase)

---

### Jetty

#### Rodar Com Jetty 9.2.+

Antes de rodar entrar no diretório do jetty e executar:
java -jar start.jar --add-to-start=cdi

---

### Maven

Para rodar o projeto com o Maven não é necessário ter o Jetty instalado, mas é preciso ter o banco de dados instalado
e ter criado o usuário e data base conforme instruções acima
 
##### Para executar

Basta entrar no diretório raiz do projeto e executar o comando:

mvn jetty:run

##### Para Rodar os Testes

Para rodar o findBugs e executar os testes automatizados, basta executar o comando:

mvn -P findbugs,test clean install

---

## Build Contínuo

A cada push realizado, é executado um hook, que inicia uma virtualização docker e compilar com maven utilizando o comando acima,
dessa forma é executado o findBugs e os testes automatizados

## Links

- [Git Repository](https://bitbucket.org/fbrissi/x-lanches)
- [Bug Tracker](https://bitbucket.org/fbrissi/x-lanches/issues)

---

## Executar Com Docker

Para compilar com docker execute o script: 
/etc/docker/build.sh

Para parar a virtualização execute o comando:
docker stop x-lanches

Para subir a virtualização execute o comando:
docker start x-lanches

Para acessar o sistema acessar a url no Browser:
http://localhost

## Usuário Administrador
Usuário: admin@xlanches.com
Senha:   123456